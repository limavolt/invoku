/* Add here all your JS customizations */
function cleanModal(form_name, clear_value) {
	$.each($('input, select, textarea', form_name), function () {
		var type = $(this).attr('type');

		if (type != 'checkbox') {
			var name_element = $(this).attr('name');

			if (clear_value) {
				$('#' + name_element).val('').trigger('change');
			}

			$('#div_' + name_element, form_name).removeClass('has-error');
            $('#label_' + name_element, form_name).empty();

			$('#div-' + name_element, form_name).removeClass('has-error');
			$('#error-' + name_element, form_name).empty();
		}

		if (type == 'checkbox') {
			$(this).prop('checked', false);
			$(this).prop('disabled', false);
		}
	});
}

/*
 * Menonaktifkan fungsi tombol `enter` sehingga tidak menutup jendela modal.
 */
function disableEvents(form) {
	$(form).bind("keypress", function(event) {
		if (event.keyCode == 13 || event.which == 13) {
			event.preventDefault();
		}
	});
}

function generateSuccess(modal, message, table = 'reload') {
	modal.modal('hide');

    if (table == 'reload') {
        table.ajax.reload();
    }

	new PNotify({
		type:  'success',
		title: 'Berhasil!',
		text:  message,
	});
}

function generateError(form_id, errors) {
	cleanModal(form_id, false);

	new PNotify({
		type:  'warning',
		title: 'Peringatan!',
		text:  'Terdapat kesalahan pada data yang dimasukkan',
	});

	$.each(errors.errors, function (col_val, msg) {
		$(form_id + ' #div-' + col_val).addClass('has-error');
		$(form_id + ' #error-' + col_val).html(msg[0]);
	});
}

$(document).on('click', '.modal-dismiss', function (e) {
	e.preventDefault();
	$.magnificPopup.close();
});

/*
 * Fungsi untuk membersihkan nilai yang dimiliki
 * setiap bidang pada sebuah form.
 */
function clearForm(form)
{
	form.find('input:text, input:password, input:file, select, textarea').val([]);
	form.find(':input[type=number]').val([]);
	form.find('input:radio, input:checkbox').prop('checked', false);
	form.find($('.select2-chosen')).text('');
}

/* Retrieve company list from database based on its type. */
function selectCompany(type, element, parent, cleared = 'clear') {
    $.ajax({
        method: 'get',
        type:   'json',
        url:    '/companies/type/' + type,

        success: function (responses) {
            var initial = element.val();

            element.empty();
            element.css('width', '100%');

            element.select2({
                dropdownParent: parent,
                placeholder:    'Pilih dari daftar',
                allowClear:     true,
                data:           responses,
            });

            if (cleared == 'clear') {
                element.val(null).trigger('change');
            }
            else {
                element.val(initial).trigger('change');
            }
        },

        error: function () {
            element.empty();
            element.css('width', '100%');
            element.select2();
        }
    });
}

/* Retrieve item list from database based on its supplier. */
function selectItem(supplier_id, element, parent, cleared = 'clear') {
    $.ajax({
        method: 'get',
        type:   'json',
        url:    '/items/supplier/' + supplier_id,

        success: function (responses) {
            var initial = element.val();

            element.empty();
            element.css('width', '100%');

            element.select2({
                dropdownParent: parent,
                placeholder:    'Pilih dari daftar',
                allowClear:     true,
                data:           responses,
            });

            if (cleared == 'clear') {
                element.val(null).trigger('change');
            }
            else {
                element.val(initial).trigger('change');
            }
        },

        error: function () {
            element.empty();
            element.css('width', '100%');
            element.select2();
        }
    });
}

/* Retrieve unit list from database. */
function selectUnit(element, parent, cleared = 'clear') {
    $.ajax({
        method: 'get',
        type:   'json',
        url:    '/units/select',

        success: function (responses) {
            var initial = element.val();

            element.empty();
            element.css('width', '100%');

            element.select2({
                dropdownParent: parent,
                placeholder:    'Pilih dari daftar',
                allowClear:     true,
                data:           responses,

                sorter: function (responses) {
                    return responses.sort(function (a, b) {
                        return a.text < b.text ? -1 : a. text > b.text ? 1 : 0;
                    });
                },
            });

            if (cleared == 'clear') {
                element.val(null).trigger('change');
            }
            else {
                element.val(initial).trigger('change');
            }
        },

        error: function () {
            element.select2();
        }
    });
}

/* Retrieve unit list from database. */
function selectSubunit(element, parent, cleared = 'clear') {
    $.ajax({
        method: 'get',
        type:   'json',
        url:    '/ajax/units/fetch',

        success: function (responses) {
            var initial = element.val();

            element.empty();
            element.css('width', '100%');

            element.select2({
                dropdownParent: parent,
                placeholder:    'Pilih dari daftar',
                allowClear:     true,
                tags:           true,
                data:           responses,
            });

            if (cleared == 'clear') {
                element.val(null).trigger('change');
            }
            else {
                element.val(initial).trigger('change');
            }
        },

        error: function () {
            element.select2();
        }
    });
}

/* Retrieve item group list from database. */
function selectItemGroup(element, parent, cleared = 'clear') {
    $.ajax({
        method: 'get',
        type:   'json',
        url:    '/item-groups/select',

        success: function (responses) {
            var initial = element.val();

            element.empty();
            element.css('width', '100%');

            element.select2({
                dropdownParent: parent,
                placeholder:    'Pilih dari daftar',
                allowClear:     true,
                tags:           true,
                data:           responses,
            });

            if (cleared == 'clear') {
                element.val(null).trigger('change');
            }
            else {
                element.val(initial).trigger('change');
            }
        },

        error: function () {
            element.empty();
            element.css('width', '100%');
            element.select2();
        }
    });
}

/* Datatable filtering function to be used by user to filtering
 * the content of datatable based on filter list. */
function setFilteredData(selection_id, column_number) {

    // Get the value of selected option from dropdown list.
    var selection = $(selection_id).find(':selected').val();

    // Get the data based on user selected filter
    // then draw the table result of filtered data.
    table.column(column_number).search(selection).draw();
}
