<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['as' => 'api.'], function(){
	Route::get('items/select2', 'Api\ApiItemController@fetchSelect2Data')->name('items.select2');
	Route::get('capital-prices/get-latest-value/{item_id}', 'Api\ApiCapitalPriceController@getLatestValue');
    Route::get('items/get-by-supplier/{supplier_id}', 'Api\ApiItemController@getItemsBySupplier')->name('items.get-by-supplier');
    Route::get('stocks/select2', 'Api\ApiStockController@fetchSelect2Data')->name('stocks.select2');
    Route::get('stocks/select2/{item_id}', 'Api\ApiStockController@fetchSelect2Item')->name('stocks.select2.item');
    Route::get('stocks/get-by/price/{comparison}', 'Api\ApiStockController@getByPrice')->name('stocks.get-by-price');
    Route::get('stocks/get-sale/{item_id}', 'Api\ApiStockController@getSale')->name('stocks.get-sale');

	Route::get('companies/get-cunsomers', 'Api\ApiCompanyController@getCunsomers');
	//Mau distandarisasi pakai get, bukan fetch, tapi barangkali fungsi ini sudah pernah digunakan. Jadi sementara masih pakai fetch sampai terkonfirmasi
	Route::get('companies/fetch-suppliers', 'Api\ApiCompanyController@fetchSuppliers');

	Route::apiResource('capital-prices', 'Api\ApiCapitalPriceController');
	Route::apiResource('companies', 'Api\ApiCompanyController');
	Route::apiResource('delivery-orders', 'Api\ApiDeliveryOrderController');
	Route::apiResource('incoming-goods', 'Api\ApiIncomingGoodController');
	Route::apiResource('invoices', 'Api\ApiInvoiceController');
	Route::apiResource('items', 'Api\ApiItemController');
	Route::apiResource('item-brands', 'Api\ApiItemBrandController');
	Route::apiResource('item-groups', 'Api\ApiItemGroupController');
	Route::apiResource('product-lists', 'Api\ApiProductListController');
	Route::apiResource('purchases', 'Api\ApiPurchaseController');
	Route::apiResource('selling-prices', 'Api\ApiSellingPriceController');
	Route::apiResource('stocks', 'Api\ApiStockController');
	Route::apiResource('suppliers', 'Api\ApiSupplierController');
	Route::apiResource('units', 'Api\ApiUnitController');
});

