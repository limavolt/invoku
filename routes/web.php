<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return redirect('/home');
})->middleware('auth');

if (config('app.env') == 'local') {
    Route::get('/tester', function () {
        $page_title = 'Tester';

        return view('pages.tester', compact('page_title'));
    })->middleware('auth');
}

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'StockController@index')->name('home');
});

/* Item brand routes. */
Route::group(['middleware' => ['auth']], function () {
    Route::get('/item-brands/export', 'ItemBrandController@export')->name('item-brands.export');
    Route::post('/item-brands/import', 'ItemBrandController@import')->name('item-brands.import');
    Route::get('/item-brands/template', 'ItemBrandController@getTemplate')->name('item-brands.get-template');
});

/* Item group routes. */
Route::group(['middleware' => ['auth']], function () {
    Route::get('item-groups/select', 'ItemGroupController@getSelect2Data')->name('item-groups.select2');
});

/* Company routes */
Route::group(['middleware' => ['auth']], function () {
    Route::get('/companies/export', 'CompanyController@export')->name('companies.export');
    Route::get('/companies/import', 'CompanyController@import')->name('companies.import');
    Route::get('/companies/template', 'CompanyController@getTemplate')->name('companies.template');
    Route::get('/companies/type/{type}', 'CompanyController@getSelect2Data')->name('companies.select2');
});

/* Item routes */
Route::group(['middleware' => ['auth']], function () {
    Route::get('/items/supplier/{id}', 'ItemController@getSelect2Data')->name('items.select2');
});

/* Unit routes */
Route::group(['middleware' => ['auth']], function () {
    Route::get('/units/select', 'InventoryUnitController@getSelect2Data')->name('units.select2');
});

/* Selling routes */
Route::group(['middleware' => ['auth']], function () {
    Route::get('/sales/invoice/{customer_id}/{item_id}', 'SaleController@getCustomerSellingPrice')->name('sales.customer-price');
});

Route::get('/sales/{id}/export', 'SaleController@export')->name('sales.export');
Route::get('/sales/print/{id}', 'SaleController@print')->name('sales.print');
Route::post('/sales/settlement/{id}', 'SaleController@settlement')->name('sales.settlement');

Route::group([ 'middleware' => 'auth', 'prefix' => 'ajax/', 'as' => 'ajax.' ], function(){
	Route::get('/fetch-id-permissions-for-role/{id}', 'RoleController@fetchIdPermissionsForRole');
	Route::post('/fetch-id-permissions-for-roles', 'RoleController@fetchIdPermissionsForRoles');
	Route::get('/fetch-id-suppliers-for-item/{id}', 'ItemController@fetchIdSuppliersForItem');
	Route::get('/fetch-inventory-units', 'InventoryUnitController@fetchInventoryUnits')->name('fetch_inventory_units');
	Route::get('/fetch-items', 'ItemController@fetchItems')->name('fetch_items');
	Route::get('/fetch-item-brands', 'ItemBrandController@fetchItemBrands')->name('fetch_item_brands');
	Route::get('/fetch-item-groups', 'ItemGroupController@fetchItemGroups')->name('fetch_item_groups');
	Route::get('/fetch-suppliers', 'SupplierController@fetchSuppliers')->name('fetch_suppliers');
	Route::get('/get-supplier-items/{id}', 'IncomingGoodController@getItem')->name('get-items');
	Route::get('/get-item', 'SaleController@getItem')->name('get-item');
	Route::get('/get-capital-prices/{id}', 'IncomingGoodController@getCapitalPrice')->name('get-capital-prices');
	Route::get('/get-selling-prices/{id}', 'SaleController@getSellingPrice')->name('get-selling-prices');
    Route::get('/get-default-selling-prices/{id}', 'SaleController@getDefaultSellingPrice')->name('get-default-selling-prices');
    Route::get('/get-stocks/{item_id}', 'StockController@getStock')->name('get-stocks');

	/* Fetching data */
	Route::get('/units/fetch', 'InventoryUnitController@fetchInventoryUnits')->name('get-units');
	Route::get('/units/get/{label}', 'InventoryUnitController@getUnit')->name('get-unit');

	/* Datatable data */
	Route::post('/items', 'ItemController@setDatatable')->name('items.data');
	Route::post('/customers', 'CustomerController@setDatatable')->name('customers.data');
	Route::post('/get-companies', 'CompanyController@setDatatable')->name('companies.data');
	Route::post('/get-invoices', 'SaleController@setDatatable')->name('sales.data');
	Route::post('/users/list', 'UserController@setDatatable')->name('users.data');
	Route::post('/units/list', 'InventoryUnitController@setDatatable')->name('units.data');
	Route::post('/incoming-goods/list', 'IncomingGoodController@setDatatable')->name('incoming-goods.data');

	Route::post('/get-inventory-units', 'InventoryUnitController@anyData')->name('inventory_units.data');
	Route::post('/get-item-brands', 'ItemBrandController@anyData')->name('item-brands.data');
	Route::post('/get-item-bundlings', 'ItemBundlingController@anyData')->name('item_bundlings.data');
	Route::post('/get-item-groups', 'ItemGroupController@anyData')->name('item-groups.data');
	Route::post('/get-item-stocks', 'ItemStockController@anyData')->name('item_stocks.data');
	Route::post('/get-roles', 'RoleController@anyData')->name('roles.data');
	Route::post('/get-stocks', 'StockController@setDatatable')->name('stocks.data');
	Route::post('/get-suppliers', 'SupplierController@setDatatable')->name('suppliers.data');
});

Route::group([ 'middleware' => 'auth', 'prefix' => 'items/', 'as' => 'items.' ], function() {
	Route::post('/choose-item', 'SaleController@chooseItem')->name('choose-item');
});

Route::group([ 'middleware' => 'auth', 'prefix' => 'incoming-goods/', 'as' => 'incoming-goods.' ], function() {
	Route::post('/choose-item', 'IncomingGoodController@chooseItem')->name('choose-item');
});

Route::group(['middleware' => 'auth'], function (){
	Route::resource('inventory-units', 'InventoryUnitController');
	Route::resource('items', 'ItemController');
	Route::resource('item-brands', 'ItemBrandController');
	Route::resource('item-bundlings', 'ItemBundlingController');
	Route::resource('item-groups', 'ItemGroupController');
	Route::resource('item-stocks', 'ItemStockController');
	Route::resource('roles', 'RoleController');
	Route::resource('stocks', 'StockController');
	Route::resource('suppliers', 'SupplierController');
	Route::resource('customers', 'CustomerController');
	Route::resource('users', 'UserController');
	Route::resource('companies', 'CompanyController');
	Route::resource('incoming-goods', 'IncomingGoodController');
	Route::resource('sales', 'SaleController');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'settings/', 'as' => 'settings.'], function () {
	Route::get('/', 'SettingController@index')->name('index');
	Route::post('/store', 'SettingController@store')->name('store');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'import/', 'as' => 'import.'], function () {
    Route::post('/items', 'ItemController@import')->name('items');
    Route::post('/item-groups', 'ItemGroupController@import')->name('item-groups');

    Route::post('/companies', 'CompanyController@import')->name('companies');
    Route::post('/units', 'InventoryUnitController@import')->name('units');
    Route::post('/stocks', 'StockController@import')->name('stocks');
    Route::post('/prices', 'DefaultPriceController@import')->name('prices');
    Route::post('/capital-price', 'CapitalPriceController@import')->name('capital-prices');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'database/', 'as' => 'database.'], function () {
    Route::post('/import/{type}', 'SettingController@import')->name('import');
});
