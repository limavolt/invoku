<?php

if (!function_exists('settings')) {
    function settings($key = null, $default = null)
    {
        if ($key === null) {
            return app(\App\AppSetting::class);
        }

        return app(\App\AppSetting::class)->get($key, $default);
    }
}
