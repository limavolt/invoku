<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class IncomingGoodController extends Controller
{
    /**
     * Mendeklarasikan metode konstruktor untuk kelas.
     */
    public function __construct()
    {
        $this->capital_price_api = new Api\ApiCapitalPriceController;
        $this->company_api       = new Api\ApiCompanyController;
        $this->incoming_good_api = new Api\ApiIncomingGoodController;
        $this->item_api          = new Api\ApiItemController;
        $this->item_brand_api    = new Api\ApiItemBrandController;
        $this->item_group_api    = new Api\ApiItemGroupController;
        $this->product_list_api  = new Api\ApiProductListController;
        $this->purchase_api      = new Api\ApiPurchaseController;
        $this->stock_api         = new Api\ApiStockController;
        $this->unit_api          = new Api\ApiUnitController;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('incoming-goods.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'item_brands' => $this->item_brand_api->index(),
            'item_groups' => $this->item_group_api->index(),
            'suppliers'   => $this->company_api->getByType('supplier'),
            'units'       => $this->unit_api->index(),
        ];

        return view('incoming-goods.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $purchase = $this->purchase_api->store($request)->getData();

        foreach ($request->item as $key => $value) {
            $request->request->add([
                'count'             => $value['count'],
                'item_id'           => $value['id'],
                'quantifiable_type' => 'App\Models\Purchase',
                'quantifiable_id'   => $purchase->data->id,
                'unit_id'           => $value['unit'],
                'value'             => $value['price'],
                'price'             => $value['price'],
                'sale'              => ($value['sale'] == 1 ? 'unit' : 'subunit')
            ]);

            $capital_price = $this->capital_price_api->store($request);
            $stock         = $this->stock_api->store($request);
            $product_list  = $this->product_list_api->store($request);
        }

        return redirect()->route('stocks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('incoming-goods.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('incoming-goods.create');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return redirect()->route('incoming-goods.create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect()->route('incoming-goods.create');
    }

    /**
     * Mengambil data item yang dipilih.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function chooseItem(Request $request)
    {
        $this->validate($request, [
            'capital_price' => 'required',
            'count'         => 'required',
            'product'       => 'required',
            'unit'          => 'required',
        ]);

        $data['item']              = $this->item_api->show($request['product'])->getData()->data;
        $data['item_count']        = $request['count'];
        $data['item_price']        = $request['capital_price'];
        $data['item_unit']         = $request['unit'];
        $data['item_unit_label']   = $this->unit_api->show($request['unit'])->getData()->data->label;
        $data['item_unit_pieces']  = $this->unit_api->show($request['unit'])->getData()->data->pieces;
        $data['item_pieces_label'] = $this->unit_api->show($request['unit'])->getData()->data->pieces_label;
        $data['price_text']        = number_format($request['capital_price'], 0, ',', '.');
        $data['sale']              = $request['sale'];

        return view('incoming-goods.selected-item', $data);
    }

    /**
     * Pembuatan data untuk select2.
     *
     * @param string $supplier_id
     * @return \Illuminate\Http\Response
     */
    public function getItem($supplier_id)
    {
        $item = Item::where('supplier_id', $supplier_id)->get();

        foreach ($item as $key => $value) {
            $data[] = [
                'id'   => $value->id,
                'text' => $value->name,
            ];
        }

        return $data;
    }

    /**
     * Mengambil data harga modal suatu item.
     *
     * @param string $item_id
     * @return \Illuminate\Http\Response
     */
    public function getCapitalPrice($item_id)
    {
        $data = \App\Models\CapitalPrice::where('item_id', $item_id)->get()->last();

        return $data;
    }


    /**
     * Generate datatable data.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function setDatatable()
    {
        // $goods = $this->incoming_good_api->query([
        //     'purchases',
        //     'supplier',
        // ]);

        $goods = $this->incoming_good_api->query([
            'supplier',
        ]);

        return Datatables::of($goods)
            ->make(true);
    }
}
