<?php

namespace App\Http\Controllers;

use App\Imports\CompanyImport;
use App\Imports\ItemImport;
use App\Imports\ItemBrandImport;
use App\Imports\ItemGroupImport;
use App\Imports\UnitImport;
use App\Models\Item;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;

class ItemController extends Controller
{
    public function __construct(){
        $this->capital_api    = new \App\Http\Controllers\Api\ApiCapitalPriceController;
        $this->company_api    = new \App\Http\Controllers\Api\ApiCompanyController;
        $this->item_api       = new \App\Http\Controllers\Api\ApiItemController;
        $this->unit_api       = new \App\Http\Controllers\Api\ApiUnitController;
        $this->item_brand_api = new \App\Http\Controllers\Api\ApiItemBrandController;
        $this->item_group_api = new \App\Http\Controllers\Api\ApiItemGroupController;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['item_groups'] = $this->item_group_api->index();
        $data['item_brands'] = $this->item_brand_api->index();
        $data['suppliers']   = $this->company_api->getByType('supplier');
        $data['units']       = $this->unit_api->index();

        return view('items.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return redirect()->route('items.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item_group_id = $this->item_group_api->setOrCreate($request->item_group_id);
        $item_brand_id = $this->item_brand_api->setOrCreate($request->item_brand_id);
        $supplier_id   = $this->company_api->setOrCreate($request->supplier_id);

        $request['item_group_id'] = $item_group_id;
        $request['item_brand_id'] = $item_brand_id;
        $request['supplier_id']   = $supplier_id;

        return $this->item_api->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['item'] = $this->item_api->show($id)->getData()->data;

        return view('items._show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('items.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->item_api->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->item_api->destroy($id);
    }

    /**
     * Generate datatable data.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function setDatatable()
    {
        $items = $this->item_api->query([
            'itemGroup',
            'itemBrand',
            'supplier',
            'capitalPrices',
            'sellingPrices'
        ]);

        return Datatables::of($items)
            ->addColumn('capital_price', function (Item $item) {
                $data = $item->capitalPrices->map(function ($price) {
                    return $price->value;
                })->last();

                if (isset($data)) {
                    return $data;
                }
                else {
                    return 0;
                }
            })
            ->addColumn('selling_price', function (Item $item) {
                $data = $item->sellingPrices->map(function ($price) {
                    return $price->value;
                })->last();

                $default = $item->price->map(function ($price) {
                    return $price->selling_price;
                })->last();

                if (isset($data)) {
                    return $data;
                }
                elseif (isset($default)) {
                    return $default;
                }
                else {
                    return 0;
                }
            })
            ->addColumn('action', function ($item) {
                $button  = '';
                $button .= '<button type="button" class="btn btn-link btn-xs mr-xs" name="btn-destroy-item" data-id="' . $item->id . '"><span class="fa fa-trash-o"></span> ' . ucwords(__('hapus')) . '</button>';
                $button .= '<button type="button" class="btn btn-link btn-xs mr-xs" name="btn-edit-item" data-id="' . $item->id . '"><span class="fa fa-edit"></span> ' . ucwords(__('perbarui')) . '</button>';
                $button .= '<button type="button" class="btn btn-link btn-xs" name="btn-show-item" data-id="' . $item->id . '"><span class="fa fa-eye"></span> ' . ucwords(__('lihat')) . '</button>';

                return $button;
            })
            ->make(true);
    }

    /**
     * Fetch id suppliers for item.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fetchIdSuppliersForItem($id)
    {
        $data = $this->item_api->show($id)->getData()->data;

        return response()->json([
            'data' => $data
        ]);
    }

    /**
     * Fetch inventory unit
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchItems()
    {
        $items = Item::pluck('name', 'id');

        return response()->json(['items' => $items]);
    }

    public function import(Request $request)
    {
        $request->validate([
            'item_group_file' => 'required',
            'item_brand_file' => 'required',
            'unit_file'       => 'required',
            'company_file'    => 'required',
        ]);

        $file = $request->file('file');
        $file = [
            'company'    => $request->file('company_file'),
            'item'       => $request->file('item_file'),
            'item_brand' => $request->file('item_brand_file'),
            'item_group' => $request->file('item_group_file'),
            'unit'       => $request->file('unit_file'),
        ];

        Excel::import(new ItemGroupImport, $file['item_group'], null, \Maatwebsite\Excel\Excel::XLSX);
        Excel::import(new ItemBrandImport, $file['item_brand'], null, \Maatwebsite\Excel\Excel::XLSX);
        Excel::import(new UnitImport, $file['unit'], null, \Maatwebsite\Excel\Excel::XLSX);
        Excel::import(new CompanyImport, $file['company'], null, \Maatwebsite\Excel\Excel::XLSX);
        Excel::import(new ItemImport, $file['item'], null, \Maatwebsite\Excel\Excel::XLSX);

        return redirect()->route('items.index');
    }

    public function getSelect2Data($supplier_id)
    {
        $items = $this->item_api->getBySupplier($supplier_id);

        foreach ($items as $key => $value) {
            $data[] = [
                'id'   => $value->id,
                'text' => $value->name,
            ];
        }

        return $data;
    }
}
