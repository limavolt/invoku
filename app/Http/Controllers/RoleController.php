<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Yajra\Datatables\Datatables;

class RoleController extends Controller
{
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        $roles = Role::select('roles.*');
        return Datatables::of($roles)
        ->addColumn('action', function ($role) {
            $btn = '';
            //if (auth()->user()->can('Ubah User')) {
                $btn .= '<button type="button" class="mb-xs mt-xs mr-xs btn btn-xs btn-info" name="btn-edit-role" data-id='.$role->id.'><span class="fa fa-edit"></span> ' . ucwords(__('ubah')) . '</button>';
            //}
                //if (auth()->user()->can('Hapus item_group')) {
                $btn .= '<button type="button" class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" name="btn-destroy-role" data-id='.$role->id.'><span class="fa fa-trash-o"></span> ' . ucwords(__('hapus')) . '</button>';
            //}
            return $btn;
        })
        ->toJson();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['permissions'] = Permission::orderBy('name')->pluck('name', 'id');
        return view('roles.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles',
        ]);

        $role = new Role([
            'name' => ucfirst($request['name']),
        ]);
        $role->save();

        $role->syncPermissions($request->permissions ? $request->permissions : []);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name,'.$id,
        ]);

        $role = Role::find($id);
        $role->update([
            'name' => ucfirst($request['name'])
        ]);

        $role->syncPermissions($request->permissions ? $request->permissions : []);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        DB::table('model_has_roles')->where('role_id', $id)->delete();
        $role->syncPermissions([]);
        $role->delete();

        return response()->json(['message' => 'Role '.$role->name.' berhasil dihapus', 'status' => 'destroyed']);
    }

    /**
     * Fetch id permission for role
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fetchIdPermissionsForRole($id)
    {
        $role = Role::find($id);
        $permissions = $role->permissions()->pluck('id');
        return response()->json(['permission_ids' => $permissions]);
    }

    /**
     * Fetch id permissions from multiple role
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function fetchIdPermissionsForRoles(Request $request)
    {
        $roles = Role::find($request['role_ids']);

        $permission_ids = [];

        if ($roles) {
            foreach ($roles as $role) {
                $permission_id = $role->permissions()->pluck('id')->toArray();
                foreach ($permission_id as $key => $value) {
                    array_push($permission_ids, $value);
                }
            }
        }

        array_unique($permission_ids);

        return response()->json(['permission_ids' => $permission_ids]);
    }
}
