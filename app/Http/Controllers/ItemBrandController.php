<?php

namespace App\Http\Controllers;

use App\Exports\ItemBrandExport;
use App\Exports\ItemBrandTemplateExport;
use App\Http\Controllers\Api\ApiItemBrandController;
use App\Imports\ItemBrandImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;

class ItemBrandController extends Controller
{
    /**
     * Item brand API instance.
     *
     * @var object
     */
    private $brand_api;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->brand_api = new ApiItemBrandController;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('item-brands.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('item-brands.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->brand_api->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('item-brands.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('item-brands.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->brand_api->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->brand_api->destroy($id);
    }

    /**
     * Processing ajax requests from datatables.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        $models = $this->brand_api->query();

        return Datatables::of($models)
            ->addColumn('action', function ($model) {
                $button  = '';
                $button .= '<button type="button" class="btn btn-link btn-xs mb-xs mt-xs mr-xs" name="btn-destroy-item-brand" data-id=' . $model->id . '><span class="fa fa-trash-o"></span> ' . ucwords(__('hapus')) . '</button>';
                $button .= '<button type="button" class="btn btn-link btn-xs mb-xs mt-xs mr-xs" name="btn-edit-item-brand" data-id=' . $model->id . '><span class="fa fa-edit"></span> ' . ucwords(__('perbarui')) . '</button>';
                $button .= '<button type="button" class="btn btn-link btn-xs mb-xs mt-xs mr-xs" name="btn-show-item-brand" data-id=' . $model->id . '><span class="fa fa-eye"></span> ' . ucwords(__('lihat')) . '</button>';

                return $button;
            })
            ->make(true);
    }

    public function getTemplate()
    {
        return Excel::download(new ItemBrandTemplateExport, 'templat-daftar-merek.xlsx');
    }

    /**
     * Export data from database to excel file.
     *
     * @return void
     */
    public function export ()
    {
        return Excel::download(new ItemBrandExport, 'daftar-merek.xlsx');
    }

    /**
     * Import item brand data from an excel file into database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        /* Validate user submitted data. */
        $request->validate([
            'file' => 'required'
        ]);

        /* Import data from user-submitted files into the database. */
        $file = $request->file('file');
        Excel::import(new ItemBrandImport, $file, null, \Maatwebsite\Excel\Excel::XLSX);

        /* Redirect users to the brand list page. */
        return redirect()->route('item-brands.index');
    }
}
