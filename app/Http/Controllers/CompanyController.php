<?php

namespace App\Http\Controllers;

use App\Exports\CompanyExport;
use App\Exports\CompanyTemplate;
use App\Imports\CompanyImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;

class CompanyController extends Controller
{
    /**
     * @var Object $brand_api API untuk pengelolaan brand.
     */
    private $company_api;

    /**
     *
     */
    public function __construct()
    {
        $this->company_api = new Api\ApiCompanyController;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('companies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('companies.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->company_api->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('companies.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('companies.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->company_api->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->company_api->destroy($id);
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function setDatatable()
    {
        $models = $this->company_api->query();

        return Datatables::of($models)
            ->addColumn('type', function ($model) {
                switch ($model->type) {
                    case 'supplier':
                        $data = ucfirst($model->type);
                        break;

                    case 'customer':
                        $data = 'Pelanggan';
                        break;

                    default:
                        $data = 'Supplier dan juga customer';
                        break;
                }

                return $data;
            })
            ->addColumn('action', function ($model) {
                $button  = '';
                $button .= '<button type="button" class="btn btn-xs btn-link mb-xs mt-xs mr-xs" name="btn-destroy-company" data-id=' . $model->id . '><span class="fa fa-trash-o"></span> ' . ucwords(__('hapus')) . '</button>';
                $button .= '<button type="button" class="btn btn-xs btn-link mb-xs mt-xs mr-xs" name="btn-edit-company" data-id=' . $model->id . '><span class="fa fa-edit"></span> ' . ucwords(__('perbarui')) . '</button>';

                return $button;
            })
            ->make();
    }

    public function getTemplate()
    {
        return Excel::download(new CompanyTemplate, 'templat-kontak.xlsx');
    }

    public function export()
    {
        return Excel::download(new CompanyExport, 'daftar-kontak.xlsx');
    }

    public function import(Request $request)
    {
        $request->validate([
            'file' => 'required'
        ]);

        $file = $request->file('file');

        Excel::import(new CompanyImport, $file, null, \Maatwebsite\Excel\Excel::XLSX);

        return redirect()->route('companies.index');
    }

    public function getSelect2Data($type)
    {
        $companies = $this->company_api->getByType($type);

        foreach ($companies as $key => $value) {
            if ($value->address !== null) {
                $data[] = [
                    'id'   => $value->id,
                    'text' => $value->name . ' - ' . $value->address,
                ];
            }
            else {
                $data[] = [
                    'id'   => $value->id,
                    'text' => $value->name,
                ];
            }
        }

        return $data;
    }
}
