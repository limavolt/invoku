<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\ApiItemGroupController;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;

class ItemGroupController extends Controller
{
    /**
     * @var Object $brand_api API untuk pengelolaan brand.
     */
    private $item_group_api;

    /**
     *
     */
    public function __construct()
    {
        $this->item_group_api = new ApiItemGroupController;
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        $models = $this->item_group_api->query();

        return Datatables::of($models)
            ->addColumn('action', function ($model) {
                $button  = '';
                $button .= '<button type="button" class="btn btn-link btn-xs mb-xs mt-xs mr-xs" name="btn-destroy-item-group" data-id=' . $model->id . '><span class="fa fa-trash-o"></span> ' . ucwords(__('hapus')) . '</button>';
                $button .= '<button type="button" class="btn btn-link btn-xs mb-xs mt-xs mr-xs" name="btn-edit-item-group" data-id=' . $model->id . '><span class="fa fa-edit"></span> ' . ucwords(__('perbarui')) . '</button>';
                $button .= '<button type="button" class="btn btn-link btn-xs mb-xs mt-xs mr-xs" name="btn-show-item-group" data-id=' . $model->id . '><span class="fa fa-eye"></span> ' . ucwords(__('lihat')) . '</button>';

                return $button;
            })
            ->filterColumn('parent_name', function ($query, $keyword){
                $query->whereRaw('parent.name LIKE ?', ["%$keyword%"]);
            })
            ->make(true);
    }

    /**
     * Fetch item group
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchItemGroups()
    {
        $item_groups = $this->item_group_api->getByColumn('name', 'id');

        return response()->json(['item_groups' => $item_groups]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('item-groups.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('item-groups.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->item_group_api->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('item-groups.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('item-groups.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->item_group_api->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->item_group_api->destroy($id);
    }

    public function import(Request $request)
    {
        $request->validate([
            'file' => 'required'
        ]);

        $file = $request->file('file');

        Excel::import(new ItemGroupImport, $file, null, \Maatwebsite\Excel\Excel::XLSX);

        return redirect()->route('item-groups.index');
    }

    public function getSelect2Data()
    {
        $groups = $this->item_group_api->index();

        foreach ($groups as $key => $value) {
            $data[] = [
                'id'   => $value->id,
                'text' => $value->name,
            ];
        }

        return $data;
    }
}
