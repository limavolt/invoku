<?php

namespace App\Http\Controllers;

use App\Models\Unit;
use App\Imports\UnitsImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;

class InventoryUnitController extends Controller
{
    private $unit_api;

    public function __construct()
    {
        $this->unit_api = new Api\ApiUnitController;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('inventory-units.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('inventory-units.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->unit_api->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('inventory-units.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('inventory-units.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->unit_api->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->unit_api->destroy($id);
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function setDatatable()
    {
        $units = $this->unit_api->query();

        return Datatables::of($units)
            ->addColumn('action', function ($units) {
                $button  = '';
                $button .= '<button type="button" class="btn btn-link btn-xs mb-xs mt-xs mr-xs" name="btn-destroy-inventory-unit" data-id=' . $units->id . '><span class="fa fa-trash-o"></span> ' . ucwords(__('hapus')) . '</button>';
                $button .= '<button type="button" class="btn btn-link btn-xs mb-xs mt-xs mr-xs" name="btn-edit-inventory-unit" data-id=' . $units->id . '><span class="fa fa-edit"></span> ' . ucwords(__('perbarui')) . '</button>';

                return $button;
            })
            ->make(true);
    }

    /**
     * Fetch inventory unit
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchInventoryUnits()
    {
        $units = $this->unit_api->index();

        // if ($units->count() > 0) {
        //     foreach ($units as $key => $value) {
        //         $data[] = [
        //             'id'   => $value->pieces_label,
        //             'text' => $value->pieces_label
        //         ];
        //     }

        //     return $data->distinct();
        // }

        $labels = $units->pluck('pieces_label')->unique();

        if ($labels->count() > 0) {
            foreach ($labels as $key => $value) {
                $data[] = [
                    'id' => $value,
                    'text' => $value
                ];
            }

            return $data;
        }
    }

    public function getUnit($label)
    {
        $labels = $this->unit_api->getUnit($label);

        foreach ($labels as $key => $value) {
            $data[] = $value;
        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function import(Request $request)
    {
        $request->validate([
            'file' => 'required'
        ]);

        $file = $request->file('file');

        Excel::import(new UnitsImport, $file, null, \Maatwebsite\Excel\Excel::XLSX);

        return redirect()->route('inventory-units.index');
    }

    public function getSelect2Data()
    {
        $units = $this->unit_api->index();

        foreach ($units as $key => $value) {
            $data[] = [
                'id'   => $value->id,
                'text' => $value->label . ' berisi ' . $value->pieces . ' ' . $value->pieces_label,
            ];
        }

        return $data;
    }
}
