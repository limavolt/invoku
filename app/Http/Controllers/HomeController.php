<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $item_api;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->item_api       = new \App\Http\Controllers\Api\ApiItemController;
        $this->item_brand_api = new \App\Http\Controllers\Api\ApiItemBrandController;
        $this->item_group_api = new \App\Http\Controllers\Api\ApiItemGroupController;
        $this->company_api    = new \App\Http\Controllers\Api\ApiCompanyController;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [
            'item_count' => $this->item_api->index()->count(),
            'item_brand_count' => $this->item_brand_api->index()->count(),
            'item_group_count' => $this->item_group_api->index()->count(),
            'supplier_count'   => $this->company_api->getByType('supplier')->count(),
            'customer_count'   => $this->company_api->getByType('consumer')->count(),
        ];

        if (settings()->get('company_name')) {
            $page_title = 'dashboard ' . settings()->get('company_name');
        }
        else {
            $page_title = 'dashboard';
        }

        return view('home', $data)->with(['page_title' => $page_title]);
    }
}
