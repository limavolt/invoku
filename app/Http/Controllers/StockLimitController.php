<?php

namespace App\Http\Controllers;

use App\Models\StockLimit;
use Illuminate\Http\Request;

class StockLimitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StockLimit  $stockLimit
     * @return \Illuminate\Http\Response
     */
    public function show(StockLimit $stockLimit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StockLimit  $stockLimit
     * @return \Illuminate\Http\Response
     */
    public function edit(StockLimit $stockLimit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StockLimit  $stockLimit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StockLimit $stockLimit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StockLimit  $stockLimit
     * @return \Illuminate\Http\Response
     */
    public function destroy(StockLimit $stockLimit)
    {
        //
    }
}
