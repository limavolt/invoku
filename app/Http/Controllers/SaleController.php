<?php

namespace App\Http\Controllers;

use App\Exports\InvoiceExport;
use App\Models\Invoice;
use App\Models\Item;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use PDF;
use Yajra\Datatables\Datatables;

class SaleController extends Controller
{
    private $invoice_api;

    /**
     *
     */
    public function __construct()
    {
        $this->company_api       = new \App\Http\Controllers\Api\ApiCompanyController;
        $this->invoice_api       = new \App\Http\Controllers\Api\ApiInvoiceController;
        $this->item_api          = new \App\Http\Controllers\Api\ApiItemController;
        $this->unit_api          = new \App\Http\Controllers\Api\ApiUnitController;
        $this->product_list_api  = new \App\Http\Controllers\Api\ApiProductListController;
        $this->selling_price_api = new \App\Http\Controllers\Api\ApiSellingPriceController;
        $this->stock_api         = new \App\Http\Controllers\Api\ApiStockController;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['customers'] = $this->company_api->getCustomers();
        $data['invoices']  = $this->invoice_api->index();

        return view('sales.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['customers'] = $this->company_api->getByType('customer');
        $data['items']     = $this->item_api->query('unit');
        $data['units']     = $this->unit_api->index();
        $data['invoices']  = $this->invoice_api->index();
        $data['stocks']    = $this->stock_api->index();

        return view('sales.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['issued_date'] = Carbon::createFromFormat('d/m/Y', $request->issued_date)->format('Y-m-d');
        $request['due_date']    = Carbon::createFromFormat('d/m/Y', $request->due_date)->format('Y-m-d');

        $invoice = $this->invoice_api->store($request)->getData();

        foreach ($request->item as $key => $value) {
            $request->request->add([
                'item_id'           => $value['id'],
                'count'             => -$value['count'],
                'quantifiable_type' => 'App\Models\Invoice',
                'quantifiable_id'   => $invoice->data->id,
                'unit_id'           => $value['unit'],
                'value'             => $value['price'],
                'price'             => $value['price'],
                'invoice_id'        => $invoice->data->id,
                'label'             => $invoice->data->number,
                'stock_id'          => $value['stock_id'],
            ]);

            $this->product_list_api->store($request);
            $this->stock_api->store($request);
            $this->selling_price_api->store($request);
        }

        return redirect()->route('sales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['invoice'] = $this->invoice_api->show($id, true);

        return view('sales.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminte\Http\JsonResponse
     */
    public function setDatatable()
    {
        $models = $this->invoice_api->query(['customer']);

        return Datatables::of($models)
            ->editColumn('number', function ($model) {
                $data  = settings()->get('invoice_code');
                $data .= date('ym', strtotime($model->issued_date));
                $data .= sprintf('%03d', $model->number);

                return $data;
            })
            ->editColumn('issued_date', function ($model) {
                $data = date('d M Y', strtotime($model->issued_date));

                return $data;
            })
            ->editColumn('due_date', function ($model) {
                $data = date('d M Y', strtotime($model->due_date));

                return $data;
            })
            ->editColumn('shipping_date', function ($model) {
                $data = date('d M Y', strtotime($model->shipping_date));

                return $data;
            })
            // ->addColumn('late', function ($model) {
            //     $today    = date_create();
            //     $due_date = date_create($model->due_date);

            //     $interval = date_diff($due_date, $today);
            //     $interval = $interval->format('%R%a');

            //     if ($interval > 0) {
            //         $data = abs($interval) . ' hari';
            //     }
            //     else {
            //         $data = '';
            //     }

            //     return $data;
            // })
            ->editColumn('profit', function ($model) {
                $data = $model->profit;

                return $data;
            })
            ->addColumn('percentage', function ($model) {
                $data = number_format(($model->profit / $model->total) * 100, 2, ',', '.') . '%';

                return $data;
            })
            ->editColumn('status', function ($model) {
                if ($model->status == 'close') {
                    $data = 'Lunas';
                }
                else {
                    if ($model->due_date < \Carbon\Carbon::today()->format('Y-m-d')) {
                        $due   = Carbon::parse($model->due_date);
                        $today = Carbon::now();
                        $data  = $due->diffInDays($today);
                    }
                    else {
                        $data = '-';
                    }
                }


                return $data;
            })
            ->addColumn('action', function ($model) {
                $button  = '';
                $button .= '<button type="button" class="btn btn-link btn-xs" name="settlement-button" data-id="' . $model->id . '"><span class="fa fa-check-circle"></span>' . ucwords(__('lunas')) . '</button>';
                $button .= '<button type="button" class="btn btn-link btn-xs" name="btn-show-invoice" data-id="' . $model->id . '"><span class="fa fa-eye"></span> ' . ucwords(__('lihat')) . '</button>';
                $button .= '<a href="' . route('sales.print', $model->id) . '" target="_blank"><span class="fa fa-print"></span> ' . ucwords(__('cetak')) . '</a>';
                // $button .= '<button type="button" class="btn btn-link btn-xs" name="btn-excel-invoice" data-id="' . $model->id . '"><span class="fa fa-print"></span> ' . ucwords(__('cetak')) . '</button>';

                return $button;
            })
            ->make(true);
    }

    public function chooseItem(Request $request)
    {
        $this->validate($request, [
            'product_id'    => 'required',
            'selling_price' => 'required',
            'count'         => 'required',
            'unit'          => 'required',
        ]);

        $data['price_text']        = number_format($request['selling_price'], 0, ',', '.');
        $data['item_price']        = $request['selling_price'];
        $data['item']              = $this->item_api->show($request['product_id'])->getData()->data;
        $data['item_count']        = $request['count'];
        $data['item_unit']         = $request['unit'];
        $data['item_unit_label']   = $this->unit_api->show($request['unit'])->getData()->data->label;
        $data['item_unit_pieces']  = $this->unit_api->show($request['unit'])->getData()->data->pieces;
        $data['item_pieces_label'] = $this->unit_api->show($request['unit'])->getData()->data->pieces_label;
        $data['stock_id']          = $request['stock_id'];
        $data['capital']           = $request['pieces'] * $request['product_capital'];
        $data['unit_price']        = $request['pieces'] * $request['selling_price'];
        $data['sale']              = $this->stock_api->show($data['stock_id'])->getData()->data->sale;

        return view('sales.selected-item', $data);
    }

    public function getSellingPrice($item_id)
    {
        $data = \App\Models\SellingPrice::where('item_id', $item_id)->get()->last();

        return $data;
    }

    public function getDefaultSellingPrice($item_id)
    {
        $data = \App\Models\DefaultPrice::where('item_id', $item_id)->get()->last();

        return $data;
    }

    public function getCustomerSellingPrice($customer_id, $item_id)
    {
        /* $invoices = \App\Models\Invoice::where('customer_id', $customer_id)->get();

        foreach ($invoices as $invoice) {
            if (\App\Models\ProductList::where('quantifiable_id', $invoice->id)->get()) {
                $products[] = \App\Models\ProductList::where('stock_id', $item_id)->where('quantifiable_type', 'like', "%Invoice")->where('quantifiable_id', $invoice->id)->where('price', '<>', '')->get();
            }
        } */

        $products = \App\Models\ProductList::where('stock_id', $item_id)->where('quantifiable_type', 'like', "%Invoice")->get();
        $invoices = \App\Models\Invoice::where('customer_id', $customer_id)->get();

        if ($products) {
            foreach ($invoices as $invoice) {
                $selected[] = $products->where('quantifiable_id', $invoice->id);
            }

            $filtered = (array)$selected;
            // $filtered = array_filter($filtered);
            $filtered = array_where($filtered, function ($value, $key) {
                $value = (array)$value;
                $value = array_filter($value);

                return $value;
            });

            $last = last($filtered);

            if ($last) {
                foreach ($last as $value) {
                    $data = $value['price'];
                }

                return $data;
            }
        }
    }

    public function print($id)
    {
        $data['invoice'] = $this->invoice_api->show($id, true);

        $pdf = PDF::loadView('sales.print', $data);

        // return view('sales.print', $data);
        return $pdf->stream();
    }

    public function settlement(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required',
        ]);

        $invoice = Invoice::find($id);

        $invoice->status = $request->status;

        $invoice->save();

        return redirect()->route('sales.index');
    }

    public function export($id)
    {
        return Excel::download(new InvoiceExport($id), 'invoices.xlsx');
    }
}
