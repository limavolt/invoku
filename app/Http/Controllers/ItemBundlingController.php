<?php

namespace App\Http\Controllers;

use App\Models\ItemBundling;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class ItemBundlingController extends Controller
{
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        $item_bundlings = ItemBundling::with('inventoryUnit')->select('item_bundlings.*');
        return Datatables::of($item_bundlings)
        ->addColumn('action', function ($item_bundling) {
            $btn = '';
            //if (auth()->user()->can('Ubah item_group')) {
                $btn .= '<button type="button" class="mb-xs mt-xs mr-xs btn btn-sm btn-info" name="btn-edit-item-bundling" data-id='.$item_bundling->id.'>Ubah</button>';
            //}
                //if (auth()->user()->can('Hapus item_group')) {
                $btn .= '<button type="button" class="mb-xs mt-xs mr-xs btn btn-sm btn-danger" name="btn-destroy-item-bundling" data-id='.$item_bundling->id.'>Hapus</button>';
            //}
            return $btn;
        }) 
        ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('item-bundlings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'inventory_unit_id' => 'required',
            'name' => 'required'
        ]);

        $item_bundling = ItemBundling::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'inventory_unit_id' => 'required',
            'name' => 'required'
        ]);

        $item_bundling = ItemBundling::find($id);
        $item_bundling->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item_bundling = ItemBundling::find($id);
        $item_bundling->items()->detach();
        $item_bundling->delete();

        return response()->json(['message' => 'Item bundling '.$item_bundling->name.' berhasil dihapus', 'status' => 'destroyed']);
    }
}
