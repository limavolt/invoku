<?php

namespace App\Http\Controllers;

use App\Imports\CapitalPriceImport;
use App\Imports\PriceImport;
use App\Imports\PurchaseImport;
use App\Imports\ProductListImport;
use App\Imports\StockImport;
use App\Models\Item;
use App\Models\Stock;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;

class StockController extends Controller
{
    /**
     * StockController class constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->company_api    = new \App\Http\Controllers\Api\ApiCompanyController;
        $this->item_api       = new \App\Http\Controllers\Api\ApiItemController;
        $this->item_brand_api = new \App\Http\Controllers\Api\ApiItemBrandController;
        $this->item_group_api = new \App\Http\Controllers\Api\ApiItemGroupController;
        $this->purchase_api   = new \App\Http\Controllers\Api\ApiPurchaseController;
        $this->stock_api      = new \App\Http\Controllers\Api\ApiStockController;
        $this->unit_api       = new \App\Http\Controllers\Api\ApiUnitController;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data['item_groups'] = $this->item_group_api->index();
        $data['item_brands'] = $this->item_brand_api->index();
        $data['suppliers']   = $this->company_api->getByType('supplier');
        $data['units']       = $this->unit_api->index();

        return view('stocks.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('stocks.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return redirect()->route('stocks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('stocks.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('stocks.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return redirect()->route('stocks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect()->route('stocks.index');
    }

    public function setDatatable()
    {
        $items = \App\Models\Stock::query()->with('item');

        return Datatables::of($items)
            ->addColumn('categories', function ($item) {
                return $item->item->itemGroup->name;
            })
            ->addColumn('brands', function ($item) {
                return $item->item->itemBrand->name;
            })
            ->addColumn('units', function ($item) {
                return $item->item->unit->label . ' @ ' . $item->item->unit->pieces . ' ' . $item->item->unit->pieces_label;
            })
            ->addColumn('values', function ($item) {
                if ($item->sale == 'unit') {
                    return $item->price * $item->count;
                }
                else {
                    return $item->price * $item->item->unit->pieces * $item->count;
                }
            })
            ->addColumn('price_label', function ($item) {
                if ($item->sale == 'unit') {
                    return 'per ' . strtolower($item->item->unit->label);
                }
                else {
                    return 'per ' . strtolower($item->item->unit->pieces_label);
                }
            })
            ->make();

        // return Datatables::of($query)
        // ->addColumn('modal', function (Item $item) {
        //     $data = $item->capitalPrices->map(function ($price) {
        //         return $price->value;
        //     })->last();

        //     if (isset($data)) {
        //         return $data;
        //     }
        //     else {
        //         return 0;
        //     }
        // })
        // ->addcolumn('stock', function (Item $item) {
        //     $data = $item->stocks->map(function ($stock) {
        //         return $stock->count;
        //     })->sum();

        //     return $data;
        // })
        // ->addColumn('unit', function (Item $item) {
        //     $data = $item->stocks->map(function ($stock) {
        //         return $stock->unit->label . ' berisi ' . $stock->unit->pieces . ' ' . $stock->unit->pieces_label;
        //     })->last();

        //     return $data;
        // })
        // ->addColumn('value', function (Item $item) {
        //     $capital_price = $item->capitalPrices->map(function ($price) {
        //         return $price->value;
        //     })->last();

        //     $stock = $item->stocks->map(function ($stock) {
        //         return $stock->count;
        //     })->sum();

        //     $value = $capital_price * $stock;

        //     return $value;
        // })
        // ->make(true);
    }

    public function import(Request $request)
    {
        $request->validate([
            'purchase_file'     => 'required',
            'product_list_file' => 'required',
            'price_file'        => 'required',
        ]);

        $file = [
            'purchase'     => $request->file('purchase_file'),
            'product_list' => $request->file('product_list_file'),
            'price'        => $request->file('price_file'),
        ];

        Excel::import(new PurchaseImport, $file['purchase'], null, \Maatwebsite\Excel\Excel::XLSX);
        Excel::import(new StockImport, $file['product_list'], null, \Maatwebsite\Excel\Excel::XLSX);
        Excel::import(new ProductListImport, $file['product_list'], null, \Maatwebsite\Excel\Excel::XLSX);
        Excel::import(new CapitalPriceImport, $file['product_list'], null, \Maatwebsite\Excel\Excel::XLSX);
        Excel::import(new PriceImport, $file['price'], null, \Maatwebsite\Excel\Excel::XLSX);

        return redirect()->route('stocks.index');
    }

    /**
     * Mengambil data harga modal suatu item.
     *
     * @param string $item_id
     * @return \Illuminate\Http\Response
     */
    public function getStock($item_id)
    {
        $data = \App\Models\Stock::where('item_id', $item_id)->get();

        return $data;
    }

}
