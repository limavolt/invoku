<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Valuestore\Valuestore;

class SettingController extends Controller
{
    public function index()
    {
        $settings = Valuestore::make(storage_path('app/settings.json'));

        return view('settings.index', ['settings' => $settings]);
    }

    public function store(Request $request)
    {
        $settings = Valuestore::make(storage_path('app/settings.json'));

        if ($request['company_name']) {
            $settings->put('company_name', $request['company_name']);

            $data['company_name'] = $request['company_name'];
        }

        if ($request['company_address']) {
            $settings->put('company_address', $request['company_address']);

            $data['company_address'] = $request['company_address'];
        }

        if ($request['invoice_code']) {
            $settings->put('invoice_code', $request['invoice_code']);

            $data['invoice_code'] = $request['invoice_code'];
        }

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    public function import($type, Request $request)
    {
        $request->validate(['file' => 'required']);

        $file = $request->file('file');
        Excel::import(new \App\Imports\InitialDataImport, $file, null, \Maatwebsite\Excel\Excel::XLSX);

        return redirect()->route('settings.index');
    }
}
