<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
    private $default_password;

    public function __construct()
    {
        $this->default_password = env('DEFAULT_PASSWORD');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('users.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required',
            'email'    => 'required|email|unique:users',
            'username' => 'required|unique:users',
        ]);

        $user = new User([
            'name'       => $request['name'],
            'email'      => $request['email'],
            'username'   => $request['username'],
            'password'   => bcrypt($this->default_password),
        ]);

        $data = $user->save();

        return response()->json([
            'status' => 'success',
            'data'   => [
                'name'     => $request['name'],
                'username' => $request['username'],
                'email'    => $request['email']
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('users.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('users.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'email'    => 'required|email|unique:users,email,' . $id,
            'username' => 'required|unique:users,username,' . $id,
            'password' => 'confirmed'
        ]);

        $user = User::find($id);
        $data = $user->update([
            'name'       => $request['name'],
            'email'      => $request['email'],
            'username'   => $request['username']
        ]);

        if ($request['password']) {
            $data = $user->update([
                'password' => bcrypt($request['password']),
            ]);
        }

        return response()->json([
            'status' => 'success',
            'data'   => [
                'name'     => $request['name'],
                'username' => $request['username'],
                'email'    => $request['email']
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (auth()->user()->id == $id) {
            return response()->json([
                'status'  => 'error',
                'message' => "Maaf, kamu tidak boleh menghapus dirimu sendiri.",
            ]);
        }
        else {
            if ($user->count === 1) {
                return response()->json([
                    'status' => 'error',
                    'message' => "Maaf, pengguna $user->name tidak boleh dihapus karena merupakan satu-satunya pengguna di sini.",
                ]);
            }
            else if ($user->username == 'admin' || $user->id === 1) {
                return response()->json([
                    'status'  => 'error',
                    'message' => "Maaf, pengguna $user->name tidak diperbolehkan dihapus karena seorang admin utama.",
                ]);
            }
            else {
                $user->delete();

                return response()->json([
                    'status'  => 'success',
                    'message' => "Pengguna $user->name berhasil dihapus.",
                ]);
            }
        }
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function setDatatable()
    {
        $users = User::get();

        return datatables()->of($users)
            ->addColumn('action', function ($user) {
                $button  = '';
                $button .= '<button class="btn btn-link btn-xs" name="btn-edit-user" type="button" data-id='.$user->id.'><span class="fa fa-edit"></span> ' . ucwords(__('ubah')) . '</button>';
                $button .= '|';
                $button .= '<button class="btn btn-link btn-xs" name="btn-destroy-user" type="button" data-id='.$user->id.'><span class="fa fa-trash-o"></span> ' . ucwords(__('hapus')) . '</button>';

                return $button;
            })
            ->make();
    }

}
