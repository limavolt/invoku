<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ItemBrandResource;
use App\Models\ItemBrand;
use Illuminate\Http\Request;

class ApiItemBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ItemBrandResource::collection(ItemBrand::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            ['name' => 'required|unique:item_brands']
        );

        $data = ItemBrand::create($request->all());

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ItemBrand::findOrFail($id);

        return response()->json(['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            ['name' => 'required|unique:item_brands,name,' . $id]
        );

        $data = ItemBrand::findOrFail($id);

        $data->update($request->all());

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item_brand = ItemBrand::findOrFail($id);

        if ($item_brand->items->count() > 0) {
            return response()->json([
                'status'  => 'canceled',
                'message' => 'Merek ' . $item_brand->name . ' masih digunakan dalam produk.',
                'data'    => null
            ]);
        }
        else {
            $item_brand->delete();

            return response()->json([
                'status'  => 'success',
                'message' => 'Merek ' . $item_brand->name . ' berhasil dihapus',
                'data'    => null
            ]);
        }
    }

    /**
     * Ambil data resource untuk digunakan oleh datatable.
     *
     * @param array $relation Relasi yang didefinisikan pada model resource.
     */
    public function query($relation = [])
    {
        if (isset($relation) || '' !== $relation) {
            $data = ItemBrand::with($relation)->selectRaw('distinct item_brands.*');
        }
        else {
            $data = ItemBrand::query();
        }

        return $data;
    }

    public function setOrCreate($value)
    {
        $model = ItemBrand::find($value);

        if (null === $model) {
            $request = new Request();

            $request->query->add(['name' => $value]);
            $result = $this->store($request);

            $data = $result->getData()->data->id;
        }
        else {
            $data = $value;
        }

        return $data;
    }
}
