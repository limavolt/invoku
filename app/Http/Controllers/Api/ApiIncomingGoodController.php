<?php

namespace App\Http\Controllers\Api;

use App\Models\Purchase;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiIncomingGoodController extends Controller
{
    public function __construct()
    {
        $this->purchase_api = new \App\Http\Controllers\Api\ApiPurchaseController;
        $this->product_list_api = new \App\Http\Controllers\Api\ApiProductListController;
        $this->stock_api = new \App\Http\Controllers\Api\ApiStockController;
        $this->capital_price_api = new \App\Http\Controllers\Api\ApiCapitalPriceController;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['arrival_date'] = Carbon::createFromFormat('d-m-Y', $request->arrival_date)->format('Y-m-d');

        $purchase = $this->purchase_api->store($request)->getData();

        $request->request->add(['quantifiable_type' => 'App\Models\Purchase']);
        $request->request->add(['quantifiable_id' => $purchase->data->id]);

        $product_list = $this->product_list_api->store($request);

        $stock = $this->stock_api->store($request);

        $capital_price = $this->capital_price_api->store($request);

        return response()->json([
            'status' => 'success',
            'data' => $purchase->data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function query($relation = [])
    {
        if (isset($relation) || '' !== $relation) {
            $data = Purchase::with($relation)->get();
        }
        else {
            $data = Purchase::query();
        }

        return $data;
    }

}
