<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\InvoiceResource;
use App\Models\Invoice;


class ApiInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return InvoiceResource::collection(Invoice::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'customer_id' => 'required',
            'issued_date' => 'required|date',
            'number'      => 'required',
            'due_date'    => 'required',
            'total'       => 'required',
        ]);

        $data = Invoice::create($request->all());

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $query = false)
    {
        $data = Invoice::find($id);

        if (false == $query) {
            return response()->json([
                'status' => 'success',
                'data' => $data
            ]);
        }
        else {
            return $data;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'customer_id' => 'required',
            'issued_date' => 'required|date',
            'number' => 'required',
            'shipping_date' => 'required',
            'due_date' => 'required',
        ]);

        $data = Invoice::findOrFail($id);

        $data->update($request->all());

        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Invoice::findOrFail($id);

        $data->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Data berhasil dihapus',
            'data' => null
        ]);
    }

    /**
     * Ambil data resource untuk digunakan oleh datatable.
     *
     * @param string[] $relation Relasi yang didefinisikan pada model resource.
     * @return \Illuminate\Http\Response
     */
    public function query($relation = [])
    {
        if (isset($relation) || '' !== $relation) {
            $data = Invoice::with($relation)->get();
        }
        else {
            $data = Invoice::query();
        }

        return $data;
    }
}
