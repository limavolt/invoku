<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use App\Models\Company;

class ApiCompanyController extends Controller
{
    //START RESOURCE CONTROLLER
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CompanyResource::collection(Company::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
        ]);

        if (count($request->type) == 2) {
            $type = 'both';
        }
        else {
            $type = implode(',', $request->type);
        }

        $data = Company::create([
            'name'    => $request->name,
            'type'    => $type,
            'address' => $request->address,
            'pic'     => $request->pic,
        ]);

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Company::findOrFail($id);

        return response()->json(['data'   => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $data = Company::find($id);

        if (null !== $request->name) {
            $data->name = $request->name;
        }

        if (null !== $request->pic) {
            $data->pic = $request->pic;
        }

        if (null !== $request->address) {
            $data->address = $request->address;
        }

        if (null !== $request->type) {
            if (count($request->type) == 1) {
                foreach ($request->type as $type => $value) {
                    $data->type = $value;
                }
            }
            else {
                $data->type = 'both';
            }
        }

        $data->save();

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Company::find($id);

        if ($data->items->count() > 0) {
            return response()->json([
                'status'  => 'canceled',
                'message' => 'Supplier ' . $data->name . ' masih memiliki produk yang terdaftar.',
                'data'    => null,
            ]);
        }
        else {
            $data->delete();

            return response()->json([
                'status'  => 'success',
                'message' => 'Supplier ' . $data->name . ' berhasil dihapus.',
                'data'    => null
            ]);
        }
    }

    //END RESOURCE CONTROLLER

    public function query($relation = [])
    {
        if (isset($relation) || '' !== $relation) {
            $data = Company::with($relation)->selectRaw('distinct companies.*');
        }
        else {
            $data = Company::query();
        }

        return $data;
    }

    /**
     * Untuk mendapatkan daftar cunsomers.
     * Customer diambil dari Company yang memiliki tipe 'cunsomer' atau 'both'.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCustomers()
    {
        $customers = Company::whereIn('type', ['customer', 'both'])->get();
        return CompanyResource::collection($customers);
    }

    /**
     * Untuk mendapatkan daftar supplier.
     * Supplier diambil dari Company yang memiliki tipe 'supplier' atau 'both'.
     *
     * @return \Illuminate\Http\Response
     */
    public function fetchSuppliers()
    {
        $suppliers = Company::whereIn('type', ['supplier', 'both'])->get();

        return CompanyResource::collection($suppliers);
    }

    public function getByType($type, $collection = true)
    {
        $data = Company::whereIn('type', [$type, 'both'])->get();

        if (true == $collection) {
            return CompanyResource::collection($data);
        }
        else {
            return $data;
        }
    }

    public function setOrCreate($value)
    {
        $model = Company::find($value);

        if (null === $model) {
            $request = new Request();

            $request->query->add(['name' => $value]);
            $result = $this->store($request);

            $data = $result->getData()->data->id;
        }
        else {
            $data = $value;
        }

        return $data;
    }
}
