<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiOutgoingGoodController extends Controller
{
    public function __construct()
    {
        $this->invoice_api = new \App\Http\Controllers\Api\ApiPurchaseController;
        $this->product_list_api = new \App\Http\Controllers\Api\ApiProductListController;
        $this->stock_api = new \App\Http\Controllers\Api\ApiStockController;
        $this->selling_price_api = new \App\Http\Controllers\Api\ApiSellingPriceController;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['arrival_date'] = Carbon::createFromFormat('d-m-Y', $request->arrival_date)->format('Y-m-d');

        $invoice = $this->invoice_api->store($request)->getData();

        $request->request->add(['quantifiable_type' => 'App\Models\Invoice']);
        $request->request->add(['quantifiable_id' => $invoice->data->id]);

        $product_list = $this->product_list_api->store($request);

        $stock = $this->stock_api->store($request);

        $selling_price = $this->selling_price_api->store($request);

        return response()->json([
            'status' => 'success',
            'data' => $invoice->data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
