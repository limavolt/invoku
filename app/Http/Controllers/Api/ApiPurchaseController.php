<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PurchaseResource;
use App\Models\Purchase;

class ApiPurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PurchaseResource::collection(Purchase::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['arrival_date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $request->arrival_date)->format('Y-m-d');

        $this->validate($request, [
            'supplier_id'  => 'required',
            'arrival_date' => 'required|date'
        ]);

        $data = Purchase::create($request->all());

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Purchase::findOrFail($id);

        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'supplier_id' => 'required',
            'arrival_date' => 'required|date'
        ]);

        $data = Purchase::findOrFail($id);

        $data->update($request->all());

        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Purchase::findOrFail($id);

        $data->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Data berhasil dihapus',
            'data' => null
        ]);
    }

    public function query($relations = [])
    {
        if (isset($relations) || '' !== $relations) {
            $data = Purchase::query()->with($relations);
        }
        else {
            $data = Purchase::query();
        }

        return $data;
    }
}
