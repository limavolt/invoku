<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductListResource;
use App\Models\ProductList;
use App\Models\Stock;
use Illuminate\Http\Request;

class ApiProductListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ProductListResource::collection(ProductList::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'item_id'           => 'required',
            'count'             => 'required|numeric',
            'price'             => 'required|numeric',
            'quantifiable_type' => 'required',
            'quantifiable_id'   => 'required|numeric',
        ]);

        if ($request->quantifiable_type == 'App\Models\Purchase') {
            $stock = Stock::where('item_id', $request->item_id)->where('price', $request->price)->first();

            $request->request->add([
                'stock_id' => $stock->id,
            ]);
        }

        $data = ProductList::create($request->all());

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ProductList::findOrFail($id);

        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'item_id' => 'required',
            'count' => 'required|numeric',
            'quantifiable_type' => 'required|in:App\Purchase',
            'quantifiable_id' => 'required|numeric',
        ]);

        $data = ProductList::findOrFail($id);
        $data->update($request->all());

        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = ProductList::findOrFail($id);

        $data->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Data berhasil dihapus',
            'data' => null
        ]);
    }
}
