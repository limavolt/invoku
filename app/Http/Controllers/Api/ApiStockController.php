<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\StockResource;
use App\Models\Stock;

class ApiStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return StockResource::collection(Stock::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'item_id' => 'required',
            'price'   => 'required',
        ]);

        if ($request->quantifiable_type == 'App\Models\Invoice') {
            $stock = Stock::find($request->stock_id);

            $request->merge(['count' => $request['count'] + $stock->count]);
            $request->merge(['price' => $stock->price]);

            $data = $stock->update($request->all());
        }
        else {
            $stock = Stock::where('item_id', $request->item_id)->where('price', $request->price)->first();

            if ($stock) {
                $request->merge(['count' => $request['count'] + $stock->count]);

                $data = $stock->update($request->all());
            }
            else {
                $data = Stock::create($request->all());
            }
        }

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Stock::with(['item'])->find($id);

        return response()->json([
            'status' => 'success',
            'data'   => $data,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'item_id' => 'required',
            'price'   => 'required',
        ]);

        $request->count = 0;

        $data = Stock::findOrFail($id);

        $data->update($request->all());

        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Stock::findOrFail($id);

        $data->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Data berhasil dihapus',
            'data' => null
        ]);
    }


    public function query($relation = [])
    {
        if (isset($relation) || '' !== $relation) {
            $data = Stock::query()->with($relation);
        }
        else {
            $data = Stock::query();
        }

        return $data;
    }

    public function fetchSelect2Data()
    {
        $stocks = Stock::where('count', '<>', 0)->orderBy('item_id', 'asc')->get();

        foreach ($stocks as $key => $value) {
            $data[] = [
                'id'   => $value->id,
                'text' => $value->item->name . ' (Modal: Rp' . number_format($value->price, 0, ',', '.') . '. Jumlah: ' . number_format($value->count, 0, ',', '.') . ' ' . strtolower($value->item->unit->label) . ')',
            ];
        }

        return $data;
    }

    public function fetchSelect2Item($item_id)
    {
        $stocks = Stock::where('item_id', $item_id)->get();

        foreach ($stocks as $key => $value) {
            $data[] = [
                'id'   => $value->id,
                'text' => 'Rp' . $value->price . ' berjumlah ' . $value->count,
            ];
        }

        return $data;
    }

    public function getByPrice($price)
    {
        $stock = Stock::where('price', $price)->with('item')->first();

        return response()->json([
            'status' => 'success',
            'message' => 'Data berhasil dipilih',
            'data' => $stock
        ]);
    }

    public function getSale($item_id)
    {
        $stock = Stock::where('item_id', $item_id)->first();

        return response()->json([
            'status' => 'success',
            'data'   => $stock
        ]);
    }
}
