<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SupplierResource;
use App\Models\Supplier;
use Illuminate\Http\Request;

class ApiSupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SupplierResource::collection(Supplier::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            ['code' => 'required|unique:suppliers',
            'name' => 'required']
        );

        $supplier = Supplier::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Supplier::findOrFail($id);

        return response()->json(['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            ['code' => 'required|unique:suppliers,code,' . $id,
            'name' => 'required']
        );

        $data = Supplier::findOrFail($id);

        $data->update($request->all());

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = Supplier::findOrFail($id);

        if ($supplier->items->count() > 0) {
            return response()->json([
                'status'  => 'canceled',
                'message' => 'Supplier ' . $supplier->name . ' masih menyuplai item untuk kita.',
                'data'    => null,
            ]);
        }
        else {
            $supplier->delete();

            return response()->json([
                'status'  => 'success',
                'message' => 'Supplier ' . $supplier->name . ' berhasil dihapus.',
                'data'    => null,
            ]);
        }
    }

    public function setOrCreate($value)
    {
        $model = Supplier::find($value);

        if (null === $model) {
            $request = new Request();

            $request->query->add(['name' => $value]);
            $result = $this->store($request);

            $data = $result->getData()->data->id;
        }
        else {
            $data = $value;
        }

        return $data;
    }
}
