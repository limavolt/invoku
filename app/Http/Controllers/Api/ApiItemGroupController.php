<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ItemGroupResource;
use App\Models\ItemGroup;
use Illuminate\Http\Request;

class ApiItemGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ItemGroupResource::collection(ItemGroup::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            ['name' => 'required|unique:item_groups']
        );

        $data = ItemGroup::create($request->all());

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ItemGroup::findOrFail($id);

        return response()->json(['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            ['name'     => 'required|unique:item_groups,name,' . $id,
            'parent_id' => 'not_in:' . $id]
        );

        $data = ItemGroup::findOrFail($id);
        $data->update($request->all());

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $parents = ItemGroup::where('parent_id', $id)->count();
        $data    = ItemGroup::findOrFail($id);

        if ($parents > 0 || $data->items()->count() > 0) {
            if ($parents > 0) {
                return response()->json([
                    'status'  => 'canceled',
                    'message' => 'Kategori produk ' . $data->name . ' merupakan induk kategori.',
                    'data'    => null
                ]);
            }else{
                return response()->json([
                    'status'  => 'canceled',
                    'message' => 'Kategori produk ' . $data->name . ' masih dimiliki produk yang lain.',
                    'data'    => null
                ]);
            }
        }
        else {
            if ($data->items->count() > 0) {
                return response()->json([
                    'status'  => 'canceled',
                    'message' => 'Kategori produk ' . $data->name . ' masih digunakan dalam produk.',
                    'data'    => null
                ]);
            }
            else {
                $data->delete();

                return response()->json([
                    'status'  => 'success',
                    'message' => 'Kategori produk ' . $data->name . ' berhasil dihapus.',
                    'data'    => null
                ]);
            }
        }
    }

    /**
     * Ambil data resource untuk digunakan oleh datatable.
     *
     * @param array $relation Relasi yang didefinisikan pada model resource.
     */
    public function query()
    {
        $data = ItemGroup::
            leftJoin('item_groups as parent', 'item_groups.parent_id', '=', 'parent.id')
            ->select('item_groups.*', 'parent.name as parent_name');

        return $data;
    }

    /**
     * Ambil data resource pada kolom yang diperlukan saja.
     *
     * @param array $column Nama kolom yang dipilih.
     */
    public function getByColumn($columns = [])
    {
        $columns = implode(', ', $columns);

        $data = ItemGroup::pluck($columns);

        return $data;
    }

    public function setOrCreate($value)
    {
        $model = ItemGroup::find($value);

        if (null === $model) {
            $request = new Request();

            $request->query->add(['name' => $value]);
            $result = $this->store($request);

            $data = $result->getData()->data->id;
        }
        else {
            $data = $value;
        }

        return $data;
    }
}
