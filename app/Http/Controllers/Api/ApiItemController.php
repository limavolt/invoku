<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ItemResource;
use App\Models\Item;
use Illuminate\Http\Request;

class ApiItemController extends Controller
{
    // START RESOURCE CONTROLLER

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ItemResource::collection(Item::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required',
            'item_group_id' => 'required',
            'item_brand_id' => 'required',
            'supplier_id'   => 'required',
            'unit_id'       => 'required'
        ]);

        //Hanya simpan item yang tidak memiliki nama dan brand yang sama pada database
        //Jika menggunakan kode ini belum bisa memberikan response untuk mengetahui apakah data yang dimasukkan sudah ada atau belum di database
        /*$item = Item::firstOrCreate([
            'name' => $request->name,
            'item_brand_id' => $request->item_brand_id,
        ], $request->all());*/

        //Membuat fungsi sendiri untuk mengatasi masalah diatas
        $this->isDataExist($request->only(['name', 'supplier_id']), null);

        //Jika data belum ditemukan, simpan data
        $item = Item::create($request->all());

        return response()->json([
            'status' => 'success',
            'data'   => $item
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Item::with(['itemGroup', 'itemBrand', 'supplier', 'unit', 'price', 'stocks'])->findOrFail($id);

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'          => 'required',
            'item_group_id' => 'required',
            'item_brand_id' => 'required',
            'supplier_id'   => 'required',
            'unit_id' => 'required'
        ]);

        $data = Item::findOrFail($id);

        $this->isDataExist($request->only(['name', 'item_brand_id']), $data);

        $data->update($request->all());

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Item::findOrFail($id);
        $data->delete();

        return response()->json([
            'status'  => 'success',
            'message' => 'Produk ' . $data->name . ' berhasil dihapus',
            'data'    => null
        ]);
    }

    // END RESOURCE CONTROLLER

    /**
     * Untuk mendapatkan daftar barang berdasarkan supplier.
     *
     * @return \Illuminate\Http\Response
     */
    public function getItemsBySupplier($supplier_id)
    {
        $items = Item::where('supplier_id', $supplier_id)->orderBy('name', 'asc')->get();
        return ItemResource::collection($items);
    }

    /**
     * Digunakan untuk mengecek apakah data "A" sudah ada pada database
     * Apabila ke depannya penggunaan fungsi ini lebih kompleks, bisa dipelajari untuk menggunakan custom validation rules
     *
     * @param  array/object  $param
     * @param \App\Models\Item $item (data pembanding)
     * @return \Illuminate\Http\Response
     */
    public function isDataExist($param, $item)
    {
        $is_exist = Item::where($param)->first();

        //Jika ditemukan data dan data yang ditemukan tidak sama dengan data pembanding, maka duplikasi data
        //Tetapi jika data yang ditemukan adalah sama dengan data pembanding, berarti data yang akan diubah bukan duplikasi data

        if ($is_exist) {
            if (is_null($item)) {
                return response()->json([
                    'message' => 'Data yang diberikan tidak sah.',
                    'errors'  => [
                        'name' => ['Produk ' . $is_exist->name . ' dengan merek ' . $is_exist->itemBrand->name . ' telah terdaftar.']
                    ]
                ], 422)->throwResponse();
            }
            else if ($is_exist->id !== $item->id) {
                return response()->json([
                    'message' => 'Data yang diberikan tidak sah.',
                    'errors'  => [
                        'name' => ['Produk ' . $is_exist->name . ' dengan merek ' . $is_exist->itemBrand->name . ' telah terdaftar.']
                    ]
                ], 422)->throwResponse();
            }
        }

        // if (($is_exist && is_null($item) ) || ($item && $is_exist->id != $item->id)) {
        //         //Karena status yang dikembalikan adalah 422, maka struktur response yang dikembalikan juga menyesuaikan seperti response 422
        //     return response()->json([
        //         'message' => 'The given data was invalid',
        //         'errors' => [
        //             'name' => [
        //                 'Produk '.$is_exist->name.' dengan merek '.$is_exist->itemBrand->name.' telah ada di sistem',
        //             ]
        //         ]
        //     ], 422)->throwResponse();
        // }
    }

    public function query($relation = [])
    {
        if (isset($relation) || '' !== $relation) {
            $data = Item::with($relation)->selectRaw('distinct items.*');
        }
        else {
            $data = Item::query();
        }

        return $data;
    }

    public function getCapital($id)
    {
        $data  = Item::findOrFail($id)->capitalPrices->last();

        return $data;
    }

    public function getById($id, $json = true)
    {
        $data = Item::where('id', $id)->get();

        if ($json) {
            return response()->json([
                'status' => 'success',
                'data'   => $data,
            ]);
        }
        else {
            return $data;
        }
    }

    public function fetchSelect2Data()
    {
        $items = Item::get();

        foreach ($items as $key => $value) {
            $data[] = [
                'id'   => $value->id,
                'text' => $value->name
            ];
        }

        return $data;
    }

    public function getBySupplier($supplier_id, $collection = true)
    {
        $data = Item::where('supplier_id', $supplier_id)->orderBy('name', 'asc')->get();

        if (true == $collection) {
            return ItemResource::collection($data);
        } else {
            return $data;
        }
    }
}
