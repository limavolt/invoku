<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemBrand extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'description'];

    /**
     * Get the items for the item brand.
     */
    public function items()
	{
		return $this->hasMany('App\Models\Item');
	}

	/**
     * Get the item brand's description via accessor.
     * If no description is stored then return the "-" character.
     *
     * @param  string  $value
     * @return string
     */
    public function getDescriptionAttribute($value)
    {
    	if ($value) {
    		return $value;
        }
        else {
    		return '-';
    	}
    }
}
