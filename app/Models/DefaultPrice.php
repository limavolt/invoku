<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DefaultPrice extends Model
{
    /**
     * List of mass-assigned model attributes
     *
     * @var string[]
     */
	protected $fillable = [
        'item_id',
        'capital_price',
        'selling_price',
    ];

    public function item()
    {
        return $this->belongsTo('App\Models\Item');
    }
}
