<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SellingPrice extends Model
{
    protected $fillable = [
    	'item_id',
    	'invoice_id',
    	'label',
    	'value'
    ];

    // RELATION
    public function item()
    {
    	return $this->belongsTo('App\Models\Item');
    }

    public function invoice()
    {
    	return $this->belongsTo('App\Models\Invoice');
    }
    // END OF RELATION
}
