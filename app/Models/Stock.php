<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'item_id',
        'count',
        'price',
        'sale',
    ];

    /**
     * Get the items of the stock.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item()
    {
    	return $this->belongsTo('App\Models\Item');
    }

    /**
     * Get the list of products that use the stock.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function quantities()
    {
        return $this->hasMany('App\Models\ProductList');
    }
}
