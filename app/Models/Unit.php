<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = [
        'id',
    	'label',
        'pieces',
        'pieces_label'
    ];

    //START OF RELATION

    public function items()
    {
        return $this->hasMany('App\Models\Item');
    }

    public function stocks()
    {
    	return $this->hasMany('App\Models\Stock');
    }

    //END OF RELATION
}
