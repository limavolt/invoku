<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CapitalPrice extends Model
{
    protected $fillable = [
    	'item_id',
    	'value'
    ];

    public function item()
    {
    	return $this->belongsTo('App\Models\Item');
    }
}
