<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
    	'customer_id',
    	'issued_date',
    	'number',
    	'shipping_date',
    	'due_date',
        'total',
        'profit',
    ];

    // RELATION
    public function customer()
    {
    	return $this->belongsTo('App\Models\Company', 'customer_id');
    }

    public function deliveryOrders()
    {
    	return $this->hasMany('App\Models\DeliveryOrder');
    }

    public function sellingPrices()
    {
    	return $this->hasMany('App\Models\SellingPrice');
    }

    public function productLists()
    {
        return $this->morphMany('App\Models\ProductList', 'quantifiable');
    }
    // END OF RELATION
}
