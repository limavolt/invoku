<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemGroup extends Model
{
    use SoftDeletes;

    protected $fillable = ['id', 'parent_id', 'name', 'description'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    // RELATION
    public function items()
    {
        return $this->hasMany('App\Models\Item');
    }

    public function parent()
    {
    	return $this->belongsTo('App\Models\ItemGroup');
    }
    // END RELATION


    // ACCESSOR
    public function getDescriptionAttribute($value)
    {
    	if ($value) {
    		return $value;
    	}else{
    		return '-';
    	}
    }
}
