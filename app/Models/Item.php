<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;

    /**
     * List of mass-assigned model attributes
     *
     * @var string[]
     */
	protected $fillable = [
        'id',
        'item_group_id',
        'item_brand_id',
        'supplier_id',
        'unit_id',
        'name',
        'description'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	// RELATION
	public function itemGroup()
	{
		return $this->belongsTo('App\Models\ItemGroup');
	}

	public function itemBrand()
	{
		return $this->belongsTo('App\Models\ItemBrand');
	}

    public function supplier()
    {
    	return $this->belongsTo('App\Models\Company');
    }

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit');
    }

    public function capitalPrices()
    {
        return $this->hasMany('App\Models\CapitalPrice');
    }

    public function sellingPrices()
    {
        return $this->hasMany('App\Models\SellingPrice');
    }

    public function stocks()
    {
        return $this->hasMany('App\Models\Stock');
    }

    public function price()
    {
        return $this->hasMany('App\Models\DefaultPrice');
    }
    // END OF RELATION

    /**
     * Format description attribute
     *
     * Retrieve description attribute from database
     * then assign it the "-" character if there is empty.
     *
     * @return mixed
     */
    public function getDescriptionAttribute($value)
    {
    	if ($value) {
    		return $value;
        }
        else {
    		return '-';
    	}
    }
}
