<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryOrder extends Model
{
    protected $fillable = [
    	'invoice_id',
    	'issued_date',
    	'label'
    ];

    // RELATION
    public function invoice()
    {
    	return $this->belongsTo('App\Models\Invoice');
    }
    // END OF RELATION
}
