<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductList extends Model
{
    protected $fillable = [
        'id',
    	'stock_id',
        'count',
        'price',
    	'quantifiable_type',
    	'quantifiable_id'
    ];

    // RELATION
    public function stock()
    {
    	return $this->belongsTo('App\Models\Stock');
    }

    public function quantifiable()
    {
    	return $this->morphTo();
    }
    // END RELATION

}
