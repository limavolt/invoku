<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
    	'name',
        'type',
        'address',
        'pic',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    // RELATION

    public function invoices()
    {
    	return $this->hasMany('App\Models\Invoice');
    }

    public function items()
    {
        return $this->hasMany('App\Models\Item', 'supplier_id');
    }

    // END OF RELATION
}
