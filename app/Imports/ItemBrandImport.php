<?php

namespace App\Imports;

use App\Models\ItemBrand;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ItemBrandImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ItemBrand([
            'id'          => $row['id'],
            'name'        => $row['nama_merek'],
            'description' => $row['catatan'],
        ]);
    }
}
