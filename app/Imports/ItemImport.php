<?php

namespace App\Imports;

use App\Models\CapitalPrice;
use App\Models\Item;
use App\Models\Stock;
use App\Models\ProductList;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ItemImport implements ToModel, WithHeadingRow, WithCalculatedFormulas
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (request()->is('database/import/item-data')) {
            return new Item([
                'id'            => $row['id_barang'],
                'item_group_id' => $row['id_kategori'],
                'item_brand_id' => $row['id_merek'],
                'supplier_id'   => $row['id_supplier'],
                'unit_id'       => $row['id_satuan'],
                'name'          => $row['nama'],
                'description'   => $row['catatan'],
            ]);
        }

        if (request()->is('database/import/stock-data')) {
            return new Stock([
                'id'      => $row['id_barang'],
                'item_id' => $row['id_barang'],
                'count'   => $row['jumlah'],
                'price'   => $row['harga_beli'],
                'sale'    => $row['penjualan'],
            ]);
        }

        if (request()->is('database/import/product-list-data')) {
            return new ProductList([
                'id'                => $row['id_barang'],
                'stock_id'          => $row['id_barang'],
                'count'             => $row['jumlah'],
                'price'             => $row['harga_beli'],
                'quantifiable_type' => $row['jenis_kedatangan'],
                'quantifiable_id'   => $row['id_kedatangan'],
            ]);
        }

        if (request()->is('database/import/capital-price-data')) {
            return new CapitalPrice([
                'item_id' => $row['id_barang'],
                'value'   => $row['harga_beli'],
            ]);
        }

        // return new Stock([
        //     'id'      => $row['id_barang'],
        //     'item_id' => $row['id_barang'],
        //     'count'   => $row['jumlah'],
        //     'price'   => $row['harga_beli'],
        //     'sale'    => $row['penjualan'],
        // ]);

        // return new ProductList([
        //     'id'                => $row['id_barang'],
        //     'stock_id'          => $row['id_barang'],
        //     'count'             => $row['jumlah'],
        //     'price'             => $row['harga_beli'],
        //     'quantifiable_type' => $row['jenis_kedatangan'],
        //     'quantifiable_id'   => $row['id_kedatangan'],
        // ]);
    }
}
