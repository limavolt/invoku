<?php

namespace App\Imports;

use App\Models\CapitalPrice;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CapitalPriceImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new CapitalPrice([
            'item_id' => $row['id_barang'],
            'value'   => $row['harga_beli'],
        ]);
    }
}
