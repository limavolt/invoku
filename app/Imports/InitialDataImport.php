<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class InitialDataImport implements WithMultipleSheets
{
    public function sheets(): array
    {
        // return [
        //     'kontak'          => new \App\Imports\CompanyImport(),
        //     'kategori-barang' => new \App\Imports\ItemGroupImport(),
        //     'merek-barang'    => new \App\Imports\ItemBrandImport(),
        //     'satuan-barang'   => new \App\Imports\UnitImport(),
        //     'kedatangan-barang' => new \App\Imports\PurchaseImport(),
        //     'daftar-barang' => new \App\Imports\ItemImport(),
        // ];

        if (request()->is('database/import/prerequisite-data')) {
            return [
                'kontak'          => new \App\Imports\CompanyImport(),
                'kategori-barang' => new \App\Imports\ItemGroupImport(),
                'merek-barang'    => new \App\Imports\ItemBrandImport(),
                'satuan-barang'   => new \App\Imports\UnitImport(),
            ];
        }

        if (request()->is('database/import/purchase-data')) {
            return [
                'kedatangan-barang' => new \App\Imports\PurchaseImport(),
            ];
        }

        if (request()->is('database/import/item-data') || request()->is('database/import/stock-data') || request()->is('database/import/product-list-data') || request()->is('database/import/capital-price-data')) {
            return [
                'daftar-barang' => new \App\Imports\ItemImport(),
            ];
        }
    }
}
