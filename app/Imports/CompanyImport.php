<?php

namespace App\Imports;

use App\Models\Company;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CompanyImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Company([
            'id'      => $row['id'],
            'name'    => $row['nama'],
            'type'    => $row['jenis'],
            'address' => $row['alamat'],
            'pic'     => $row['pic'],
        ]);
    }
}
