<?php

namespace App\Imports;

use App\Models\DefaultPrice;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PriceImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new DefaultPrice([
            'item_id'       => $row['item_id'],
            'capital_price' => $row['capital_price'],
            'selling_price' => $row['selling_price'],
        ]);
    }
}
