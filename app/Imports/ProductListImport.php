<?php

namespace App\Imports;

use App\Models\ProductList;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductListImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ProductList([
            'stock_id'          => $row['item_id'],
            'count'             => $row['count'],
            'quantifiable_type' => $row['quantifiable_type'],
            'quantifiable_id'   => $row['quantifiable_id'],
            'price'             => $row['value'],
        ]);
    }
}
