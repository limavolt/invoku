<?php

namespace App\Imports;

use App\Models\ItemGroup;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ItemGroupImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ItemGroup([
            'id'          => $row['id'],
            'name'        => $row['nama_kategori'],
            'description' => $row['catatan'],
        ]);
    }
}
