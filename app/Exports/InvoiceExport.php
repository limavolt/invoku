<?php

namespace App\Exports;

use App\Models\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class InvoiceExport implements FromView, WithEvents
{
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getPageMargins()->setTop(0.2);
                $event->sheet->getPageMargins()->setBottom(0.2);
                $event->sheet->getPageMargins()->setRight(0.2);
                $event->sheet->getPageMargins()->setLeft(0.2);
                $event->sheet->getPageMargins()->setHeader(0);
                $event->sheet->getPageMargins()->setFooter(0);
                $event->sheet->getPageSetup()->setHorizontalCentered(true);
                $event->sheet->getDefaultStyle()->getFont()->setName('Calibri');
                $event->sheet->getDefaultStyle()->getFont()->setSize(16);

                // Jadikan lebar default kolom
                $event->sheet->getDelegate()->getDefaultColumnDimension()->setWidth(25);
                // Jadikan kolom A memiliki lebar kolom 38 pt
                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(38);

                // Jadikan header rata atas.
                $event->sheet->getDelegate()->getStyle('A1:D3')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                // Jadikan header tengah rata tengah.
                $event->sheet->getDelegate()->getStyle('B1:C3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                // Jadikan header kanan rata kanan.
                $event->sheet->getDelegate()->getStyle('D1:D3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                // Jadikan informasi pelanggan memiliki text-wrap.
                $event->sheet->getDelegate()->getStyle('D2')->getAlignment()->setWrapText(true);
                // Jadikan informasi pelanggan memiliki tinggi yang cukup.
                $event->sheet->getDelegate()->getRowDimension('2')->setRowHeight(45);

                $event->sheet->getDelegate()->getStyle('B1')->getFont()->setUnderline(true);
                $event->sheet->getDelegate()->getStyle('A1')->getFont()->setSize(72);

                // Mengatur border konten.
                $event->sheet->getDelegate()->getStyle('A5:D16')->getBorders()->getOutline()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $event->sheet->getDelegate()->getStyle('A5:D5')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $event->sheet->getDelegate()->getStyle('A5:D5')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $event->sheet->getDelegate()->getStyle('A5:D5')->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $event->sheet->getDelegate()->getStyle('A5:D5')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $event->sheet->getDelegate()->getStyle('A5:D5')->getBorders()->getVertical()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $event->sheet->getDelegate()->getStyle('A5:D16')->getBorders()->getVertical()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $event->sheet->getDelegate()->getStyle('A17:D17')->getBorders()->getOutline()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $event->sheet->getDelegate()->getStyle('A17:D17')->getBorders()->getVertical()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $event->sheet->getDelegate()->getStyle('B18:C19')->getBorders()->getOutline()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $event->sheet->getDelegate()->getStyle('B18:C19')->getBorders()->getHorizontal()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                // Jadikan konten tabel kanan rata kanan.
                $event->sheet->getDelegate()->getStyle('B5:D16')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                // Jadikan baris total rata kanan.
                $event->sheet->getDelegate()->getStyle('A17:D17')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

                // Jadikan catatan rata atas dan berhuruf kecil.
                $event->sheet->getDelegate()->getStyle('A18')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $event->sheet->getDelegate()->getStyle('A18')->getFont()->setSize(6.25);

                // Jadikan tanda tangan rata tengah.
                $event->sheet->getDelegate()->getStyle('A21:D22')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            }
        ];
    }

    public function view(): View
    {
        return view('sales.excel', ['invoice' => Invoice::find($this->id)]);
    }
}
