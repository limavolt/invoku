<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class CompanyTemplate implements FromView, ShouldAutoSize, WithEvents
{
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $validation = $event->sheet->getDelegate()->getDataValidation('A2:A');

                $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                $validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
                $validation->setAllowBlank(false);
                $validation->setFormula1('"supplier,customer"');
                $validation->setShowDropDown(true);
                $validation->setShowInputMessage(true);

                $event->sheet->getDelegate()->setDataValidation('A2:A100', $validation);
                $event->sheet->setTitle('kontak');
            },
        ];
    }

    public function view(): View
    {
        return view('companies.template');
    }
}
