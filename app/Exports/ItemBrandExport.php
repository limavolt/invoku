<?php

namespace App\Exports;

use App\Models\ItemBrand;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ItemBrandExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        return view('item-brands.list-exported', [
            'item_brands' => ItemBrand::all()
        ]);
    }
}
