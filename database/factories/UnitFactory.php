<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Unit::class, function (Faker $faker) {
    return [
        'label' => $faker->unique()->word(),
        'pieces' => $faker->randomNumber(2),
        'pieces_label' => $faker->word(),
    ];
});
