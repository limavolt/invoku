<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Company::class, function (Faker $faker) {
    return [
        'name'    => $faker->company(),
        'type'    => $faker->randomElement(['supplier', 'customer', 'both']),
        'address' => $faker->city(),
        'pic'     => $faker->name(),
    ];
});
