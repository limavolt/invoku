<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\ItemGroup::class, function (Faker $faker) {
    return [
        'name'        => ucfirst($faker->sentence(3, true)),
        'description' => $faker->optional()->paragraph(),
        'parent_id'   => $faker->optional()->numberBetween(1, 50),
    ];
});
