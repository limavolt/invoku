<?php

use App\User;
use App\Models\Profile;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $new_user = new User([
            'name'       => 'System Administrator',
        	'email'      => 'haeriadi@limavolt.web.id',
        	'username'   => 'admin',
        	'password'   => bcrypt('password'),
        ]);
        $new_user->save();
    }
}
