<?php

use Illuminate\Database\Seeder;

class ItemBrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ItemBrand::class, 350)->create();
    }
}
