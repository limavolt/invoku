@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-6 col-lg-6">
            @component('components.dashboard-panel', [
                'panel_context' => 'panel-horizontal',
                'panel_icon'    => 'upload',
            ])
                <p>
                    Jika ada pengiriman barang, kamu harus mencatatkannya pada sistem
                    agar dapat mengurangi jumlah stok produk pada sistem.
                </p>

                @slot('panel_footer_body')
                    <a href="{{ route('sales.create') }}" class="btn btn-default">
                        <span class="fa fa-chevron-right"></span>
                        {{ ucwords(__('buat faktur pejualan')) }}
                    </a>
                @endslot
            @endcomponent
        </div>

        <div class="col-md-6 col-lg-6">
            @component('components.dashboard-panel', [
                'panel_context' => 'panel-horizontal',
                'panel_icon'    => 'download',
            ])
                <p>
                    Jika ada kedatangan barang, kamu harus mencatatkannya pada sistem
                    agar dapat menambahkan jumlah stok produk pada sistem.
                </p>

                @slot('panel_footer_body')
                    <a href="{{ route('incoming-goods.create') }}" class="btn btn-default">
                        <span class="fa fa-chevron-right"></span>
                        {{ ucwords(__('catat kedatangan produk')) }}
                    </a>
                @endslot
            @endcomponent
        </div>
    </div>

    <hr>

    <div class="row">
        <!-- start: widget faktur -->
        <a href="{{ route('sales.index') }}">
            <div class="col-md-2">
                @component('components.dashboard-panel', [
                    'panel_icon'      => 'money',
                    'icon_background' => 'bg-info',
                ])
                    <p class="text-center text-dark">{{ ucfirst(__('daftar faktur penjualan')) }}</p>
                @endcomponent
            </div>
        </a>
        <!-- end: widget faktur -->

        <!-- start: widget stock -->
        <div class="col-md-2">
            <a href="{{ route('stocks.index') }}">
                @component('components.dashboard-panel', [
                    'panel_icon'      => 'cubes',
                    'icon_background' => 'bg-info',
                ])
                    <p class="text-center text-dark">{{ ucfirst(__('daftar stok produk')) }}</p>
                @endcomponent
            </a>
        </div>
        <!-- end: widget stock -->

        <!-- start: widget produk -->
        <a href="{{ route('items.index') }}">
            <div class="col-md-2">
                @component('components.dashboard-panel', [
                    'panel_icon'      => 'list',
                    'icon_background' => 'bg-info',
                ])
                    <p class="text-center text-dark">{{ ucfirst(__('daftar produk')) }}</p>
                @endcomponent
            </div>
        </a>
        <!-- end: widget produk -->

        <!-- start: widget brand -->
        <a href="{{ route('item-brands.index') }}">
            <div class="col-md-2">
                @component('components.dashboard-panel', [
                    'panel_icon'      => 'asterisk',
                    'icon_background' => 'bg-info',
                ])
                    <p class="text-center text-dark">{{ ucfirst(__('daftar merek')) }}</p>
                @endcomponent
            </div>
        </a>
        <!-- end: widget brand -->

        <!-- start: widget category -->
        <a href="{{ route('item-groups.index') }}">
            <div class="col-md-2">
                @component('components.dashboard-panel', [
                    'panel_icon'      => 'folder',
                    'icon_background' => 'bg-info',
                ])
                    <p class="text-center text-dark">{{ ucfirst(__('daftar kategori produk')) }}</p>
                @endcomponent
            </div>
        </a>
        <!-- end: widget category -->

        <!-- start: widget supplier -->
        <a href="{{ route('suppliers.index') }}">
            <div class="col-md-2">
                @component('components.dashboard-panel', [
                    'panel_icon'      => 'truck',
                    'icon_background' => 'bg-info',
                ])
                    <p class="text-center text-dark">{{ ucfirst(__('daftar supplier')) }}</p>
                @endcomponent
            </div>
        </a>
        <!-- end: widget supplier -->

        <!-- start: widget unit -->
        <a href="{{ route('inventory-units.index') }}">
            <div class="col-md-2">
                @component('components.dashboard-panel', [
                    'panel_icon'      => 'sliders',
                    'icon_background' => 'bg-info',
                ])
                    <p class="text-center text-dark">{{ ucfirst(__('daftar satuan')) }}</p>
                @endcomponent
            </div>
        </a>
        <!-- end: widget unit -->
    </div>
@endsection
