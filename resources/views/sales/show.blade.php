{{-- sales/show.blade.php --}}

@extends('layouts.dashboard', ['page_title' => 'detail invoice'])

@section('content')
    <div class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="{{ route('sales.index') }}" class="fa fa-arrow-left" title="{{ ucwords(__('kembali')) }}"></a>

                <a href="{{ route('sales.print', $invoice->id) }}" class="fa fa-print" title="{{ ucfirst(__('cetak')) }}" target="_blank"></a>
            </div>

            <h2 class="panel-title">{{ strtoupper(__('detail faktur')) . ' ' . strtoupper(__('nomor')) . ' ' . strtoupper(e(settings()->get('invoice_code'))) . \Carbon\Carbon::parse($invoice->issued_date)->format('ym') }}{{ sprintf('%03d', $invoice->number) }}</h2>
        </header>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    @if (settings()->get('company_name'))
                        <p class="h1">{{ settings()->get('company_name') }}</p>
                    @else
                        <p class="h1">{{ config('app.name') }}</p>
                    @endif

                    <p>&nbsp;</p>
                    <p>{{ ucfirst(__('sales')) }}: MA</p>
                </div>

                <div class="col-md-4">
                    <div class="row text-center">
                        <div class="col-md-12">
                            <h3 class="h4 mb-none"><strong>{{ strtoupper(__('nota penjualan')) }}</strong></h3>
                            <p class="h4 mt-none text-muted">{{ strtoupper(e(settings()->get('invoice_code'))) . \Carbon\Carbon::parse($invoice->issued_date)->format('ym') . sprintf('%03d', $invoice->number) }}</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 pt-lg text-right">
                    <p class="mb-none">{{ strtoupper(__('kepada yth.')) }}</p>
                    <p>{{ $invoice->customer->name }}<br>{{ $invoice->customer->address }}</p>
                    <p>{{ ucwords(__('tanggal')) }}: {{ \Carbon\Carbon::parse($invoice->issued_date)->format('d/m/Y') }}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered mb-none">
                        <thead>
                            <tr>
                                <th class="col-md-3">{{ strtoupper(__('nama barang')) }}</th>
                                <th class="col-md-3">{{ strtoupper(__('qty')) }}</th>
                                <th class="col-md-3 text-right">{{ strtoupper(__('harga')) }}</th>
                                <th class="col-md-3 text-right">{{ strtoupper(__('jumlah')) }}</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($invoice->productLists as $item)
                                <tr>
                                    <td class="col-md-3" style="border-top: none; border-bottom: none;">{{ $item->stock->item->name }} (@ {{ $item->stock->item->unit->pieces }} {{ $item->stock->item->unit->pieces_label }})</td>
                                    <td class="col-md-3" style="border-top: none; border-bottom: none;">{{ $item->stock->sale == 'unit' ? abs($item->count) . ' ' . $item->stock->item->unit->label : abs($item->count * $item->stock->item->unit->pieces) . ' ' . $item->stock->item->unit->pieces_label }}</td>
                                    <td class="col-md-3 text-right" style="border-top: none; border-bottom: none;">Rp{{ number_format($item->price, 0, ',', '.') }}</td>
                                    <td class="col-md-3 text-right" style="border-top: none; border-bottom: none;">Rp{{ number_format((($item->stock->sale == 'unit' ? $item->price : ($item->price * $item->stock->item->unit->pieces)) * abs($item->count)), 0, ',', '.') }}</td>
                                </tr>
                            @endforeach
                        </tbody>

                        <tfoot>
                            <tr>
                                <th class="text-right" colspan="3">{{ strtoupper(__('total')) }}</th>
                                <th class="text-right">Rp{{ number_format($invoice->total, 0, ',', '.') }}</th>
                            </tr>

                            {{-- <tr>
                                <th class="text-right">{{ strtoupper(__('tanggal kirim')) }}</th>
                                <td>{{ \Carbon\Carbon::parse($invoice->shipping_date)->format('l, d F Y') }}</td>
                            </tr>

                            <tr>
                                <th class="text-right">{{ strtoupper(__('jatuh tempo')) }}</th>
                                <td>{{ \Carbon\Carbon::parse($invoice->due_date)->format('l, d F Y') }}</td>
                            </tr> --}}
                        </tfoot>
                    </table>

                    <div class="col-md-6 col-md-offset-3 mt-none">
                        <table class="table table-bordered">
                            <tr>
                                <th class="col-md-3">{{ strtoupper(__('tempo bayar')) }}</th>
                                <td class="col-md-3">{{ \Carbon\Carbon::parse($invoice->due_date)->diffInDays(\Carbon\Carbon::parse($invoice->issued_date)) !== 0 ? \Carbon\Carbon::parse($invoice->due_date)->diffInDays(\Carbon\Carbon::parse($invoice->issued_date)) . ' ' . strtoupper(__('hari')) : strtoupper(__('tunai')) }}</td>
                            </tr>

                            <tr class="warning">
                                <th class="col-md-3">{{ strtoupper(__('jatuh tempo')) }}</th>
                                <td class="col-md-3">{{ \Carbon\Carbon::parse($invoice->due_date)->format('d-m-Y') }}</td>
                            </tr>
                        </table>
                    </div>
                </div>

                {{-- <div class="col-md-6 col-md-offset-3">
                    <table class="table table-bordered">
                        <tr>
                            <th>{{ strtoupper(__('tanggal kirim')) }}</th>
                            <td>{{ \Carbon\Carbon::parse($invoice->shipping_date)->format('l, d F Y') }}</td>
                        </tr>

                        <tr class="warning">
                            <th>{{ strtoupper(__('jatuh tempo')) }}</th>
                            <td>{{ \Carbon\Carbon::parse($invoice->due_date)->format('l, d F Y') }}</td>
                        </tr>
                    </table>
                </div> --}}
            </div>

            {{-- <div class="row">
                <div class="col-md-6 text-center">
                    <p>{{ ucfirst(__('diterima oleh')) }},</p>

                    <div style="min-height: 30px;"></div>

                    <p>(........................................)</p>
                </div>

                <div class="col-md-6 text-center">
                    <p>{{ ucfirst(__('diperiksa oleh')) }},</p>

                    <div style="min-height: 30px;"></div>

                    <p>(........................................)</p>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-md-12 text-center">
                    <p>Komplain/retur lebih dari 2 minggu tidak diterima atau dilayani.</p>
                </div>
            </div> --}}
        </div>

        <div class="panel-footer">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('sales.index') }}" class="btn btn-default">
                        <span class="fa fa-arrow-left"></span>
                        {{ ucwords(__('kembali')) }}
                    </a>

                    <a class="btn btn-primary" href="{{ route('sales.print', $invoice->id) }}" target="_blank">
                        <span class="fa fa-print"></span>
                        {{ ucfirst(__('cetak')) }}
                    </a>
                </div>
            </div>
        </div>
    </div>

    <p class="text-center">
        <a href="{{ route('sales.index') }}"><span class="fa fa-arrow-left"></span> {{ ucfirst(__('kembali ke daftar tagihan')) }}</a>
    </p>
@endsection
