{{-- sales/print.blade.php --}}

@extends('layouts.print')

@section('content')
    <div class="row">
        <div class="col-xs-4">
            @if (settings()->get('company_logo'))
                <img src="{{ settings()->get('company_logo') }}" alt="Logo perusahaan">
            @elseif (settings()->get('company_name'))
                <p class="h1">{{ settings()->get('company_name') }}</p>
            @else
                <p class="h1">{{ config('app.name') }}</p>
            @endif

            <p>{{ ucfirst(__('sales')) }}: MA</p>
        </div>

        <div class="col-xs-4 text-center">
            <h3 class="h4 mb-none"><strong>{{ strtoupper(__('nota penjualan')) }}</strong></h3>
            <p class="h4 mt-none">{{ strtoupper(e(settings()->get('invoice_code'))) . \Carbon\Carbon::parse($invoice->issued_date)->format('ym') . sprintf('%03d', $invoice->number) }}</p>
        </div>

        <div class="col-xs-4 text-right">
            <p class="mb-none">{{ strtoupper(__('kepada yth.')) }}</p>
            <p>{{ $invoice->customer->name }}</p>
            <p>{{ ucwords(__('tanggal')) }}: {{ \Carbon\Carbon::parse($invoice->due_date)->format('d/m/Y') }}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" style="margin-bottom: 0;">
                <thead>
                    <tr>
                        <th class="col-md-3">{{ strtoupper(__('nama barang')) }}</th>
                        <th class="col-md-3">{{ strtoupper(__('kuantitas')) }}</th>
                        <th class="col-md-3 text-right">{{ strtoupper(__('harga')) }}</th>
                        <th class="col-md-3 text-right">{{ strtoupper(__('jumlah')) }}</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($invoice->productLists as $item)
                        <tr>
                            <td class="col-md-3" style="border-top: none; border-bottom: none;">{{ $item->item->name }} (@ {{ $item->item->unit->pieces }} {{ $item->item->unit->pieces_label }})</td>
                            <td class="col-md-3" style="border-top: none; border-bottom: none;">{{ abs($item->count) * $item->item->unit->pieces }} {{ $item->item->unit->pieces_label }}</td>
                            <td class="col-md-3 text-right" style="border-top: none; border-bottom: none;">Rp{{ number_format($item->item->sellingPrices->where('invoice_id', $invoice->id)->map(function ($price) { return $price->value; })->first(), 2, ',', '.') }}</td>
                            <td class="col-md-3 text-right" style="border-top: none; border-bottom: none;">Rp{{ number_format($item->item->sellingPrices->where('invoice_id', $invoice->id)->map(function ($price) { return $price->value; })->first() * abs($item->count), 2, ',', '.') }}</td>
                        </tr>
                    @endforeach
                </tbody>

                <tfoot>
                    <tr>
                        <th class="text-right" colspan="3">{{ strtoupper(__('total')) }}</th>
                        <th class="text-right">Rp{{ number_format($invoice->total, 2, ',', '.') }}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>


    <div class="row">
        <div class="col-md-3 text-center" style="padding-top: 2em;">
            <p>{{ ucfirst(__('diterima oleh')) }},</p>

            <div style="min-height: 30px;"></div>

            <p>(........................................)</p>
        </div>

        <div class="col-md-4 col-md-offset-1 mt-none">
            <table class="table table-bordered">
                <tr>
                    <th class="col-md-2">{{ strtoupper(__('tanggal kirim')) }}</th>
                    <td class="col-md-2">{{ \Carbon\Carbon::parse($invoice->shipping_date)->format('l, d F Y') }}</td>
                </tr>

                <tr>
                    <th class="col-md-2">{{ strtoupper(__('jatuh tempo')) }}</th>
                    <td class="col-md-2">{{ \Carbon\Carbon::parse($invoice->due_date)->format('l, d F Y') }}</td>
                </tr>
            </table>
        </div>

        <div class="col-md-3 col-md-offset-1 text-center" style="padding-top: 2em;">
            <p>{{ ucfirst(__('diperiksa oleh')) }},</p>

            <div style="min-height: 30px;"></div>

            <p>(........................................)</p>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-md-12">
            <p><strong>{{ strtoupper(__('komplain/retur lebih dari 2 minggu tidak diterima atau dilayani')) }}</strong></p>
        </div>
    </div>
@endsection
