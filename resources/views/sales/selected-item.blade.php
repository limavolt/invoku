{{-- sales/selected-item.blade.php --}}

<tr class="item-list">
    <td>{{ $item->name }}</td>
    <td>{{ $item_count }} {{ strtolower($item_unit_label . ' @ ' . $item_unit_pieces . ' ' . $item_pieces_label) }}</td>
    <td class="text-right">Rp{{ $price_text }} per {{ strtolower($sale == 'unit' ? $item_unit_label : $item_pieces_label) }}</td>
    <td class="text-right">Rp{{ number_format($sale == 'unit' ? $item_price * $item_count : $item_price * $item_unit_pieces * $item_count, 0, ',', '.') }}</td>
    <td class="text-right"><button type="button" class="btn btn-link" name="delete-item-button"><span class="fa fa-trash-o"></span> {{ ucwords(__('hapus')) }}</button></td>

    <input type="hidden" name="item[{{ $item->id }}][id]" value="{{ $item->id }}">
    <input type="hidden" name="item[{{ $item->id }}][name]" value="{{ $item->name }}">
    <input type="hidden" name="item[{{ $item->id }}][price]" class="item-value" value="{{ $item_price }}">
    <input type="hidden" name="item[{{ $item->id }}][count]" class="item-count" value="{{ $item_count }}">
    <input type="hidden" name="item[{{ $item->id }}][unit]" value="{{ $item_unit }}">
    <input type="hidden" name="item[{{ $item->id }}][stock_id]" value="{{ $stock_id }}">
    <input type="hidden" name="item[{{ $item->id }}][capital]" class="item-capital" value="{{ $capital }}">
    <input type="hidden" name="item[{{ $item->id }}][unit_price]" class="item-unit-value" value="{{ $unit_price }}">
 </tr>
