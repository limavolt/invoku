{{-- sales/index.blade.php --}}

@extends('layouts.dashboard', ['page_title' => 'penjualan'])

@section('content')
    <!-- start: content lead -->
    <div class="row">
        <div class="col-md-6">
            <div class="alert alert-info">
                <p class="lead text-center">Penjualan kamu sebesar <strong>Rp<span id="sale"></span></strong>.</p>
            </div>
        </div>

        <div class="col-md-6">
            <div class="alert alert-success">
                <p class="lead text-center">Profit kamu sebesar <strong>Rp<span id="valuation"></span></strong>.</p>
            </div>
        </div>
    </div>
    <!-- end: content lead -->

    <!-- start: customer credit list panel -->
    @component('components.panel', ['panel_title' => 'daftar piutang pelanggan'])
        <!-- start: toolbar -->
        <div class="row mb-md">
            <!-- start: left toolbar -->
            <div class="col-md-6">
                <label for="issued-date-filter">{{ ucfirst(__('tanggal faktur')) }}</label>

                <select id="issued-date-filter" class="col-md-3" name="issued-date-filter">
                    <option></option>

                    @foreach ($invoices->pluck('issued_date')->map(function ($date) { return \Carbon\Carbon::parse($date)->format('d M Y'); })->unique()->sort() as $invoice)
                        <option value="{{ $invoice }}">{{ $invoice }}</option>
                    @endforeach
                </select>

                {{-- <label for="issued-month-filter">{{ ucfirst(__('bulan faktur')) }}</label>

                <select id="issued-month-filter" class="col-md-3" name="issued-month-filter">
                    <option></option>

                    @foreach ($invoices->pluck('issued_date')->map(function ($date) { return \Carbon\Carbon::parse($date)->format('M Y'); })->unique()->sort() as $invoice)
                        <option value="{{ $invoice }}">{{ $invoice }}</option>
                    @endforeach
                </select> --}}

                <button id="show-all-button" class="btn btn-primary" type="button" role="button">
                    {{ ucwords(__('tampilkan semua')) }}
                </button>

                <button id="show-today-button" class="btn btn-primary" type="button" role="button">
                    {{ ucwords(__('tampilkan hari ini')) }}
                </button>
            </div>
            <!-- end: left toolbar -->

            <!-- start: right toolbar -->
            <div class="col-md-6 text-right">
                <label for="customer-filter">{{ ucfirst(__('pelanggan')) }}</label>

                <select id="customer-filter" class="col-md-3" name="customer-filter">
                    <option></option>

                    @foreach ($customers as $customer)
                        <option value="{{ $customer->name }}">{{ $customer->name }}</option>
                    @endforeach
                </select>

                <label for="invoice-status-filter">{{ ucfirst(__('status')) }}</label>

                <select id="invoice-status-filter" class="col-md-3" name="invoice-status-filter">
                    <option></option>
                    <option value="open">{{ ucfirst(__("belum lunas")) }}</option>
                    <option value="lunas">{{ ucfirst(__("lunas")) }}</option>
                </select>
            </div>
            <!-- end: right toolbar -->
        </div>
        <!-- end: toolbar -->

        <!-- start: invoice list -->
        @component('components.datatable-ajax', [
            'table_id'      => 'sales',
            'condition'     => true,
            'table_headers' => ['nomor faktur', 'nama pelanggan', 'tanggal faktur', 'jatuh tempo', 'total tagihan', 'telat/hari', 'profit', 'persentase'],
            'data'          => [
                ['name' => 'number', 'data' => 'number'],
                ['name' => 'customer.name', 'data' => 'customer.name'],
                ['name' => 'issued_date', 'data' => 'issued_date'],
                ['name' => 'due_date', 'data' => 'due_date'],
                ['name' => 'total', 'data' => 'total', 'render' => "$.fn.dataTable.render.number( '.', ',', 2, 'Rp' )"],
                ['name' => 'status', 'data' => 'status'],
                ['name' => 'profit', 'data' => 'profit', 'render' => "$.fn.dataTable.render.number( '.', ',', 2, 'Rp' )"],
                ['name' => 'percentage', 'data' => 'percentage',],
            ],
            'renders' => [
                ['text_align' => 'text-right col-sm-2', 'column_target' => '9']
			],
            'callback' => "$('#valuation').empty(); $('#valuation').append($.number($('#sales-table').DataTable().column(7, {'filter': 'applied'}).data().sum(), 0, ',', '.'));"
                        . "$('#sale').empty(); $('#sale').append($.number($('#sales-table').DataTable().column(5, {'filter': 'applied'}).data().sum(), 0, ',', '.'));",
        ])
            @slot('data_send_ajax')
            @endslot
        @endcomponent
        <!-- end: invoice list -->
    @endcomponent
    <!-- end: customer credit list panel -->

    @include('sales.settlement')
@endsection

@push('headscripts')
    <script src="{{ asset('js/moment.js') }}" type="text/javascript"></script>
@endpush

@push('vendorstyles')
	<link href="{{ asset('assets/vendor/pnotify/pnotify.custom.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/vendor/select2-4.0.8/css/select2.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/vendor/select2-bootstrap/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
@endpush

@push('vendorscripts')
	<script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendor/select2-4.0.8/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/sum.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.number.min.js') }}" type="text/javascript"></script>
@endpush

@push('appscripts')
    <script type="text/javascript">
        $(document).ready(function () {
            // Initialize select2 for all of select field.
            $(document).find('select').each(function () {
                $(this).select2({
                    placeholder: 'Pilih dari daftar',
                    allowClear:  true
                });
            });

            table.column(3).search("{{ date('d M Y') }}").draw();

            $('#show-all-button').on('click', function () {
                table.column(3).search('').draw();
            });

            $('#show-today-button').on('click', function () {
                table.column(3).search("{{ date('d M Y') }}").draw();
            });

            // Filter by customer name.
            $('#customer-filter').on('change', function () {
                var selection = $('#customer-filter').find(':selected').val();

                table.column(2).search(selection).draw();
            });

            // Filter by invoice's issued date.
            $('#issued-date-filter').on('change', function () {
                var selection = $('#issued-date-filter').find(':selected').val();

                table.column(3).search(selection).draw();
            });

            // Filter by invoice's due date.
            $('#issued-month-filter').on('change', function () {
                var selection = $('#issued-month-filter').find(':selected').val();

                table.column(3).search(selection).draw();
            });

            // Filter by invoice's payment status.
            $('#invoice-status-filter').on('change', function () {
                var selection = $('#invoice-status-filter').find(':selected').val();

                var log;

                if (selection == 'open') {
                    table.column(6).search("([^(Lunas)])", true).draw();
                }
                else {
                    table.column(6).search(selection).draw();
                }
            });

            // Function to direct users to the invoice details page
            // when the "view" button is pressed.
            $('#sales-table tbody')
                .on('click', 'button[name=btn-show-invoice]', function () {
                    // Defines the URL of the invoice detail page.
                    var url = APP_URL + '/sales/' + $(this).data('id');

                    // Direct the user to the invoice details page
                    // by changing the browser URL property.
                    window.location.href = url;
                });

            $('#sales-table tbody').on('click', 'button[name=btn-excel-invoice]', function () {
                var url = APP_URL + '/sales/' + $(this).data('id') + '/export';

                window.location.href = url;
            })
        });
    </script>
@endpush
