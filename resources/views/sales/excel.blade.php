<table>
    <thead>
        <tr>
            <td rowspan="2"><strong>{{ strtoupper('sjp') }}</strong></td>
            <td colspan="2"><strong>{{ strtoupper(__('nota penjualan')) }}</strong></td>
            <td>{{ strtoupper(__('kepada yth.')) }}</td>
        </tr>

        <tr>
            <td colspan="2">{{ strtoupper(e(settings()->get('invoice_code'))) . \Carbon\Carbon::parse($invoice->issued_date)->format('ym') . sprintf('%03d', $invoice->number) }}</td>
            <td>{{ $invoice->customer->name }}<br>{{ $invoice->customer->address }}</td>
        </tr>

        <tr>
            <td>{{ ucfirst(__('sales')) }}: MA</td>
            <td></td>
            <td></td>
            <td>{{ ucfirst(__('tanggal')) . ': ' . $invoice->issued_date }}</td>
        </tr>

        <tr></tr>
    </thead>

    <tbody>
        <tr>
            <td><strong>{{ strtoupper(__('nama barang')) }}</strong></td>
            <td><strong>{{ strtoupper(__('qty')) }}</strong></td>
            <td><strong>{{ strtoupper(__('harga')) }}</strong></td>
            <td><strong>{{ strtoupper(__('jumlah')) }}</strong></td>
        </tr>

        @foreach ($invoice->productLists as $item)
            @php $count = $loop->count; @endphp

            <tr>
                <td>
                    {{ $item->stock->item->name }} (@ {{ $item->stock->item->unit->pieces }} {{ $item->stock->item->unit->pieces_label }})
                </td>

                <td>
                    {{ $item->stock->sale == 'unit' ? abs($item->count) . ' ' . $item->stock->item->unit->label : abs($item->count * $item->stock->item->unit->pieces) . ' ' . $item->stock->item->unit->pieces_label }}
                </td>

                <td>
                    {{ number_format($item->price, 2, ',', '.') }}
                </td>

                <td>
                    {{ number_format((($item->stock->sale == 'unit' ? $item->price : ($item->price * $item->stock->item->unit->pieces)) * abs($item->count)), 2, ',', '.') }}
                </td>
            </tr>

            @if (0 === $loop->iteration % 11)
                <tr>
                    <td></td>
                    <td><strong>{{ strtoupper(__('tempo bayar')) }}</strong></td>
                    <td>{{ \Carbon\Carbon::parse($invoice->due_date)->diffInDays(\Carbon\Carbon::parse($invoice->issued_date)) !== 0 ? \Carbon\Carbon::parse($invoice->due_date)->diffInDays(\Carbon\Carbon::parse($invoice->issued_date)) . ' ' . strtoupper(__('hari')) : strtoupper(__('tunai')) }}</td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>
                    <td><strong>{{ strtoupper(__('jatuh tempo')) }}</strong></td>
                    <td>{{ \Carbon\Carbon::parse($invoice->due_date)->format('d-m-Y') }}</td>
                    <td></td>
                </tr>
            @endif
        @endforeach

        @if (0 !== $count % 11)
            @for ($i = 0; $i < 11 - $count % 11; $i++)
                <tr>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
            @endfor
        @endif
    </tbody>

    <tfoot>
        <tr>
            <td colspan="3"><strong>{{ strtoupper(__('total')) }}</strong></td>
            <td><strong>{{ number_format($invoice->total, 2, ',', '.') }}</strong></td>
        </tr>

        <tr>
            <td rowspan="2">
                <strong>** KOMPLAIN/RETUR LEBIH DARI 2 MINGGU TIDAK DITERIMA ATAU DILAYANI</strong><br>
                <strong>** BARANG YANG DITERIMA SUDAH DALAM KEADAAN BAIK DAN BENAR</strong><br>
                <strong>** HARGA TIDAK MENGIKAT DAN SEWAKTU-WAKTU DAPAT BERUBAH</strong>
            </td>

            <td><strong>{{ strtoupper(__('tempo bayar')) }}</strong></td>
            <td>{{ \Carbon\Carbon::parse($invoice->due_date)->diffInDays(\Carbon\Carbon::parse($invoice->issued_date)) !== 0 ? \Carbon\Carbon::parse($invoice->due_date)->diffInDays(\Carbon\Carbon::parse($invoice->issued_date)) . ' ' . strtoupper(__('hari')) : strtoupper(__('tunai')) }}</td>
        </tr>

        <tr>
            <td><strong>{{ strtoupper(__('jatuh tempo')) }}</strong></td>
            <td>{{ \Carbon\Carbon::parse($invoice->due_date)->format('d-m-Y') }}</td>
        </tr>

        <tr></tr>

        <tr>
            <td>{{ ucfirst(__('diperiksa oleh')) }},</td>
            <td></td>
            <td></td>
            <td>{{ ucfirst(__('penerima/cap toko')) }},</td>
        </tr>

        <tr>
            <td rowspan="3">(.........................)</td>
            <td></td>
            <td></td>
            <td rowspan="3">(.........................)</td>
        </tr>
    </tfoot>
</table>
