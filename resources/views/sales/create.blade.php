{{-- sales/create.blade.php --}}

@extends('layouts.dashboard', ['page_title' => 'pembuatan faktur penjualan'])

@section('content')
    <!-- start: invoice creation form -->
    <form id="sales-form" action="{{ route('sales.store') }}" method="post">
        @csrf

        <!-- start: customer input field -->
        <div id="div-customer_id" class="form-group mt-lg">
            <label class="col-sm-3 control-label text-right" for="customer_id">
                {{ ucfirst(__('nama customer')) }}
            </label>

            <div class="col-sm-9">
                <div class="row">
                    <div class="col-md-8">
                        <select id="customer_id" class="form-control" name="customer_id">
                            <option></option>

                            {{-- @foreach ($customers as $customer)
                                <option value="{{ $customer->id }}">{{ $customer->name }} - {{ $customer->address }}, {{ $customer->pic }}</option>
                            @endforeach --}}
                        </select>
                    </div>

                    <div class="col-md-4">
                        <button class="btn btn-block btn-default" type="button" data-toggle="modal" data-target="#add-customer-modal" data-backdrop="static" data-keyboard="false">
                            <span class="fa fa-plus"></span>
                            {{ ucwords(__('tambah customer')) }}
                        </button>
                    </div>
                </div>

                <span id="error-issued_date" class="help-block text-error"></span>
            </div>
        </div>
        <!-- end: customer input field -->

        <!-- start: invoice data input field -->
        <div class="form-group mt-lg" id="div-issued_date">
            <label for="issued_date" class="col-sm-3 control-label text-right">
                {{ ucfirst(__('tanggal faktur')) }}
            </label>

            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>

                            <input type="text" class="form-control" name="issued_date" id="issued_date">

                            <span class="help-block text-error" id="error-issued_date"></span>
                        </div>
                    </div>

                    <div class="col-sm-6 col-sm-offset-2">
                        <div class="row">
                            <label for="number" class="col-sm-4 control-label text-right">
                                {{ ucfirst(__('nomor faktur')) }}
                            </label>

                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        {{ settings()->get('invoice_code') . date('ym')  }}
                                    </span>

                                    <input type="text" name="number" class="form-control" id="number" value="{{ $invoices->map(function ($invoice) { return $invoice->number; })->last() + 1 }}">
                                </div>


                                <span class="help-block text-error" id="error-number"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group mt-lg" id="div-shipping_date">
            <label for="shipping_date" class="col-sm-3 control-label text-right">
                {{ ucfirst(__('tempo pembayaran')) }}
            </label>

            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-4">
                        <select name="payment_due" id="payment_due" class="form-control">
                            <option value="3">{{ ucfirst(__('2 minggu')) }}</option>
                            <option value="1">{{ ucfirst(__('cash')) }}</option>
                            <option value="2">{{ ucfirst(__('1 minggu')) }}</option>
                            <option value="4">{{ ucfirst(__('3 minggu')) }}</option>
                            <option value="5">{{ ucfirst(__('1 bulan')) }}</option>
                        </select>

                        <span class="help-block text-error" id="error-issued_date"></span>
                    </div>

                    <div class="col-sm-6 col-sm-offset-2">
                        <div class="row">
                            <label for="due_date" class="col-sm-4 control-label text-right">Tanggal jatuh tempo</label>

                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>

                                    <input type="text" class="form-control" name="due_date" id="due_date">
                                </div>

                                <span class="help-block text-error" id="error-due_date"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: invoice data input field -->

        <!-- start: product list input field -->
        <div class="form-group mt-lg" id="div-items">
            <label for="items" class="col-sm-3 control-label text-right">
                {{ ucfirst(__('daftar produk')) }}
            </label>

            <div class="col-sm-9" id="item-list">
                <button type="button" class="btn btn-primary" id="add-item">
                    <span class="fa fa-check-square"></span>
                    {{ ucwords(e(__('pilih produk'))) }}
                </button>

                <div class="panel mt-md">
                    <div class="panel-body table-responsive">
                        <table class="table mb-md" id="item-list-table">
                            <thead>
                                <tr>
                                    <th>{{ strtoupper(e(__('nama produk'))) }}</th>
                                    <th>{{ strtoupper(e(__('jumlah'))) }}</th>
                                    <th class="text-right">{{ strtoupper(e(__('harga jual satuan'))) }}</th>
                                    <th class="text-right">{{ strtoupper(e(__('subtotal'))) }}</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody id="selected-item-list"></tbody>
                        </table>
                    </div>

                    <div class="panel-footer text-right">
                        <p>Total tagihan adalah <span id="total-displayed">0</span>.</p>
                        <p>Total profit adalah <span id="profit-displayed">0</span>.</p>
                        <input type="hidden" name="total" id="total">
                        <input type="hidden" name="profit" id="profit">
                    </div>
                </div>
            </div>
        </div>
        <!-- end: product list input field -->

        <hr>

        <div class="form-group mt-lg">
            <div class="col-md-12">
                <a href="{{ route('sales.index') }}" class="btn btn-default">
                    <span class="fa fa-arrow-left"></span>
                    {{ ucwords(__('batal')) }}
                </a>

                <button type="submit" class="btn btn-primary pull-right" id="sales-submit-button">
                    <span class="fa fa-save"></span> {{ ucwords(__('simpan catatan')) }}
                </button>
            </div>
        </div>
    </form>
    <!-- start: invoice creation form -->

    @include('sales.add-item')
    @include('companies.customer.create')
@endsection

@push('vendorstyles')
    <link href="{{ asset('assets/vendor/pnotify/pnotify.custom.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}">
    <link href="{{ asset('assets/vendor/select2-4.0.8/css/select2.min.css') }}" rel="stylesheet" style="text/css">
	<link href="{{ asset('assets/vendor/select2-bootstrap/select2-bootstrap.min.css') }}" rel="stylesheet" style="text/css">
@endpush

@push('vendorscripts')
<script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-datepicker/js/locales/bootstrap-datepicker.id.js') }}"></script>
    <script src="{{ asset('assets/vendor/select2-4.0.8/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendor/fuelux/js/spinner.js') }}"></script>
    <script src="{{ asset('js/moment.js') }}"></script>
@endpush

@push('appscripts')
    <script>
        // Specify a function to execute when the document is fully loaded.
        $(document).ready(function () {
            selectCompany('customer', $('#sales-form #customer_id'), $('#sales-form'));

            // Set the default theme for the select2 component
            // using the bootstrap theme.
            $.fn.select2.defaults.set( "theme", "bootstrap" );

            // Initiates select fields inside sales-form form to use select2.
            $('#sales-form')
                .find('select')
                .each(function () {
                    $(this).select2({
                        placeholder: 'Pilih dari daftar',
                        allowClear:  true
                    });
            });

            // Prepare values for use in issued-date and due-date fields.
            var date     = new Date();
            var due_date = new Date();
            var today    = new Date(date.getFullYear(), date.getMonth(), date.getDate());

            due_date.setDate(today.getDate() + 14);

            // Initiate the use of datepickers in the issued-date and due-date fields.
            $('#issued_date, #due_date').datepicker({
                format:         'dd/mm/yyyy',
                language:       'id-ID',
                todayBtn:       'linked',
                todayHighlight: true
            });

            // Set the issued-date and due-date field initial value.
            $('#issued_date').datepicker('setDate', today);
            $('#due_date').datepicker('setDate', due_date);

            // Emptying the value in the payment-due and due-date field
            // when there is a change in the issued-date field.
            $('#issued_date').on('change', function () {
                $('#payment_due').val('').change();
                $('#due_date').val('');
            })

            // Change the value of the due-date field when there is a change
            // in the value of the payment-due field.
            $('#payment_due').on('change', function () {
                var selected_payment = $('#payment_due').val();

                switch (selected_payment) {
                    case '1':
                        term = 0;
                        break;

                    case '2':
                        term = 7;
                        break;

                    case '3':
                        term = 14;
                        break;

                    case '4':
                        term = 21;
                        break;

                    case '5':
                        term = 30;
                        break;

                    default:
                        term = 14;
                }

                var issued_date  = $('#issued_date').val().replace(/[\/]/g, '-').split("-").reverse().join("-");
                var invoice_date = new Date(issued_date);
                var due          = new Date();

                due.setDate(invoice_date.getDate() + term);

                $('#due_date').val(due.getDate() + '/' + (due.getMonth() + 1) + '/' + due.getFullYear());
            });

            // Reload the page when the additional customer form modal is closed.
            $('#add-customer-modal').on('hide.bs.modal', function() {
                selectCompany('customer', $('#customer_id'), $('#sales-form'));
            });

            /*
             * Menjalankan aksi yang diminta ketika tombol add-item
             * ditekan pengguna.
             */
            $('#add-item').click(function () {
                customer = $('#customer_id').val();

                if (customer) {
                    $('#choose-item-modal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });

                    clearForm($('#choose-item-form'));

                    var url = "{{ route('api.stocks.select2') }}";

                    $.ajax({
                        method: 'get',
                        type:   'json',
                        url:    url,

                        success: function (data) {
                            $('#product').empty();
                            $('#product').select2({
                                dropdownParent: $('#choose-item-modal'),
                                data: data,
                            });
                        }
                    });
                }
                else {
                    new PNotify({
                        type:  'warning',
                        title: 'Peringatan!',
                        text:  'Harap pilih customer dulu.'
                    });
                }
            });

            /*
             * Mengirim data pada form penambahan resource saat tombol sumbisi ditekan.
             */
            /* $('#incoming-goods-submit-button').click(function () {
                 var form = $('#incoming-good-form');
                 var data = form.serializeArray();

                $.each(data, function (key, value) {
                    if (value.name === 'shipment_date' && value.value !== '') {
                        value.value = moment(value.value, "DD/MM/YYYY").format("YYYY-MM-DD");
                    }
                })

                $.ajax({
                    url:    form.attr('action'),
                    method: form.attr('method'),
                    data:   data,

                    success: function (response) {
                        new PNotify({
                            type:  'success',
                            title: 'Berhasil',
                            text:  'Paket yang datang sudah dicatat dalam sistem.'
                        });g

                        Response.redirect('{{ route('stocks.index') }}');
                    },
                });
            }); */

            /*
             * Fungsi untuk membersihkan nilai yang dimiliki
             * setiap bidang pada sebuah form.
             */
            function clearForm($form)
            {
                $form.find('input:text, input:password, input:file, select, textarea').val([]);
                $form.find(':input[type=number]').val([]);
                $form.find('input:radio, input:checkbox').prop('checked', false);
                $form.find($('.select2-chosen')).text('');
            }
        });
    </script>
@endpush
