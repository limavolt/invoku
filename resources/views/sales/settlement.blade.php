{{-- sales/settlement.blade.php --}}

@component('components.modal', [
    'modal_id'    => 'settlement-modal',
    'modal_title' => 'pelunasan',
])
    @slot('modal_body')
        <form method="post" action="{{ route('sales.settlement', 0) }}" id="settlement-form">
                @csrf

                <p>Apakah kamu yakin akan menandai tagihan ini sudah lunas?</p>
                <input type="hidden" name="status" value="close">
    @endslot

	@slot('modal_button')
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-arrow-circle-left"></span> {{ ucwords(__('batal')) }}</button>
            <button type="submit" class="btn btn-danger" id="invoice-paid-button"><span class="fa fa-check-circle-o"></span> {{ ucwords(__('lunas')) }}</button>
        </form>
    @endslot
@endcomponent

@push('appscripts')
    <script type="text/javascript">
        $('#settlement-modal').ready(function () {
            $('#sales-table tbody').on('click', 'button[name=settlement-button]', function () {
                $('#settlement-form').attr('action', APP_URL + '/sales/settlement/' + $(this).data('id'))

                $('#settlement-modal').modal('show')
            })
        })
    </script>
@endpush
