{{-- sales/add-item.blade.php --}}

@component('components.modal', [
    'modal_id'    => 'choose-item-modal',
    'modal_title' => 'pilih produk'
])
    @slot('modal_body')
        <form id="choose-item-form" method="post">
            @csrf

            <!-- start: product input field -->
            <div id="div-product" class="form-group">
                <label class="control-label col-sm-3 text-right" for="product">
                    {{ ucfirst(e(__('nama produk'))) }}
                </label>

                <div class="col-sm-9">
                    <select id="product" class="form-control" name="product">
                        <option></option>
                    </select>

                    <span class="help-block">Barang yang berjumlah 0 tidak akan ditampilkan.</span>
                    <span id="error-product" class="help-block text-error"></span>
                </div>

                <input id="stock_id" type="hidden" name="stock_id">
                <input id="product_id" type="hidden" name="product_id">
                <input id="product_capital" type="hidden" name="product_capital">
            </div>
            <!-- end: product input field -->

            <!-- start: selling price input field -->
            <div id="div-selling_price" class="form-group">
                <label class="control-label col-sm-3 text-right" for="selling-price">
                    {{ ucfirst(e(__('harga jual'))) }}
                </label>

                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon">Rp</span>
                                <input id="selling-price-label" class="form-control" type="text" name="selling_price_label" required>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <p class="help-block">per <span id="sale-label"></span>.</p>
                        </div>

                        <input id="selling-price" type="hidden" name="selling_price" required>
                    </div>
{{--
                    <p class="help-block text-muted">
                        Harga jual barang dihitung berdasarkan satuan <span id="sale-label"></span>.
                    </p> --}}

                    <p id="default-selling-price" class="help-block text-muted"></p>
                    <span id="error-selling_price" class="help-block text-error"></span>

                    <input id="pieces" type="hidden" name="pieces">
                </div>
            </div>
            <!-- end: selling price input field -->

            <div class="form-group" id="div-count">
                <label for="count" class="control-label col-sm-3 text-right">{{ ucfirst(e(__('jumlah'))) }}</label>

                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" name="count-label" id="count-label" class="form-control" min="0" required>
                            <input type="hidden" name="count" id="count" min="0" required>
                        </div>

                        <div class="col-sm-6">
                            <p class="form-control-static" id="unit-label"></p>
                            <input type="hidden" class="form-control" id="unit" name="unit">
                        </div>

                        <div class="col-sm-12">
                            <span class="help-block text-error" id="error-count"></span>
                            <span class="help-block text-error" id="error-unit"></span>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    @endslot

    @slot('modal_button')
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-arrow-circle-left"></span> {{ ucwords(__('batal')) }}</button>
        <button type="submit" class="btn btn-primary" id="choose-item-submit"><span class="fa fa-edit"></span> {{ ucwords(__('catat')) }}</button>
    @endslot
@endcomponent

@push('vendorscripts')
    <script src="{{ asset('assets/vendor/autonumeric/autoNumeric-1.9.41.js') }}"></script>
@endpush

@push('appscripts')
    <script type="text/javascript">
        $('#choose-item-modal').ready(function () {
            // Action fired when choose-item-modal is shown.
            $('#choose-item-modal').on('shown.bs.modal', function () {
                $('#unit-label').empty();
                clearForm($('#choose-item-form'));

                $(this).find('select').each(function () {
                    $(this).select2({
                        dropdownParent: $(this).parent(),
                        placeholder:    'Pilih dari daftar'
                    });
                });

                $('#div-selling_price, #div-count').hide();
            });

            $('#selling-price-label').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 0});
            $('#selling-price-label').on('change', function () {
                $('#selling-price').val($('#selling-price-label').autoNumeric('get'));
            });

            $('#count-label').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 0});
            $('#count-label').on('change', function () {
                $('#count').val($('#count-label').autoNumeric('get'));
            });

            // Action fired when product field is changed.
            $('#product').on('change', function () {
                // Get item id
                var stock_id      = $('#product').val();
                var stock_api_url = "{{ route('api.stocks.show', ':id') }}";
                stock_api_url     = stock_api_url.replace(':id', stock_id);

                $.ajax({
                    method: 'get',
                    type:   'json',
                    url:    stock_api_url,

                    success: function (response) {
                        var customer_id = $('#customer_id').val();

                        $('#stock_id').val(response.data.id);
                        $('#product_id').val(response.data.item_id);
                        $('#product_capital').val(response.data.price);

                        {{--
                        var url_selling_price = "{{ route('ajax.get-selling-prices', ':id') }}";
                        url_selling_price     = url_selling_price.replace(':id', response.data.item_id);
                        --}}

                        var url_selling_price = "//localhost/sales/invoice/" + customer_id + "/" + response.data.item_id;

                        $.ajax({
                            method: 'get',
                            type:   'json',
                            url:     url_selling_price,

                            success: function (response) {
                                $('#selling-price').empty();
                                $('#selling-price').val(response);
                                $('#selling-price-label').empty();
                                $('#selling-price-label').val(response);
                            },

                            error: function (response) {
                                new PNotify({
                                    type:  'warning',
                                    title: 'Peringatan!',
                                    text:  'Terjadi kesalahan pada data harga jual.'
                                });
                            }
                        });

                        var url_unit = "{{ route('api.items.show', ':id') }}";
                        url_unit     = url_unit.replace(':id', response.data.item_id);

                        $.ajax({
                            method: 'get',
                            url:    url_unit,

                            success: function (response) {
                                // var quantity = 0;
                                // if (response.data.quantities.length > 0) {
                                //     for (let index = 0; index < response.data.quantities.length; index++) {
                                //         quantity = quantity + response.data.quantities[index].count;
                                //     }
                                // }

                                quantity = response.data.stocks[0].count;

                                category = response.data.item_group.name.substring(0, 1).toUpperCase() + response.data.item_group.name.substring(1).toLowerCase();

                                $('#count-label').attr('max', quantity);
                                $('#count').attr('max', quantity);

                                $('#unit-label').empty();
                                $('#unit-label').append(response.data.unit.label + ' yang berisi ' + response.data.unit.pieces + ' ' + response.data.unit.pieces_label);
                                $('#unit').val(response.data.unit.id);

                                $('#unit-label-only').empty();
                                $('#unit-label-only').append(response.data.unit.label);

                                $('#content-label-only').empty();
                                $('#content-label-only').append(response.data.unit.pieces_label);
                            },
                        });

                        // Get selling price settings.
                        unitPrice(response.data.id);

                    },

                    error: function () {
                        new PNotify({
                            type:  'warning',
                            title: 'Peringatan!',
                            text:  'Terjadi kesalahan pada data stok.'
                        });
                    }
                });

                $('#div-selling_price, #div-count').show();
            });

            // Remove items that have been selected on the form.
            $('#item-list-table tbody').on('click', 'button[name=delete-item-button]', function () {
                $(this).parents('tr').remove();

                var total = 0;

                $('.item-list').each(function () {
                    var price    = $('.item-value', this).val();
                    var count    = $('.item-count', this).val();
                    var subtotal = price * count;

                    total += subtotal;
                })

                $('input#total').val('');
                $('input#total').val(total);

                total = Intl.NumberFormat('id-ID').format(total);

                $('span#total-displayed').empty();
                $('span#total-displayed').append('Rp' + total);

                var profits = 0;

                $('.item-list').each(function () {
                    var price    = $('.item-unit-value', this).val();
                    var capital  = $('.item-capital', this).val();
                    var count    = $('.item-count', this).val()
                    var sale     = price * count;
                    var purchase = capital * count;
                    var profit   = sale - purchase;

                    profits += profit;
                });


                $('input#profit').val('');
                $('input#profit').val(profits);

                profits = Intl.NumberFormat('id-ID').format(profits);

                $('span#profit-displayed').empty();
                $('span#profit-displayed').append('Rp' + profits);
            });

            // $('#div-selling_price').on('show', function () {

            //     var url_item_stock    = "{{ route('api.stocks.show', ':id') }}";


            //     var url_default_selling_price = "{{ route('ajax.get-default-selling-prices', ':id') }}";
            //     var url_stock         = "{{ route('api.stocks.select2.item', ':item_id') }}";

            //     url_item_stock   = url_item_stock.replace(':id', item_id);


            //     url_default_selling_price = url_default_selling_price.replace(':id', item_id);
            //     url_stock         = url_stock.replace(':item_id', item_id);

            //     $.ajax({
            //         method: 'get',
            //         type:   'json',
            //         url:    url_default_selling_price,

            //         success: function (response) {
            //             $('#default-selling-price').empty();
            //             $('#default-selling-price').append(
            //                 'Harga jual standarnya adalah ' + Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(response.selling_price)
            //             )
            //         }
            //     })

            //     $('#div-stock, #div-selling_price, #div-count').show();
            // });

            /*
             * Memproses formulir penambahan produk ketika tombol
             * submit ditekan.
             */
            $('#choose-item-submit').click(function(e) {
                storeItem();
            });

            // $(document).on('click', '#sum-button', function () {
            //     var total = 0;

            //     $('.item-value').each(function () {
            //         total += Number($(this).val());
            //     });

            //     $('#total').val('');
            //     $('#total').val(total);
            // });

            // Function to retrieve unit and sub-unit data from selected products.
            function unitPrice(stock_id) {
                var url = "{{ route('api.stocks.show', ':id') }}";
                url     = url.replace(':id', stock_id);

                $.ajax({
                    method: 'get',
                    url:    url,

                    success: function (response) {
                        var sale     = response.data.sale;

                        var item_url = "{{ route('api.items.show', ':item_id') }}";
                        item_url     = item_url.replace(':item_id', response.data.item_id);

                        $.ajax({
                            method: 'get',
                            url:    item_url,

                            success: function (response) {
                                if (sale == 'unit') {
                                    $('#sale-label').empty();
                                    $('#sale-label').append(response.data.unit.label);
                                    $('#pieces').val(1).change();
                                }
                                else {
                                    $('#sale-label').empty();
                                    $('#sale-label').append(response.data.unit.pieces_label);
                                    $('#pieces').val(response.data.unit.pieces).change();
                                }
                            }
                        });
                    }
                });
            }

            /*
             * Fungsi untuk menyimpan data pilihan produk sehingga
             * dapat digunakan formulir pencatatan penerimaan produk.
             */
            function storeItem() {
                var total = $('#total').val();
                var total = $('#profit').val();

                if ($('#count').val() <= $('#count').attr('max')) {
                    $.ajax({
                        method: "post",
                        url:    "{{ route('items.choose-item') }}",
                        data:   $('#choose-item-form').serialize(),

                        success: function(response) {
                            $('#selected-item-list').append(response);
                            $('#choose-item-modal').modal('hide');

                            var total = 0;

                            $('.item-list').each(function () {
                                var price = $('.item-unit-value', this).val()
                                var count = $('.item-count', this).val()
                                var subtotal = price * count

                                total += subtotal
                            })

                            var profits = 0;

                            $('.item-list').each(function () {
                                var price    = $('.item-unit-value', this).val();
                                var capital  = $('.item-capital', this).val();
                                var count    = $('.item-count', this).val()
                                var sale     = price * count;
                                var purchase = capital * count;
                                var profit   = sale - purchase;

                                profits += profit;
                            });

                            // var total = $('#total').val();

                            // $('.item-list').each(function () {
                            //     total = Number(total) + (Number($('.item-value').val()) * Number($('.item-count').val()));
                            // });


                            $('input#total').val('');
                            $('input#total').val(total);

                            total = Intl.NumberFormat('id-ID').format(total);

                            $('span#total-displayed').empty();
                            $('span#total-displayed').append('Rp' + total);

                            $('input#profit').val('');
                            $('input#profit').val(profits);

                            profits = Intl.NumberFormat('id-ID').format(profits);

                            $('span#profit-displayed').empty();
                            $('span#profit-displayed').append('Rp' + profits);
                        },

                        error: function(response) {
                            if (response.status == 422) {
                                var errors = response.responseJSON;

                                new PNotify({
                                    type:  'warning',
                                    title: 'Peringatan!',
                                    text:  'Terdapat kesalahan pada data yang dimasukan.'
                                });

                                $.each(errors.errors, function (col_val, msg) {
                                    $('#div-' + col_val).addClass('has-error');
                                    $('#error-' + col_val).html(msg[0]);
                                });
                            }
                            else {
                                systemError();
                            }
                        }
                    });
                }
                else {
                    new PNotify({
                        type:  'warning',
                        title: 'Peringatan!',
                        text:  'Jumlah yang dijual lebih banyak dibandingkan stok.'
                    });
                }
            }
        });
    </script>
@endpush
