{{-- sales/print.blade.php --}}

@extends('layouts.print')

@section('content')
    <header>
        <table>
            <tr class="text-top">
                <td class="width-1-3">
                    <img class="text-top logo-toko" src="{{ public_path('img/logo-toko.png') }}" alt="Logo toko">
                    <p>{{ ucfirst(__('sales')) }}: MA</p>
                </td>

                <td class="width-1-3 text-center">
                    <h3 style="margin-bottom: 0;"><strong>{{ strtoupper(__('nota penjualan')) }}</strong></h3>
                    <p style="margin-top: 0;">{{ strtoupper(e(settings()->get('invoice_code'))) . \Carbon\Carbon::parse($invoice->issued_date)->format('ym') . sprintf('%03d', $invoice->number) }}</p>
                </td>

                <td class="width-1-3 text-right">
                    <p>{{ strtoupper(__('kepada yth.')) }}<br />
                    <span style="font-size: 1.2 em;">{{ $invoice->customer->name }}</span><br />
                    <span>{{ $invoice->customer->address }}</span></p>

                    <p>{{ ucwords(__('tanggal nota')) }}: {{ \Carbon\Carbon::parse($invoice->issued_date)->format('d-M-Y') }}</p>
                </td>
            </tr>
        </table>
    </header>

    <footer>
        <table>
            <tr>
                <td class="text-left">
                    <p>{{ ucfirst(__('penerima/cap toko')) }}</p>
                </td>

                <td></td>

                <td class="text-right">
                    <p>{{ ucfirst(__('diperiksa oleh')) }}</p>
                </td>
            </tr>
        </table>
    </footer>

    <main>
        <table class="table">
            <thead>
                <tr>
                    <th class="width-1-5">{{ strtoupper(__('nama barang')) }}</th>
                    <th class="width-1 text-right">{{ strtoupper(__('qty')) }}</th>
                    <th class="width-1 text-right">{{ strtoupper(__('harga')) }}</th>
                    <th class="width-1 text-right">{{ strtoupper(__('jumlah')) }}</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($invoice->productLists as $item)
                    @php $count = $loop->count; @endphp

                    <tr>
                        <td>
                            {{ strtoupper($item->stock->item->name) }} ({{ '@' . $item->stock->item->unit->pieces . $item->stock->item->unit->pieces_label }})
                        </td>

                        <td class="text-right">
                            {{ $item->stock->sale == 'unit' ? abs($item->count) . $item->stock->item->unit->label : abs($item->count * $item->stock->item->unit->pieces) . $item->stock->item->unit->pieces_label }}
                        </td>

                        <td class="text-right">
                            {{ number_format($item->price, 0, ',', '.') }}
                        </td>

                        <td class="text-right">
                            {{ number_format((($item->stock->sale == 'unit' ? $item->price : ($item->price * $item->stock->item->unit->pieces)) * abs($item->count)), 0, ',', '.') }}
                        </td>

                    </tr>

                    @if (0 === $loop->iteration % 12)
                        <tr>
                            <td style="border-top: 1px solid black; border-left: 0px;"></td>
                            <td class="text-right text-bottom" style="border-top: 1px solid black; font-size: 1 em; border-bottom: 1px solid black;">{{ strtoupper(__('tempo bayar')) }}</td>
                            <td class="text-right text-bottom" style="border-top: 1px solid black; font-size: 1 em;border-bottom: 1px solid black">{{ \Carbon\Carbon::parse($invoice->due_date)->diffInDays(\Carbon\Carbon::parse($invoice->issued_date)) !== 0 ? \Carbon\Carbon::parse($invoice->due_date)->diffInDays(\Carbon\Carbon::parse($invoice->issued_date)) . ' ' . strtoupper(__('hari')) : strtoupper(__('tunai')) }}</td>
                            <td style="border-top: 1px solid black; border-right: 0px;"></td>
                        </tr>

                        <tr>
                            <td style="border-left: 0px;"></td>
                            <td class="text-right" style="border-bottom: 1px solid black;">{{ strtoupper(__('jatuh tempo')) }} <hr></td>
                            <td class="text-right text-bottom" style="border-bottom: 1px solid black;">{{ \Carbon\Carbon::parse($invoice->due_date)->format('d-M-Y') }} <hr></td>
                            {{-- <th class="text-right text-bottom" style="border-top: 1px solid black; border-bottom: 1px solid black;">{{ strtoupper(__('jatuh tempo')) }} <hr /></th>
                            <td class="text-bottom" style="border-top: 1px solid black; border-bottom: 1px solid black;">{{ \Carbon\Carbon::parse($invoice->due_date)->format('d/m/Y') }} <hr /></td> --}}
                            <td style="border-right: 0px;"></td>
                        </tr>
                    @endif
                @endforeach

                @if (0 !== $count % 12)
                    @for ($i = 0; $i < 12 - $count % 12; $i++)
                        <tr>
                            <td>-</td>
                            <td class="text-right">-</td>
                            <td class="text-right">-</td>
                            <td class="text-right">-</td>
                        </tr>
                    @endfor
                @endif
            </tbody>

            <tfoot>
                <tr>
                    <th class="text-right" style="border-bottom: 1px solid black;" colspan="3">{{ strtoupper(__('total')) }}</th>
                    <th class="text-right" style="border-bottom: 1px solid black;">Rp{{ number_format($invoice->total, 0, ',', '.') }}</th>
                </tr>

                <tr>
                    <td rowspan="2" style="border-top: 1px solid black; border: 0px; font-size: 0.6 em;">
                        ** KOMPLAIN/RETUR LEBIH DARI 2 MINGGU TIDAK DITERIMA ATAU DILAYANI<br>
                        ** HARGA TIDAK MENGIKAT DAN SEWAKTU-WAKTU DAPAT BERUBAH
                    </td>
                    {{-- <td style="border-top: 1px solid black; border: 0px;"></td> --}}
                    <td class="text-right" style="border-top: 1px solid black; font-size: 1 em;">{{ strtoupper(__('tempo bayar')) }}</td>
                    <td class="text-right" style="border-top: 1px solid black; font-size: 1 em;">{{ \Carbon\Carbon::parse($invoice->due_date)->diffInDays(\Carbon\Carbon::parse($invoice->issued_date)) !== 0 ? \Carbon\Carbon::parse($invoice->due_date)->diffInDays(\Carbon\Carbon::parse($invoice->issued_date)) . ' ' . strtoupper(__('hari')) : strtoupper(__('tunai')) }}</td>
                    <td style="border-top: 1px solid black; border-right: 0px;"></td>
                </tr>

                <tr style="font-size: 1 em;">
                    {{-- <td style="border-top: 0; border: 0px;"></td> --}}
                    {{-- <td style="border-top: 0; border: 0px;"></td> --}}
                    <td class="text-right" style="border-bottom: 1px solid black;">{{ strtoupper(__('jatuh tempo')) }}</td>
                    <td class="text-right" style="border-bottom: 1px solid black;">{{ \Carbon\Carbon::parse($invoice->due_date)->format('d-M-Y') }}</td>
                    <td style="border-top: 0; border-right: 0px;"></td>
                </tr>
            </tfoot>
        </table>
    </main>

@endsection

@push('printstyles')
    <style>
        @page {
            size: 21.5cm 14cm;

            margin-top: 0.5cm;
            margin-bottom: 0.3cm;
            margin-left  : 1.7cm;
            margin-right : 1.7cm;
        }

        body {
            font-size  : 14px;
            font-family: Arial, Helvetica, sans-serif;

            -webkit-text-size-adjust: 100%;

            margin-top   : 2.7cm;
            margin-bottom: 1cm;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-top {
            vertical-align: top;
        }

        .text-bottom {
            vertical-align: bottom;
        }

        .text-small {
            font-size: 8px;
        }

        table {
            width    : 100%;
            max-width: 100%;
        }

        .width-1-3 {
            width: 33.33333%;
        }

        .width-1-4 {
            width: 25%;
        }

        .width-1-5 {
            width: 20%;
        }

        .width-2 {
            width: 16.66666666666667%;
        }

        .width-1 {
            width: 8.333333333333333%;
        }

        table.table {
            page-break-inside: auto;
        }

        table.table tr {
            page-break-inside: avoid;
            page-break-after : auto;
        }

        table.table tr.break {
            page-break-after: always;
        }

        .table {
            width    : 100%;
            max-width: 100%;

            /* border         : 1px solid black; */
            border-collapse: collapse;
            border-spacing : 0;
        }

        .table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
            border-left : 1px solid black;
            border-right: 1px solid black;

            padding-left: 4px;
            padding-right: 4px;

            vertical-align: top;
        }

        .table > tfoot > tr > th,
        .table > tfoot > tr > td {
            border-top: 1px solid black;
        }

        .table > thead > tr > th {
            border: 1px solid black;
            vertical-align: bottom;
        }

        header {
            position: fixed;

            top   : 0;
            left  : 0;
            right : 0;
        }

        footer {
            position: fixed;

            left  : 0;
            right : 0;
            bottom: 0;

            height: 2.5cm;
        }

        .logo-toko {
            height: 1.6cm;
        }
    </style>

    {{-- <style>
        @page {
            size: 21.5cm 14cm;

            margin-top   : 0.3cm;
            margin-right : 0.3cm;
            margin-left  : 0.3cm;
            margin-bottom: 0.3cm;
        }

        .page-break {
            page-break-after: always;
        }

        body {
            font-size  : 10pt;
            font-family: Verdana, Geneva, Tahoma, sans-serif;

            margin-top   : 3cm;
            margin-bottom: 4cm;
        }

        header {
            position: fixed;

            top   : 0;
            left  : 0;
            right : 0;

            height: 3cm;
        }

        footer {
            position: fixed;

            left  : 0;
            right : 0;
            bottom: 0;

            height: 4cm;
        }

        table {
            width    : 100%;
            max-width: 100%;
        }

        .table {
            border         : 1px solid black;
            border-collapse: collapse;
            border-spacing : 0;
        }

        .table > thead {
            display: table-header-group;
        }

        .table > tbody {
            display: table-row-group;
        }

        .table > tfoot {
            display: table-footer-group;
        }

        .table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
            border        : 1px solid black;
            padding       : 0.3em;
            vertical-align: top;
        }

        .table > tbody > tr > td {
            border-bottom : 0;
            border-top    : 0;
        }

        .table {
            page-break-inside: auto;
        }

        .table tr
        .table td, {
            page-break-inside: avoid;
            page-break-after: auto;
        }

        .text-right {
            text-align: right;
        }

        .text-center {
            text-align: center;
        }

        .text-top {
            vertical-align: top;
        }

        .text-bottom {
            vertical-align: bottom;
        }

        .text-logo {
            font-size: 1em;
        }
    </style> --}}
@endpush
