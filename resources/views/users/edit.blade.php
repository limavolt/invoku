{{-- users/edit.blade.php --}}

@component('components.modal', [
	'modal_id'    => 'modal-edit-user',
	'modal_title' => 'perbarui informasi pengguna'
])
	@slot('modal_body')
		<form method="put" action="{{ route('users.update', 0) }}" id="form-edit-user">
			@csrf
			@include('users._form')

			<div class="form-group mt-lg">
				<div class="col-sm-9 col-sm-offset-3">
					<button class="btn btn-default" id="change-password-button" type="button">{{ ucwords(__('ganti sandi')) }}</button>
				</div>
			</div>

			<div class="form-group mt-lg" id="div-password" style="visibility: hidden;">
				<label for="password" class="control-label col-sm-3 text-right">
					{{ ucfirst(__('kata sandi')) }}
				</label>

				<div class="col-sm-9">
					<input type="password" id="password" name="password" class="form-control">
					<span class="help-block text-error" id="label-password"></span>
				</div>
			</div>

			<div class="form-group mt-lg" id="div-password_confirmation" style="visibility: hidden;">
				<label for="password_confirmation" class="control-label col-sm-3 text-right">
					{{ ucfirst(__('konfirmasi kata sandi')) }}
				</label>

				<div class="col-sm-9">
					<input type="password" id="password_confirmation" name="password_confirmation" class="form-control">
					<span class="help-block text-muted">Harus sama dengan yang diisi pada bidang kata sandi di atas.</span>
					<span class="help-block text-error" id="label-password_confirmation"></span>
				</div>
			</div>
	@endslot

	@slot('modal_button')
			<button type="button" class="btn btn-default" data-dismiss="modal">{{ ucwords(__('batal')) }}</button>
			<button type="button" class="btn btn-primary" id="btn-edit-user">{{ ucwords(__('ubah')) }}</button>
		</form>
	@endslot
@endcomponent

@push('appscripts')
	<script type="text/javascript">
		$('#modal-edit-user').ready(function () {
			$('#users-table tbody').on('click', 'button[name="btn-edit-user"]', function () {
				var data     = table.row($(this).closest('tr')).data();
				var selected = null;

				$.each($('input, select, textarea', '#form-edit-user'), function () {
					if ($(this).attr('id')) {
						var id_element = $(this).attr('id');

						if (data[id_element]) {
							$('#' + id_element, '#form-edit-user').val(data[id_element]).trigger('change');
						}
						else {
							$('#' + id_element, '#form-edit-user').val('').trigger('change');
						}
					}
				});

				$('#form-edit-user').attr('action', APP_URL + '/users/' + $(this).data('id'));

				cleanModal('#form-edit-user', false);

				$('#modal-edit-user').modal('show');
			});

			$('#change-password-button').click(function () {
				$('#div-password, #div-password_confirmation').css('visibility', 'visible');
			});

			$('#btn-edit-user').click(function () {
				var form = $('#form-edit-user');

				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function (response) {
						generateSuccess(
							$('#modal-edit-user'),
							'Informasi pengguna ' + response.data.name + ' berhasil diperbarui.'
						);
					},

					error: function (response) {
						if (response.status == 422) {
							generateError(
								'#form-edit-user',
								response.responseJSON
							);
						}
						else {
							systemError();
						}
					}
				});

				$('#div-password, #div-password_confirmation').css('visibility', 'hidden');
			});
		});
	</script>
@endpush
