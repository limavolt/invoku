{{-- users/create.blade.php --}}

@component('components.modal', [
	'modal_id'    => 'modal-add-user',
	'modal_title' => 'tambah pengguna'
])
	@slot('modal_body')
		<form method="post" action="{{ route('users.store') }}" id="form-add-user">
			@csrf
			@include('users._form')
	@endslot

	@slot('modal_button')
			<button type="button" class="btn btn-default" data-dismiss="modal">{{ ucwords(__('batal')) }}</button>
			<button type="button" class="btn btn-primary" id="btn-add-user">{{ ucwords(__('tambah')) }}</button>
		</form>
	@endslot
@endcomponent

@push('appscripts')
	<script type="text/javascript">
		$('#modal-add-user').ready(function () {
			$('#modal-add-user').on('shown.bs.modal', function () {
				cleanModal('#form-add-user', true);
			});

			$('#btn-add-user').click(function () {
				var form = $('#form-add-user');

				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function (response) {
						generateSuccess(
							$('#modal-add-user'),
							'Pengguna ' + response.data.name + ' berhasil didaftarkan.'
						);
					},

					error: function (response) {
						if (response.status == 422) {
							generateError(
								'#form-add-user',
								response.responseJSON
							);
						}
						else {
							systemError();
						}
					}
				});
			});
		});
	</script>
@endpush
