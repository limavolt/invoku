{{-- users/_form.blade.php --}}

<div class="form-group mt-lg" id="div-name">
	<label class="col-sm-3 control-label text-right">
		{{ ucfirst(__('nama lengkap')) }}
	</label>

	<div class="col-sm-9">
		<input type="text" id="name" name="name" class="form-control" required autofocus>
		<span class="help-block text-muted">{{ ucfirst(__('wajib diisi')) }}.</span>
		<span class="help-block text-error" id="label-name"></span>
	</div>
</div>

<div class="form-group mt-lg" id="div-email">
	<label class="col-sm-3 control-label text-right">
		{{ ucfirst(__('alamat email')) }}
	</label>

	<div class="col-sm-9">
		<input type="email" id="email" name="email" class="form-control" required>
		<span class="help-block text-muted">{{ ucfirst(__('wajib diisi')) }}.</span>
		<span class="help-block text-error" id="label-email"></span>
	</div>
</div>

<div class="form-group mt-lg" id="div-username">
	<label class="col-sm-3 control-label text-right">
		{{ ucfirst(__('username')) }}
	</label>

	<div class="col-sm-9">
		<input type="text" id="username" name="username" class="form-control" required>
		<span class="help-block text-muted">{{ ucfirst(__('wajib diisi')) }}.</span>
		<span class="help-block text-muted" id="label-username"></span>
	</div>
</div>
