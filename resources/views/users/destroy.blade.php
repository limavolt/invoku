{{-- users/destroy.blade.php --}}

@component('components.modal', [
	'modal_id'    => 'modal-destroy-user',
	'modal_title' => 'hapus pengguna'
])
	@slot('modal_body')
		<form method="post" action="{{ route('users.destroy', 0) }}" id="form-destroy-user">
			@csrf
			@method('DELETE')

			<p>Apakah anda yakin akan menghapus data ini?</p>
	@endslot

	@slot('modal_button')
			<button type="button" class="btn btn-default" data-dismiss="modal">{{ ucwords(__('batal')) }}</button>
			<button type="button" class="btn btn-danger" id="btn-destroy-user">{{ ucwords(__('hapus')) }}</button>
		</form>
	@endslot
@endcomponent

@push('appscripts')
	<script type="text/javascript">
		$('#modal-destroy-user').ready(function () {
			$('#users-table tbody').on('click', 'button[name="btn-destroy-user"]', function () {
				$('#form-destroy-user').attr('action', APP_URL + '/users/' + $(this).data('id'));
				$('#modal-destroy-user').modal('show');
			});

			$('#btn-destroy-user').click(function () {
				var form = $('#form-destroy-user');

				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function (response) {
						generateSuccess(
							$('#modal-destroy-user'),
							response.message
						);
					},

					error: function (response) {
						if (response.status == 422) {
							generateError(
								'#form-destroy-user',
								response.responseJSON
							);
						}
						else {
							systemError();
						}
					}
				});
			});
		});
	</script>
@endpush
