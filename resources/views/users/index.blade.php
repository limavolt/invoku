{{-- users/index.blade.php --}}

@extends('layouts.dashboard', ['page_title' => 'pengaturan aplikasi'])

@section('content')
	<p class="lead">
		Di sini kamu dapat mengatur pengguna aplikasi ini. Kamu dapat melihat
		daftar pengguna terdaftar, menambahkan pengguna, memperbarui informasi pengguna,
		dan menghapus pengguna yang ada.
	</p>

	@component('components.panel', ['panel_title' => 'daftar pengguna sistem'])
		<div class="mb-lg">
			<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal-add-user">
				<span class="fa fa-plus"></span>
				{{ ucwords(__('tambah pengguna')) }}
			</button>
		</div>

		@component('components.datatable-ajax', [
			'table_id'      => 'users',
			'table_headers' => ['nama', 'email', 'username'],
			'condition'     => true,
			'data'          => [
				['name' => 'name', 'data' => 'name'],
				['name' => 'email', 'data' => 'email'],
				['name' => 'username', 'data' => 'username']
			],
			'renders'       => [
				['text_align' => 'text-right', 'column_target' => '4']
			]
		])
			@slot('data_send_ajax')
			@endslot
		@endcomponent
	@endcomponent

	<h3>Keterangan</h3>

	<p>
		Pengguna adalah orang yang memiliki akses untuk menggunakan aplikasi ini. Jadi,
		hanya buatkan akun pengguna kepada yang kamu percayai.
	</p>

	@include('users.create')
	@include('users.edit')
	@include('users.destroy')
@endsection

@push('vendorstyles')
	<link href="{{ asset('assets/vendor/pnotify/pnotify.custom.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/vendor/select2-4.0.8/css/select2.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/vendor/select2-bootstrap/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
@endpush

@push('vendorscripts')
	<script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendor/select2-4.0.8/js/select2.min.js') }}" type="text/javascript"></script>
@endpush
