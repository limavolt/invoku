{{-- stocks/index.blade.php --}}

@extends('layouts.dashboard', ['page_title' => 'stok barang'])

@section('content')
    <!-- start: content lead -->
    <div class="row">
        <div class="col-md-6">
            <div class="alert alert-info">
                <p class="lead text-center">
                    Jumlah barang kamu ada <strong><span id="item-total"></span></strong>.
                </p>
            </div>
        </div>

        <div class="col-md-6">
            <div class="alert alert-success">
                <p class="lead text-center">
                    Nilai total stok kamu sebesar <strong>Rp<span id="valuation"></span></strong>.
                </p>
            </div>
        </div>
    </div>
    <!-- end: content lead -->

    <!-- start: stock panel -->
    @component('components.panel', ['panel_title' => 'daftar stok barang'])
        <!-- start: toolbar -->
        <div class="row mb-md">
            <!-- start: left toolbar -->
            <div class="col-md-6">
                <div class="btn-group">
                    <a href="{{ route('sales.create') }}" class="btn btn-default">
                        <span class="fa fa-print"></span>
                        {{ ucwords(__('gawe faktur')) }}
                    </a>

                    <a href="{{ route('incoming-goods.create') }}" class="btn btn-default">
                        <span class="fa fa-truck"></span>
                        {{ ucwords(__('barang datang')) }}
                    </a>
                </div>
            </div>
            <!-- end: left toolbar -->

            <!-- start: right toolbar -->
            <div class="col-md-6 text-right">
                <label for="item-group-filter">{{ ucfirst(__('hanya tampilkan barang di kategori')) }}</label>

                <select id="item-group-filter" class="col-md-4" name="item-group-filter">
                    <option></option>

                    @foreach ($item_groups as $group)
                        <option value="{{ $group->name }}">{{ $group->name }}</option>
                    @endforeach
                </select>
            </div>
            <!-- end: right toolbar -->
        </div>
        <!-- end: toolbar -->

        <!-- start: information table -->
        @component('components.datatable-ajax', [
            'table_id'      => 'stocks',
            'table_headers' => ['kategori', 'merek', 'nama barang', 'modal', '', 'stok', '', 'nilai'],
            'data'          => [
                ['name' => 'categories',  'data' => 'categories'],
                ['name' => 'brands',      'data' => 'brands'],
                ['name' => 'item.name',   'data' => 'item.name'],
                ['name' => 'price',       'data' => 'price', 'render' => "$.fn.dataTable.render.number( '.', ',', 2, 'Rp' )"],
                ['name' => 'price_label', 'data' => 'price_label', 'orderable' => 'false', 'searchable' => 'false'],
                ['name' => 'count',       'data' => 'count'],
                ['name' => 'units',       'data' => 'units', 'orderable' => 'false', 'searchable' => 'false'],
                ['name' => 'values',      'data' => 'values', 'render' => "$.fn.dataTable.render.number( '.', ',', 2, 'Rp' )"],
            ],
            'renders'       => [
                ['text_align' => 'text-right', 'column_target' => '4, 6, 8']
            ],
            'default_order' => 3,
            'callback'      => "$('#valuation').empty(); $('#valuation').append($.number($('#stocks-table').DataTable().column(8).data().sum(), 0, ',', '.'));"
                            .  "$('#item-total').empty(); $('#item-total').append($.number($('#stocks-table').DataTable().column(6).data().sum(), 0, ',', '.'))",
        ])
            @slot('data_send_ajax')
            @endslot
        @endcomponent
        <!-- end: information table -->
    @endcomponent
    <!-- end: stock panel -->
@endsection

@push('vendorstyles')
    <!-- start: stock index styles -->
    <link href="{{ asset('assets/vendor/select2-4.0.8/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendor/select2-bootstrap/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendor/pnotify/pnotify.custom.css') }}" rel="stylesheet" type="text/css">
    <!-- end: stock index styles -->
@endpush

@push('vendorscripts')
    <!-- start: stock index vendor scripts -->
    <script src="{{ asset('assets/vendor/select2-4.0.8/js/select2.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/sum.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.number.min.js') }}" type="text/javascript"></script>
    <!-- end: stock index vendor scripts -->
@endpush

@push('appscripts')
    <!-- start: stock index app scripts -->
    <script type="text/javascript">
        /* Initialize the page. */
        $(document).ready(function () {

            // Set select element as select2 element.
            $(document).find('select').each(function () {
                $(this).select2({
                    placeholder: "{{ ucfirst(__('pilih dari daftar')) }}",
                    allowClear:  true,
                });
            });
        });

        /* Table filter function. */
        $('#item-group-filter').on('change', function () {
            setFilteredData('#item-group-filter', 1);
        });
    </script>
    <!-- end: stock index app scripts -->
@endpush
