{{-- components/datatable-ajax.blade.php --}}

<table class="table table-hover table-striped table-condensed" id="{{ $table_id }}-table" style="width: 100%;">
	<thead>
		<tr>
			<th class="col-sm-1">NO</th>

			@foreach($table_headers as $table_header)
				<th>{{ strtoupper(e(__($table_header))) }}</th>
			@endforeach

			@if(isset($condition) && true === $condition)
				<th>{{ strtoupper(e(__('aksi'))) }}</th>
			@endif
		</tr>
	</thead>
</table>

@push('vendorstyles')
	<link href="{{ asset('assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" rel="stylesheet" type="text/css">
@endpush

@push('vendorscripts')
	<script src="{{ asset('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}" type="text/javascript"></script>
@endpush

@push('appscripts')
	<script type="text/javascript">
		$(document).ready(function() {
			/* Mempersiapkan data untuk ditampilkan oleh datatable. */
			table = $("#{{ $table_id }}-table").DataTable({
				processing: true,
				serverSide: false,
                responsive: true,
                pageLength: 100,
                language: {
                    sEmptyTable:   "Tidak ada data yang tersedia pada tabel ini",
                    sProcessing:   "Sedang memproses...",
                    sLengthMenu:   "Tampilkan _MENU_ entri",
                    sZeroRecords:  "Tidak ditemukan data yang sesuai",
                    sInfo:         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    sInfoEmpty:    "Menampilkan 0 sampai 0 dari 0 entri",
                    sInfoFiltered: "(disaring dari _MAX_ entri keseluruhan)",
                    sInfoPostFix:  "",
                    sSearch:       "",
                    sUrl:          "",
                    oPaginate: {
                        sFirst:    "Pertama",
                        sPrevious: "Sebelumnya",
                        sNext:     "Selanjutnya",
                        sLast:     "Terakhir"
                    }
                },
                drawCallback: function () {
                    {!! isset($callback) ? $callback : "" !!}
                },
				ajax: {
					url: '{{ route('ajax.' . $table_id . '.data') }}',
					dataType: "json",
					type: "POST",
					data: function(d) {
						{{ $data_send_ajax }}
					}
				},
				columns: [
					{ targets: 0, data: 'null', defaultContent: '', orderable: false, searchable: false },

					@foreach ($data as $value)
						{
							data: '{{ $value['data'] }}',
							name: '{{ $value['name'] }}',
							orderable: {{ isset($value['orderable']) ? $value['orderable'] : 'true' }},
							searchable: {{ isset($value['searchable']) ? $value['searchable'] : 'true' }},
							{!! isset($value['render']) ? "render: " . $value['render'] : '' !!}
						},
					@endforeach

					@if (isset($condition) && true === $condition)
						{ data: 'action', name : 'action', orderable: false, searchable: false },
					@endif
				],
				columnDefs: [
					{ responsivePriority: 1, targets: 0 },

					@if(isset($renders))
						@foreach ($renders as $render)
							{ className: "{{ $render['text_align'] }}", "targets": [ {{ $render['column_target'] }} ] }
						@endforeach
					@endif
				],

				@if (isset($default_order))
					order: [{{ $default_order }},'asc'],
				@else
					order: [1,'asc'],
				@endif
			});

			/* Membuat nomor urut pada datatable. */
			table.on('draw.dt', function () {
				var info = table.page.info();

				table.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
					cell.innerHTML = i + 1 + info.start;
				});
			});

			{{--
			$('button[name="btn-reset"]').on('click', function() {
				reset();
			});

			$('select').on('change', function () {
				table.draw();
			});

			function reset() {
				$(".js-example-basic-single").val("");
				$(".js-example-basic-single").select2();
				table.search('');
				table.columns().search('');
				table.draw();
			}

			function filter() {
				table.draw();
			}
			--}}
		});
	</script>
@endpush
