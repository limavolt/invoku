{{-- components/dashboard-panel.blade.php --}}

<section class="panel {{ isset($panel_context) ? $panel_context : '' }}">
    @if (isset($panel_title))
        <header class="panel-heading">
            <h2 class="panel-title">{{ $panel_title }}</h2>

            @if ($panel_subtitle)
                <p class="panel-subtitle">{{ $panel_subtitle }}</p>
            @endif
        </header>
    @endif

    @if (isset($panel_icon))
        <header class="panel-heading {{ isset($icon_background) ? $icon_background : 'bg-white' }}">
            <div class="panel-heading-icon">
                <span class="fa fa-{{ $panel_icon }}"></span>
            </div>
        </header>
    @endif

    <div class="panel-body">
        <div class="thumb-info mb-md">
            {{ $slot }}
        </div>

        @if (isset($panel_footer_body))
            <hr>

            {{ $panel_footer_body }}
        @endif
    </div>

    @if (isset($panel_footer))
        <div class="panel-footer">
            {{ $panel_footer }}
        </div>
    @endif
</section>
