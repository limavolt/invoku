{{-- components/sidebar.blade.php --}}

<!-- start: sidebar -->
<aside class="sidebar-left" id="sidebar-left">
	<!-- start: sidebar header -->
	<div class="sidebar-header">
		<div class="sidebar-title text-muted">
			{{ ucfirst(e(__('navigasi'))) }}
		</div>

		<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
			<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
		</div>
	</div>
	<!-- end: sidebar header -->

	<!-- start: sidebar content -->
	<div class="nano">
		<div class="nano-content">
            <!-- start: navigation -->
			<nav class="nav-main" id="menu" role="navigation">
				<ul class="nav nav-main">
                    <!-- start: stock list menu -->
					<li>
						<a href="{{ route('stocks.index') }}">
							<i class="fa fa-cubes" aria-hidden="true"></i>
							<span>{{ ucfirst(e(__('daftar stok total barang'))) }}</span>
						</a>
                    </li>
                    <!-- end: stock menu -->

                    <!-- start: invoice creation menu -->
					<li>
						<a href="{{ route('sales.create') }}">
							<i class="fa fa-print" aria-hidden="true"></i>
							<span>{{ ucfirst(e(__('gawe nota penjualan'))) }}</span>
						</a>
                    </li>
                    <!-- end: invoice creation menu -->

                    <!-- start: invoice list menu -->
					<li>
						<a href="{{ route('sales.index') }}">
							<i class="fa fa-list"></i>
							<span>{{ ucfirst(e(__('daftar piutang pelanggan'))) }}</span>
						</a>
					</li>
                    <!-- end: invoice list menu -->

					<hr>

                    <!-- start: addressbook menu -->
					<li class="nav-parent">
						<a>
							<i class="fa fa-book"></i>
							<span>{{ ucfirst(__('kontak')) }}</span>
						</a>

						<ul class="nav nav-children">
							<li>
								<a href="{{ route('suppliers.index') }}">
									{{ ucfirst(__('daftar supplier')) }}
								</a>
                            </li>

							<li>
								<a href="{{ route('customers.index') }}">
									{{ ucfirst(__('daftar pelanggan')) }}
								</a>
							</li>

							<li>
								<a href="{{ route('companies.index') }}">
									{{ ucfirst(__('semua kontak')) }}
								</a>
							</li>
						</ul>
                    </li>
                    <!-- end: addressbook menu -->

                    <!-- start: product management menu -->
					<li class="nav-parent">
						<a>
							<i class="fa fa-database" aria-hidden="true"></i>
							<span>{{ ucfirst(__('pembetulan barang')) }}</span>
						</a>

						<ul class="nav nav-children">
							<li>
								<a href="{{ route('items.index') }}">
									{{ ucfirst(__('barang')) }}
								</a>
							</li>

							<li>
								<a href="{{ route('suppliers.index') }}">
									{{ ucfirst(__('supplier')) }}
								</a>
							</li>

							<li>
								<a href="{{ route('item-groups.index') }}">
									{{ ucfirst(__('kategori barang')) }}
								</a>
							</li>

							<li>
								<a href="{{ route('item-brands.index') }}">
									{{ ucfirst(__('merek barang')) }}
								</a>
							</li>

							<li>
								<a href="{{ route('inventory-units.index') }}">
									{{ ucfirst(__('satuan')) }}
								</a>
							</li>
						</ul>
                    </li>
                    <!-- end: product management menu -->
				</ul>
            </nav>
            <!-- end: navigation -->
		</div>
	</div>
	<!-- end: sidebar content -->
</aside>
<!-- end: sidebar -->
