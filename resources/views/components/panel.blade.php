{{-- components/panel.blade.php --}}

{!! isset($panel_width) ? '<div class="col-md-' . $panel_width . '">' : '' !!}
    <section class="panel {{ isset($context) ? 'panel-' . $context : '' }}">
        @if (isset($panel_title))
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                </div>

                <h3 class="panel-title">{{ ucwords(__($panel_title)) }}</h3>
            </header>
        @endif

        <div class="panel-body">
            {{ $slot }}
        </div>
    </section>
{!! isset($panel_width) ? '</div>' : '' !!}
