{{-- components/rightbar.blade.php --}}

<!-- start: rightbar -->
<aside class="sidebar-right" id="sidebar-right">
    <div class="nano">
        <div class="nano-content">
            <a href="#" class="mobile-close visible-xs">
                {{ ucwords(e(__('tutup'))) }} <i class="fa fa-chevron-right"></i>
            </a>

            <div class="sidebar-right-wrapper">

            </div>
        </div>
    </div>
</aside>
<!-- end: rightbar -->
