{{-- components/topbar.blade.php --}}

<!-- start: header -->
    <header class="header">
        <!-- start: logo -->
        <div class="logo-container">
            <a href="{{ route('home') }}" class="logo">
                <img src="{{ asset('assets/images/logo.png') }}" alt="{{ ucfirst(e(__('logo'))) . ' ' . config('app.name') }}" style="height: 35px;">
            </a>

            <div class="toggle-sidebar-left visible-xs" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
                <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
            </div>
        </div>
        <!-- end: logo -->

        <!-- start: header right side -->
        <div class="header-right">
            <!-- start: userbox -->
            <span class="separator"></span>

            <div class="userbox" id="userbox">
                <a href="#" data-toggle="dropdown">
                    <figure class="profile-picture">
                        <img src="{{ asset('assets/images/!logged-user.jpg') }}" class="img-circle" alt="{{ ucwords(e(__(auth()->user()->name))) }}" style="height: 35px;">
                    </figure>

                    <div class="profile-info" data-lock-name="{{ auth()->user()->name }}" data-lock-email="{{ auth()->user()->email }}">
                        <span class="name">{{ auth()->user()->name }}</span>
                    </div>

                    <i class="fa custom-caret"></i>
                </a>

                <div class="dropdown-menu">
                    <ul class="list-unstyled">
                        <li>
                            <a href="{{ route('home') }}">
                                <i class="fa fa-home" aria-hidden="true"></i>
                                <span>{{ ucfirst(e(__('dashboard'))) }}</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('users.index') }}">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                {{ ucfirst(__('manajemen pengguna')) }}
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('settings.index') }}">
                                <i class="fa fa-cogs" aria-hidden="true"></i>
                                {{ ucfirst(__('konfigurasi')) }}
                            </a>
                        </li>
                        <li class="divider"></li>

                        {{-- <li>
                            <a href="#" role="menuitem" tabindex="-1"><i class="fa fa-user"></i> Profilku</a>
                        </li> --}}

                        <li>
                            <a href="{{ route('logout') }}" role="menuitem" tabindex="-1" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-power-off"></i> {{ ucfirst(e(__('keluar'))) }}
                            </a>

                            <form method="post" action="{{ route('logout') }}" id="logout-form" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- end: userbox -->
        </div>
        <!-- end: header right side -->
    </header>
<!-- end: header -->
