{{-- settings/index.blade.php --}}

@extends('layouts.dashboard', ['page_title' => 'pengaturan aplikasi'])

@section('content')
    @component('components.panel', ['panel_title' => 'informasi perusahaan', 'panel_width' => '6'])
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4 text-right">{{ ucfirst(__('nama perusahaan')) }}</div>
                <div class="col-md-8">{{ $settings->get('company_name') }}</div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4 text-right">{{ ucfirst(__('alamat perusahaan')) }}</div>
                <div class="col-md-8">{{ $settings->get('company_address') }}</div>
            </div>
            <div class="col-md-12 text-right">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-edit-company" data-backdrop="static" data-keyboard="false">
                    <span class="fa fa-edit"></span>
                    {{ ucwords(__('perbarui info perusahaan')) }}
                </button>
            </div>
        </div>
    @endcomponent

    @component('components.panel', ['panel_title' => 'informasi penjualan', 'panel_width' => '6'])
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4 text-right">{{ ucfirst(__('kode faktur penjualan')) }}</div>
                <div class="col-md-8">{{ $settings->get('invoice_code') }}</div>
            </div>

            <div class="col-md-12">
                <div class="col-md-4 text-right">{{ ucfirst(__('nomor rekening bank')) }}</div>
                <div class="col-md-8">{{  $settings->get('bank_account') }}</div>
            </div>

            <div class="col-md-12 text-right">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-edit-sales-setting" data-backdrop="static" data-keyboard="false">
                    <span class="fa fa-edit"></span>
                    {{ ucwords(__('perbarui info penjualan')) }}
                </button>
            </div>
        </div>
    @endcomponent

    @component('components.panel', ['panel_title' => 'pengaturan data awal', 'panel_width' => '6'])
        <div class="row">
            <div class="col-md-4">
                <button class="btn btn-block" role="button" data-toggle="modal" data-target="#prerequisite-import-modal" style="background: transparent;">
                    @component('components.dashboard-panel', [
                        'panel_icon'      => 'anchor',
                        'icon_background' => 'bg-info',
                    ])
                        <p class="text-center text-dark">1. {{ ucfirst(__('masukan data pendukung')) }}</p>
                    @endcomponent
                </button>
            </div>

            <div class="col-md-4">
                <button class="btn btn-block" role="button" data-toggle="modal" data-target="#purchase-import-modal" style="background: transparent;">
                    @component('components.dashboard-panel', [
                        'panel_icon'      => 'truck',
                        'icon_background' => 'bg-info',
                    ])
                        <p class="text-center text-dark">2. {{ ucfirst(__('masukan data kedatangan')) }}</p>
                    @endcomponent
                </button>
            </div>

            <div class="col-md-4">
                <button class="btn btn-block" role="button" data-toggle="modal" data-target="#item-import-modal" style="background: transparent;">
                    @component('components.dashboard-panel', [
                        'panel_icon'      => 'cube',
                        'icon_background' => 'bg-info',
                    ])
                        <p class="text-center text-dark">3. {{ ucfirst(__('masukan data barang')) }}</p>
                    @endcomponent
                </button>
            </div>

            <div class="col-md-4">
                <button class="btn btn-block" role="button" data-toggle="modal" data-target="#price-import-modal" style="background: transparent;">
                    @component('components.dashboard-panel', [
                        'panel_icon'      => 'money',
                        'icon_background' => 'bg-info',
                    ])
                        <p class="text-center text-dark">4. {{ ucfirst(__('masukan data harga barang')) }}</p>
                    @endcomponent
                </button>
            </div>

            <div class="col-md-4">
                <button class="btn btn-block" role="button" data-toggle="modal" data-target="#stock-import-modal" style="background: transparent;">
                    @component('components.dashboard-panel', [
                        'panel_icon'      => 'cubes',
                        'icon_background' => 'bg-info',
                    ])
                        <p class="text-center text-dark">5. {{ ucfirst(__('masukan data stock')) }}</p>
                    @endcomponent
                </button>
            </div>

            <div class="col-md-4">
                <button class="btn btn-block" role="button" data-toggle="modal" data-target="#product-list-import-modal" style="background: transparent;">
                    @component('components.dashboard-panel', [
                        'panel_icon'      => 'info',
                        'icon_background' => 'bg-info',
                    ])
                        <p class="text-center text-dark">6. {{ ucfirst(__('masukan daftar pembalian')) }}</p>
                    @endcomponent
                </button>
            </div>
            {{-- <!-- start: product import widget -->
            <div class="col-md-4">
                <button class="btn btn-block" role="button" data-toggle="modal" data-target="#product-import-modal" style="background: transparent;">
                    @component('components.dashboard-panel', [
                        'panel_icon'      => 'cube',
                        'icon_background' => 'bg-info',
                    ])
                        <p class="text-center text-dark">{{ ucfirst(__('impor data produk')) }}</p>
                    @endcomponent
                </button>
            </div>
            <!-- end: product import widget -->

            <!-- start: product import widget -->
            <div class="col-md-4">
                <button class="btn btn-block" role="button" data-toggle="modal" data-target="#stock-import-modal" style="background: transparent;">
                    @component('components.dashboard-panel', [
                        'panel_icon'      => 'cubes',
                        'icon_background' => 'bg-info',
                    ])
                        <p class="text-center text-dark">{{ ucfirst(__('impor data stok')) }}</p>
                    @endcomponent
                </button>
            </div>
            <!-- end: product import widget --> --}}

            {{-- <!-- start: widget category -->
            <div class="col-md-4">
                <button class="btn btn-block" role="button" data-toggle="modal" data-target="#item-groups-import-modal" style="background: transparent;">
                    @component('components.dashboard-panel', [
                        'panel_icon'      => 'folder',
                        'icon_background' => 'bg-info',
                    ])
                        <p class="text-center text-dark">{{ ucfirst(__('impor kategori produk')) }}</p>
                    @endcomponent
                </button>
            </div>
            <!-- end: widget category -->

            <!-- start: widget brand -->
            <div class="col-md-4">
                <button class="btn btn-block" role="button" data-toggle="modal" data-target="#item-brands-import-modal" style="background: transparent;">
                    @component('components.dashboard-panel', [
                        'panel_icon'      => 'asterisk',
                        'icon_background' => 'bg-info',
                    ])
                        <p class="text-center text-dark">{{ ucfirst(__('impor merek')) }}</p>
                    @endcomponent
                </button>
            </div>
            <!-- end: widget brand -->

            <!-- start: widget company -->
            <div class="col-md-4">
                <button class="btn btn-block" role="button" data-toggle="modal" data-target="#companies-import-modal" style="background: transparent;">
                    @component('components.dashboard-panel', [
                        'panel_icon'      => 'building',
                        'icon_background' => 'bg-info',
                    ])
                        <p class="text-center text-dark">{{ ucfirst(__('impor kontak perusahaan')) }}</p>
                    @endcomponent
                </button>
            </div>
            <!-- end: widget brand -->

            <!-- start: widget company -->
            <div class="col-md-4">
                <button class="btn btn-block" role="button" data-toggle="modal" data-target="#units-import-modal" style="background: transparent;">
                    @component('components.dashboard-panel', [
                        'panel_icon'      => 'sliders',
                        'icon_background' => 'bg-info',
                    ])
                        <p class="text-center text-dark">{{ ucfirst(__('impor satuan produk')) }}</p>
                    @endcomponent
                </button>
            </div>
            <!-- end: widget brand -->

            <!-- start: widget company -->
            <div class="col-md-4">
                <button class="btn btn-block" role="button" data-toggle="modal" data-target="#item-import-modal" style="background: transparent;">
                    @component('components.dashboard-panel', [
                        'panel_icon'      => 'cube',
                        'icon_background' => 'bg-info',
                    ])
                        <p class="text-center text-dark">{{ ucfirst(__('impor produk')) }}</p>
                    @endcomponent
                </button>
            </div>
            <!-- end: widget brand -->

            <!-- start: widget company -->
            <div class="col-md-4">
                <button class="btn btn-block" role="button" data-toggle="modal" data-target="#stock-import-modal" style="background: transparent;">
                    @component('components.dashboard-panel', [
                        'panel_icon'      => 'cubes',
                        'icon_background' => 'bg-info',
                    ])
                        <p class="text-center text-dark">{{ ucfirst(__('impor stok produk')) }}</p>
                    @endcomponent
                </button>
            </div>
            <!-- end: widget brand -->

            <!-- start: widget company -->
            <div class="col-md-4">
                <button class="btn btn-block" role="button" data-toggle="modal" data-target="#price-import-modal" style="background: transparent;">
                    @component('components.dashboard-panel', [
                        'panel_icon'      => 'money',
                        'icon_background' => 'bg-info',
                    ])
                        <p class="text-center text-dark">{{ ucfirst(__('impor harga produk')) }}</p>
                    @endcomponent
                </button>
            </div>
            <!-- end: widget brand --> --}}
        </div>
    @endcomponent

    @include('settings.modal-company-name')
    @include('settings.modal-sales-setting')
    @include('settings.prerequisite-import')
    @include('settings.purchase-import')
    @include('settings.item-import')
    @include('settings.price-import')
    @include('settings.stock-import')
    @include('settings.product-list-import')
    {{-- @include('settings.stock-import') --}}
@endsection
