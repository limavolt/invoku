{{-- settings/modal-company-name.blade.php --}}

@component('components.modal', [
    'modal_id'    => 'modal-edit-company',
    'modal_title' => 'ubah nama perusahaan'
])
    @slot('modal_body')
        <form action="{{ route('settings.store') }}" method="post" id="form-edit-company">
            @csrf

            <div class="form-group">
                <label for="company_name" class="control-label col-md-3 text-right">
                    {{ ucfirst(__('nama perusahaan')) }}
                </label>

                <div class="col-md-9">
                    <input type="text" class="form-control" name="company_name" value="{{ $settings->get('company_name') }}">
                </div>
            </div>

            <div class="form-group">
                <label for="company_address" class="control-label col-md-3 text-right">
                    {{ ucfirst(__('alamat perusahaan')) }}
                </label>

                <div class="col-md-9">
                    <textarea name="company_address" class="form-control" id="company_address" rows="5">{{ $settings->get('company_address') }}</textarea>
                </div>
            </div>
		</form>
    @endslot

    @slot('modal_button')
        <button type="button" class="btn btn-default" data-dismiss="modal" id="btn-cancel-edit-company"><span class="fa fa-arrow-circle-left"></span> {{ ucwords(__('batal')) }}</button>
        <button type="submit" class="btn btn-primary" id="btn-edit-company"><span class="fa fa-save"></span> {{ ucwords(__('simpan')) }}</button>
    @endslot
@endcomponent

@push('appscripts')
    <script type="text/javascript">
        $('#modal-edit-company-name').ready(function () {
            var modal = $('#modal-edit-company');
            var form  = $('#form-edit-company');

            modal.on('shown.bs.modal', function () {
                cleanModal('#form-edit-company');
            });

            $('#btn-edit-company').click(function () {
                $.ajax({
                    url:    form.attr('action'),
                    method: form.attr('method'),
                    data:   form.serialize(),

                    success: function (response) {
                        window.location.reload(true);

                        generateSuccess(modal, 'Data berhasil diperbarui.');
                    },

                    error: function (response) {
                        generateError('#form-edit-company', 'Data yang dikirimkan salah.');
                    }
                });
            });

            $('#btn-cancel-edit-company').click(function () {
                form.trigger('reset');
            })
        });
    </script>
@endpush
