{{-- settings/product-import.blade.php --}}

@component('components.modal', [
    'modal_id'    => 'stock-import-modal',
    'modal_title' => 'impor data stok produk',
])
    @slot('modal_body')
        <form id="stock-import-form" action="{{ route('database.import', 'stock-data') }}" method="post" enctype="multipart/form-data">
            @csrf

            <div class="form-group" id="div-file">
                <label for="file" class="col-sm-4 control-label text-right">
                    {{ ucfirst(__('file data awal')) }}
                </label>

                <div class="col-sm-8">
                    <input type="file" name="file" id="file">
                </div>
            </div>
    @endslot

    @slot('modal_button')
            <button type="button" class="btn btn-default" data-dismiss="modal">
                <span class="fa fa-arrow-circle-left"></span>
                {{ ucwords(__('batal')) }}
            </button>

            <button type="submit" class="btn btn-primary" id="stock-import-button">
                <span class="fa fa-database"></span>
                {{ ucwords(__('impor')) }}
            </button>
        </form>
    @endslot
@endcomponent
