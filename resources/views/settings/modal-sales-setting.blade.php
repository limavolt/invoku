{{-- settings/modal-sales-setting.blade.php --}}

@component('components.modal', [
    'modal_id'    => 'modal-edit-sales-setting',
    'modal_title' => 'perbarui informasi penjualan'
])
    @slot('modal_body')
        <form action="{{ route('settings.store') }}" method="post" id="form-edit-sales-setting">
            @csrf

            <div class="form-group">
                <label for="invoice_code" class="control-label col-md-3 text-right">
                    {{ ucfirst(__('kode faktur penjualan')) }}
                </label>

                <div class="col-md-9">
                    <input type="text" class="form-control" name="invoice_code" value="{{ $settings->get('invoice_code') }}">
                </div>
            </div>

            <div class="form-group">
                <label for="bank_account" class="control-label col-md-3 text-right">
                    {{ ucfirst(__('nomor rekening bank')) }}
                </label>

                <div class="col-md-9">
                    <input type="text" class="form-control" name="bank_account" value="{{ $settings->get('bank_account') }}">
                </div>
            </div>
		</form>
    @endslot

    @slot('modal_button')
        <button type="button" class="btn btn-default" data-dismiss="modal" id="btn-cancel-edit-sales-setting"><span class="fa fa-arrow-circle-left"></span> {{ ucwords(__('batal')) }}</button>
        <button type="submit" class="btn btn-primary" id="btn-edit-sales-setting"><span class="fa fa-save"></span> {{ ucwords(__('simpan')) }}</button>
    @endslot
@endcomponent

@push('appscripts')
    <script type="text/javascript">
        $('#modal-edit-sales-setting').ready(function () {
            var modal         = $('#modal-edit-sales-setting')
            var form          = $('#form-edit-sales-setting')
            var save_button   = $('#btn-edit-sales-setting')
            var cancel_button = $('#btn-cancel-edit-sales-setting')

            modal.on('shown.bs.modal', function () {
                cleanModal('#form-edit-sales-setting')
            })

            save_button.click(function () {
                $.ajax({
                    url:    form.attr('action'),
                    method: form.attr('method'),
                    data:   form.serialize(),

                    success: function (response) {
                        window.location.reload(true)

                        generateSuccess(modal, 'Data berhasil diperbarui.')
                    },

                    error: function () {
                        generateError('#form-edit-sales-setting', 'Data yang dikirimkan  salah.')
                    }
                })
            })

            cancel_button.click(function () {
                form.trigger('reset')
            })
        })
    </script>
@endpush
