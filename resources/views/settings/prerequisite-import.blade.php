{{-- settings/prerequisite-import.blade.php --}}

@component('components.modal', [
    'modal_id'    => 'prerequisite-import-modal',
    'modal_title' => 'import produk',
])
    @slot('modal_body')
        <form id="prerequisite-import-form" action="{{ route('database.import', 'prerequisite-data') }}" method="post" enctype="multipart/form-data">
            @csrf

            {{-- <div class="form-group" id="div-item_group_file">
                <label for="item_group_file" class="col-sm-4 control-label text-right">
                    {{ ucfirst(__('file kategori produk')) }}
                </label>

                <div class="col-sm-8">
                    <input type="file" name="item_group_file" id="item_group_file">
                </div>
            </div>

            <div class="form-group" id="div-item_brand_file">
                <label for="item_brand_file" class="col-sm-4 control-label text-right">
                    {{ ucfirst(__('file merek produk')) }}
                </label>

                <div class="col-sm-8">
                    <input type="file" name="item_brand_file" id="item_brand_file">
                </div>
            </div>

            <div class="form-group" id="div-unit_file">
                <label for="unit_file" class="col-sm-4 control-label text-right">
                    {{ ucfirst(__('file satuan produk')) }}
                </label>

                <div class="col-sm-8">
                    <input type="file" name="unit_file" id="unit_file">
                </div>
            </div>

            <div class="form-group" id="div-company_file">
                <label for="company_file" class="col-sm-4 control-label text-right">
                    {{ ucfirst(__('file daftar supplier dan pelanggan')) }}
                </label>

                <div class="col-sm-8">
                    <input type="file" name="company_file" id="company_file">
                </div>
            </div> --}}

            <div class="form-group" id="div-file">
                <label for="file" class="col-sm-4 control-label text-right">
                    {{ ucfirst(__('file data awal')) }}
                </label>

                <div class="col-sm-8">
                    <input type="file" name="file" id="file">
                </div>
            </div>
    @endslot

    @slot('modal_button')
            <button type="button" class="btn btn-default" data-dismiss="modal">
                <span class="fa fa-arrow-circle-left"></span>
                {{ ucwords(__('batal')) }}
            </button>

            <button type="submit" class="btn btn-primary" id="prerequisite-import-button">
                <span class="fa fa-database"></span>
                {{ ucwords(__('simpan')) }}
            </button>
        </form>
    @endslot
@endcomponent
