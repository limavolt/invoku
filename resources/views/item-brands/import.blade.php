{{-- item-brands/import.blade.php --}}

@component('components.modal', [
    'modal_id'    => 'item-brands-import-modal',
    'modal_title' => 'impor daftar merek'
])
    @slot('modal_body')
        <form id="item-brands-import-form" action="{{ route('item-brands.import') }}" method="post" enctype="multipart/form-data">
            @csrf

            <div class="form-group" id="div-file">
                <label for="file" class="col-sm-3 control-label text-right">
                    {{ ucfirst(__('file excel')) }}
                </label>

                <div class="col-sm-9">
                    <input type="file" name="file" id="file">
                </div>
            </div>
    @endslot

    @slot('modal_button')
            <button type="button" class="btn btn-default" data-dismiss="modal">
                <span class="fa fa-arrow-circle-left"></span>
                {{ ucwords(__('batal')) }}
            </button>

            <button type="submit" class="btn btn-primary" id="btn-import-item-brands">
                <span class="fa fa-database"></span>
                {{ ucwords(__('import')) }}
            </button>
        </form>
    @endslot
@endcomponent

@push('appscripts')
    <script>
        $('#item-brands-import-modal').ready(function () {
            /* Upload a list of brand items template from an excel file to database. */
            $('#item-brand-import').on('click', function () {
                $('#item-brands-import-modal').modal('show');
            });
        })
    </script>
@endpush
