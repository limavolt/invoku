{{-- item-brands/_form.blade.php --}}

<!-- start: item brand name input field -->
<div id="div-name" class="form-group mt-lg">
	<label class="control-label col-sm-3" for="name">{{ ucfirst(e(__('nama merek'))) }}</label>

	<div class="col-sm-9">
        <input id="name" class="form-control" type="text" name="name" required>

		<span class="help-block text-muted">Wajib diisi</span>
		<span class="help-block text-error" id="error-name"></span>
	</div>
</div>
<!-- end: item brand name input field -->

<!-- start: item brand description input field -->
<div class="form-group mt-lg" id="div-description">
	<label class="control-label col-sm-3" for="description">{{ ucfirst(__('deskripsi')) }}</label>

	<div class="col-sm-9">
        <textarea name="description" class="form-control" id="description"></textarea>

		<span class="help-block text-error" id="error-description"></span>
	</div>
</div>
<!-- end: item brand description input field -->
