<table>
    <thead>
        <tr>
            <td>NAMA BRAND</td>
            <td>DESKRIPSI</td>
        </tr>
    </thead>

    <tbody>
        @foreach ($item_brands as $brand)
            <tr>
                <td>{{ $brand->name }}</td>
                <td>{{ $brand->description }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
