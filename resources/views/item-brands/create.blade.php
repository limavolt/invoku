{{-- item-brands/create.blade.php --}}

<!-- start: modal window for item brand addition -->
@component('components.modal', [
	'modal_id'    => 'modal-add-item-brand',
	'modal_title' => 'Tambah Merek',
])
    @slot('modal_body')
        <!-- start: item brand addition form -->
		<form id="form-add-item-brand" method="post" action="{{ route('item-brands.store') }}">
			@csrf
			@include('item-brands._form')
        </form>
        <!-- end: item brand addition form -->
	@endslot

	@slot('modal_button')
		<button class="btn btn-default" type="button" data-dismiss="modal">
            <span class="fa fa-arrow-circle-left"></span>
            {{ ucwords(__('batal')) }}
        </button>

		<button id="btn-add-item-brand" class="btn btn-primary" type="submit">
            <span class="fa fa-save"></span>
            {{ ucwords(__('simpan')) }}
        </button>
	@endslot
@endcomponent
<!-- end: modal window for brand addition -->
