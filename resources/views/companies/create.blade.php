{{-- companies/create.blade.php --}}

@component('components.modal',[
	'modal_id'    => 'modal-add-company',
	'modal_title' => 'tambah kontak'
])
	@slot('modal_body')
		<form method="post" action="{{ route('companies.store') }}" id="form-add-company">
			@csrf
			@include('companies._form')
		</form>
	@endslot

	@slot('modal_button')
		<button type="button" class="btn btn-default" data-dismiss="modal">{{ ucwords(__('batal')) }}</button>
		<button type="button" class="btn btn-primary" id="btn-add-company">{{ ucwords(__('simpan')) }}</button>
	@endslot
@endcomponent

@push('appscripts')
	<script type="text/javascript">
		$('#modal-add-company').ready(function () {
			/*
			 * Memodifikasi tampilan formulir penambahan resource
			 * ketika modal penambahan resource dimunculkan.
			 */
			 $('#modal-add-company').on('shown.bs.modal', function() {
				// Menghapus isian formulir.
				cleanModal('#form-add-company', true);
			});

			/* Penyimpanan data modal penambahan data. */
			$('#btn-add-company').click(function () {
				var form = $('#form-add-company');

				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function (response) {
                        $('#modal-add-company').modal('hide');

						table.ajax.reload();

						new PNotify({
							title: 'Sukses!',
							text: 'Data perusahaan berhasil ditambahkan.',
							type: 'success',
						});
					},
					error: function(response) {
						if (response.status == 422) {
							var errors = response.responseJSON;

							new PNotify({
								title: 'Peringatan!',
								text: 'Terdapat kesalahan pada data yang dimasukkan',
								type: 'warning'
							});

                            cleanModal('#form-add-company', false);

							$.each(errors.errors, function(col_val, msg) {
								$('#div-' + col_val).addClass('has-error');
								$('#label-' + col_val).html(msg[0]);
							});
						}
						else {
							systemError();
						}
					}
				});
			});
		});
	</script>
@endpush
