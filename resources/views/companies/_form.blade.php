{{-- views/companies/_form.blade.php --}}

<div class="form-group mt-lg" id="div-name">
	<label class="col-sm-3 control-label text-right">Nama perusahaan</label>

	<div class="col-sm-9">
		<input type="text" id="name" name="name" class="form-control" required>
		<span class="help-block text-muted">Wajib diisi</span>
		<span class="help-block text-error" id="error-name"></span>
	</div>
</div>

<div class="form-group mt-lg" id="div-pic">
	<label class="col-sm-3 control-label text-right">{{ ucfirst(__('nama kontak')) }}</label>

	<div class="col-sm-9">
        <input type="text" id="pic" name="pic" class="form-control">

		<span class="help-block text-error" id="error-pic"></span>
	</div>
</div>

<div class="form-group mt-lg" id="div-type">
	<label class="col-sm-3 control-label text-right">Jenis</label>

	<div class="col-sm-9">
		<div class="checkbox-custom checkbox-default" id="supplier-checkbox">
			<input type="checkbox" name="type[]" id="type-supplier" value="supplier">
			<label for="type[]">{{ ucfirst(__('supplier')) }}</label>
		</div>

		<div class="checkbox-custom checkbox-default" id="customer-checkbox">
			<input type="checkbox" name="type[]" id="type-customer" value="customer">
			<label for="type[]">{{ ucfirst(__('pelanggan')) }}</label>
		</div>

		<span class="help-block text-muted">Wajib dipilih. Bisa pilih salah satu atau keduanya.</span>
		<span class="help-block text-error" id="error-type"></span>
	</div>
</div>

<div class="form-group mt-lg" id="div-address">
    <label class="col-sm-3 control-label text-right">{{ ucfirst(__('alamat')) }}</label>

    <div class="col-sm-9">
        <textarea name="address" id="address" class="form-control" rows="4"></textarea>

        <span class="help-block text-error" id="error-address"></span>
    </div>
</div>
