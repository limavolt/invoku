{{-- companies/customer/create.blade.php --}}

@component('components.modal', [
    'modal_id'    => 'add-customer-modal',
    'modal_title' => 'tambah pelanggan'
])
    @slot('modal_body')
        <form action="{{ route('customers.store') }}" method="post" id="add-customer-form">
            @csrf
            @include('companies.customer.form')
        </form>
    @endslot

    @slot('modal_button')
    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-arrow-circle-left"></span> {{ ucwords(__('batal')) }}</button>
    <button type="submit" class="btn btn-primary" id="add-customer-button"><span class="fa fa-save"></span> {{ ucwords(__('simpan')) }}</button>
    @endslot
@endcomponent

@push('appscripts')
    <script type="text/javascript">
        $('#add-customer-modal').ready(function () {
            var modal = $('#add-customer-modal');
            var form  = $('#add-customer-form');
            /*
             * Memodifikasi tampilan formulir penambahan resource
			 * ketika modal penambahan resource dimunculkan.
             */
            modal.on('shown.bs.modal', function () {
                cleanModal('#add-customer-form', true);

                $('#add-customer-form #supplier-checkbox').remove();

				// Menandai kotak centang untuk supplier dan menjadikannya tidak dapat diubah lagi.
				$('#add-customer-form #type-consumer:checkbox').prop('checked', true);
				$('#add-customer-form #type-consumer').prop('disabled', true);

				// Menyembunyikan bagian pemilihan jenis perusahaan.
				$('#add-customer-form #div-type').hide();
            });

			/*
			 * Mengirim data pada form penambahan resource saat tombol submisi ditekan.
			 */
			$('#add-customer-button').click(function () {
				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function(response) {
						modal.modal('hide');

						table.ajax.reload();

						new PNotify({
							type:  'success',
							title: 'Berhasil!',
							text:  response.data.name + ' berhasil ditambahkan sebagai customer.',
						});
					},

					error: function(response) {
						if (response.status == 422) {
							var errors = response.responseJSON;

							new PNotify({
								type:  'warning',
								title: 'Peringatan!',
								text:  'Terdapat kesalahan pada data yang dimasukkan',
							});

							cleanModal('#add-customer-form', false);

							$.each(errors.errors, function(col_val, msg) {
								$('#div-' + col_val).addClass('has-error');
								$('#error-' + col_val).html(msg[0]);
							});

							console.log(errors.errors);
						}
						else {
							systemError();
						}
					}
				});
			});
        });
    </script>
@endpush
