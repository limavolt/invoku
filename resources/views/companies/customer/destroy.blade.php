{{-- companies/customer/destroy.blade.php --}}

@component('components.modal', [
	'modal_id'    => 'destroy-customer-modal',
	'modal_title' => 'penghapusan customer',
])
	@slot('modal_body')
		<form method="post" action="{{ route('customers.destroy', 0) }}" id="destroy-customer-form">
			@csrf
			@method('DELETE')

			<p>Apakah kamu yakin ingin menghapus <span id="customer-name"></span>?</p>
	@endslot

	@slot('modal_button')
		<button type="button" class="btn btn-primary" data-dismiss="modal"><span class="fa fa-arrow-circle-left"></span> {{ ucwords(__('batal')) }}</button>
		<button type="button" class="btn btn-default" id="destroy-customer-button"><span class="fa fa-trash-o"></span> {{ ucwords(__('hapus data')) }}</button>
	</form>
	@endslot
@endcomponent

@push('appscripts')
	<script type="text/javascript">
		$('#destroy-customer-modal').ready(function () {
			/*
			 * Menampilkan jendela modal ketika tombol hapus resource ditekan.
			 */
			 $('#customers-table tbody').on('click', 'button[name="btn-destroy-customer"]', function () {
                // Ambil data supplier dari baris tombol yang ditekan.
				var data = table.row($(this).closest('tr')).data();

                $('#destroy-customer-form #customer-name').text(data.name + ' - ' + data.address);

				// Menetapkan URL untuk pemrosesan form.
				$('#destroy-customer-form').attr('action', APP_URL + '/customers/' + $(this).data('id'));

				// Menampilkan jendela modal.
				$('#destroy-customer-modal').modal('show');
			});

			/*
			 * Mengirim data pada form penghapusan resource saat tombol submisi ditekan.
			 */
			$('#destroy-customer-button').click(function () {
				var form = $('#destroy-customer-form');

				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function(response) {
						$('#destroy-customer-modal').modal('hide');

						if (response.status == 'success') {
							new PNotify({
								type:  'success',
								title: 'Berhasil!',
								text:  response.message,
							});

							table.ajax.reload();
						}
						else {
							new PNotify({
								type:  'warning',
								title: 'Peringatan!',
								text:  response.message,
							});

							console.log(response);
						}
					},

					error: function(response) {
						console.log(response);

						systemError();
					}
				});
			});
		});
	</script>
@endpush
