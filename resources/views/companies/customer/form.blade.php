{{-- companies/customer/form.blade.php --}}

<div class="form-group mt-lg" id="div-name">
    <label class="col-sm-3 control-label text-right">{{ ucfirst(__('nama pelanggan')) }}</label>

    <div class="col-sm-9">
        <input type="text" id="name" name="name" class="form-control" required>
        <span class="help-block text-muted">Wajib diisi</span>
        <span class="help-block text-error" id="error-name"></span>
    </div>
</div>

<div class="form-group mt-lg" id="div-pic">
    <label class="col-sm-3 control-label text-right">{{ ucfirst(__('nama kontak')) }}</label>

    <div class="col-sm-9">
        <input type="text" id="pic" name="pic" class="form-control" required>

        <span class="help-block text-error" id="error-pic"></span>
    </div>
</div>

<div class="form-group mt-lg" id="div-address">
    <label class="col-sm-3 control-label text-right">{{ ucfirst(__('alamat')) }}</label>

    <div class="col-sm-9">
        <textarea name="address" id="address" class="form-control" rows="4"></textarea>

        <span class="help-block text-error" id="error-address"></span>
    </div>
</div>
