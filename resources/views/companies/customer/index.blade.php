{{-- companies/customer/index.blade.php --}}

@extends('layouts.dashboard', ['page_title' => 'manajemen kontak'])

@section('content')
    @component('components.panel', ['panel_title' => 'daftar pelanggan'])
        <div class="mb-sm hidden-xs">
            <!-- start: toolbar -->
            <div class="row mb-md">
                <!-- start: left toolbar -->
                <div class="col-md-6">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#add-customer-modal" data-backdrop="static" data-keyboard="false">
                        <span class="fa fa-plus"></span>
                        {{ ucwords(__('tambah pelanggan')) }}
                    </button>
                </div>
                <!-- end: left toolbar -->

                <!-- start: right toolbar -->
                <div class="col-md-6 text-right">
                    <div class="btn-group">
                        <a href="{{ route('sales.create') }}" class="btn btn-default">
                            <span class="fa fa-print"></span>
                            {{ ucwords(__('gawe faktur')) }}
                        </a>

                        <a href="{{ route('incoming-goods.create') }}" class="btn btn-default">
                            <span class="fa fa-truck"></span>
                            {{ ucwords(__('barang datang')) }}
                        </a>
                    </div>
                </div>
                <!-- end: right toolbar -->
            </div>
            <!-- end: toolbar -->

        @component('components.datatable-ajax', [
            'table_id'      => 'customers',
            'condition'     => true,
            'table_headers' => ['nama pelanggan', 'alamat', 'kontak'],
            'data'          => [
                ['name' => 'name', 'data' => 'name'],
                ['name' => 'address', 'data' => 'address'],
                ['name' => 'pic', 'data' => 'pic'],
            ],
            'renders'       => [
                ['text_align' => 'text-right', 'column_target' => '4'],
            ],
            'default_order' => 1
        ])
            @slot('data_send_ajax')
            @endslot
        @endcomponent
    @endcomponent

    @include('companies.customer.create')
    @include('companies.customer.edit')
    @include('companies.customer.destroy')
@endsection

@push('vendorstyles')
	<link href="{{ asset('assets/vendor/pnotify/pnotify.custom.css') }}" rel="stylesheet" style="text/css">
	<link href="{{ asset('assets/vendor/select2-4.0.8/css/select2.min.css') }}" rel="stylesheet" style="text/css">
	<link href="{{ asset('assets/vendor/select2-bootstrap/select2-bootstrap.min.css') }}" rel="stylesheet" style="text/css">
@endpush

@push('vendorscripts')
	<script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendor/select2-4.0.8/js/select2.min.js') }}" type="text/javascript"></script>
@endpush
