{{-- companies/customer/edit.blade.php --}}

@component('components.modal', [
    'modal_id'    => 'edit-customer-modal',
    'modal_title' => 'perbarui informasi pelanggan'
])
    @slot('modal_body')
        <form action="{{ route('customers.update', 0) }}" method="put" id="edit-customer-form">
            @csrf
            @include('companies.customer.form')
    @endslot

    @slot('modal_button')
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-arrow-circle-left"></span> {{ ucwords(__('batal')) }}</button>
            <button type="button" class="btn btn-primary" id="edit-customer-button"><span class="fa fa-edit"></span> {{ ucwords(__('perbarui')) }}</button>
        </form>
    @endslot
@endcomponent

@push('appscripts')
    <script type="text/javascript">
        $('#edit-customer-modal').ready(function () {
            /*
             * Menampilkan jendela modal ketika tombol edit resource ditekan.
             */
            $('#customers-table tbody').on('click', 'button[name="btn-edit-customer"]', function () {
                // Ambil data supplier dari baris tombol yang ditekan.
                var data = table.row($(this).closest('tr')).data();

                // Mengisi nilai masing-masing bidang masukan formulir sesusai data yang diambil.
                $.each($('input, select, textarea', '#edit-customer-form'), function () {
                    if ($(this).attr('id')) {
                        var id_element = $(this).attr('id');

                        if (data[id_element]) {
                            $('#' + id_element, '#edit-customer-form').val(data[id_element]).trigger('change');
                        }
                        else {
                            $('#' + id_element, '#edit-customer-form').val('').trigger('change');
                        }
                    }
                });

				// Membatasi data yang boleh diubah pengguna dengan menghilangkan bidang yang dilindungi.
				$('#edit-customer-modal #div-type').remove();

				// Menetapkan URL untuk pemrosesan form.
				$('#edit-customer-form').attr('action', APP_URL + '/customers/'+ $(this).data('id'));

				// Menampilkan jendela modal kepada pengguna.
				$('#edit-customer-modal').modal('show');
            });

			/*
			 * Memodifikasi tampilan formulir pengubahan resource
			 * ketika modal pengubahan resource dimunculkan.
			 */
			$('#edit-customer-modal').on('shown.bs.modal', function() {
				// Menghapus isian formulir.
				cleanModal('#edit-customer-form', false);
            });

			/*
			 * Mengirim data pada form pengubahan resource saat tombol submisi ditekan.
			 */
			$('#edit-customer-button').click(function () {
				var form = $('#edit-customer-form');

				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function(response) {
						$('#edit-customer-modal').modal('hide');

						table.ajax.reload();

						new PNotify({
							type:  'success',
							title: 'Berhasil!',
							text:  'Informasi customer ' + response.data.name + ' berhasil diperbarui.',
						});
					},

					error: function(response) {
						if (response.status == 422) {
							var errors = response.responseJSON;

							new PNotify({
								type:  'warning',
								title: 'Peringatan!',
								text:  'Terdapat kesalahan pada data yang dimasukkan',
							});

							cleanModal('#edit-customer-form', false);

							$.each(errors.errors, function(col_val, msg) {
								$('#div-' + col_val, '#edit-customer-form').addClass('has-error');
								$('#error- ' + col_val, '#edit-customer-form').html(msg[0]);
							});
						}
						else {
							systemError();
						}
					}
				});
			});
        });
    </script>
@endpush
