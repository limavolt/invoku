{{-- companies/supplier/edit.blade.php --}}

@component('components.modal', [
	'modal_id'    => 'supplier-edit-modal',
	'modal_title' => 'perbarui informasi supplier'
])
	@slot('modal_body')
		<form method="put" action="{{ route('suppliers.update', 0) }}" id="supplier-edit-form">
			@csrf
			@include('companies.supplier.form')
	@endslot

	@slot('modal_button')
            <button type="button" class="btn btn-default" data-dismiss="modal">
                <span class="fa fa-arrow-circle-left"></span>
                {{ ucwords(__('batal')) }}
            </button>

			<button type="button" class="btn btn-primary" id="btn-edit-supplier">
                <span class="fa fa-edit"></span>
                {{ ucwords(__('perbarui')) }}
            </button>
		</form>
	@endslot
@endcomponent

@push('appscripts')
	<script type="text/javascript">
		$('#supplier-edit-modal').ready(function () {
            var modal = $('#supplier-edit-modal');
            var form  = $('#supplier-edit-form');
			/*
			 * Menampilkan jendela modal ketika tombol edit resource ditekan.
			 */
			 $('#suppliers-table tbody').on('click', 'button[name="btn-edit-supplier"]', function() {
				// Ambil data supplier dari baris tombol yang ditekan.
				var data = table.row($(this).closest('tr')).data();

				// Mengisi nilai masing-masing bidang masukan formulir sesusai data yang diambil.
				$.each($('input, select, textarea', '#supplier-edit-form'), function() {
					if ($(this).attr('id')) {
						var id_element = $(this).attr('id');

						if (data[id_element]) {
							$('#' + id_element, '#supplier-edit-form').val(data[id_element]).trigger('change');
						}
						else {
							$('#' + id_element, '#supplier-edit-form').val('').trigger('change');
						}
					}
				});

				// Menetapkan URL untuk pemrosesan form.
				form.attr('action', APP_URL + '/suppliers/'+ $(this).data('id'));

				// Menampilkan jendela modal kepada pengguna.
				modal.modal('show');
			});

			/*
			 * Memodifikasi tampilan formulir pengubahan resource
			 * ketika modal pengubahan resource dimunculkan.
			 */
			modal.on('shown.bs.modal', function() {
				// Menghapus isian formulir.
				cleanModal('#supplier-edit-form', false);
			});

			/*
			 * Mengirim data pada form pengubahan resource saat tombol submisi ditekan.
			 */
			$('#btn-edit-supplier').click(function () {
				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function(response) {
						$('#supplier-edit-modal').modal('hide');

						table.ajax.reload();

						new PNotify({
							type:  'success',
							title: 'Berhasil!',
							text:  'Informasi supplier ' + response.data.name + ' berhasil diperbarui.',
						});
					},

					error: function(response) {
						if (response.status == 422) {
							var errors = response.responseJSON;

							new PNotify({
								type:  'warning',
								title: 'Peringatan!',
								text:  'Terdapat kesalahan pada data yang dimasukkan',
							});

							cleanModal('#supplier-edit-form', false);

							$.each(errors.errors, function(col_val, msg) {
								$('#div-' + col_val, '#supplier-edit-form').addClass('has-error');
								$('#error- ' + col_val, '#supplier-edit-form').html(msg[0]);
							});
						}
						else {
							systemError();
						}
					}
				});
			});
		});
	</script>
@endpush
