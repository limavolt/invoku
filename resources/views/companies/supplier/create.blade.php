{{-- companies/supplier/create.blade.php --}}

@component('components.modal', [
    'modal_id'    => 'supplier-addition-modal',
    'modal_title' => 'pendaftaran supplier'
])
    @slot('modal_body')
        <form action="{{ route('suppliers.store') }}" method="post" id="supplier-addition-form">
            @csrf
            @include('companies.supplier.form')
        </form>
    @endslot

    @slot('modal_button')
        <button type="button" class="btn btn-default" id="supplier-addition-cancel-button" data-dismiss="modal">
            <span class="fa fa-arrow-circle-left"></span>
            {{ ucwords(__('batal')) }}
        </button>

        <button type="submit" class="btn btn-primary" id="supplier-addition-submit-button">
            <span class="fa fa-save"></span>
            {{ ucwords(__('simpan')) }}
        </button>
    @endslot
@endcomponent

@push('appscripts')
    <script type="text/javascript">
        $('#supplier-addition-modal').ready(function () {
            var modal_name    = '#supplier-addition-modal';
            var form_name     = '#supplier-addition-form';
            var modal         = $('#supplier-addition-modal');
            var form          = $('#supplier-addition-form');
            var submit_button = $('#supplier-addition-submit-button');

            /*
             * Menyiapkan form untuk diisi oleh pengguna.
             */
            modal.on('shown.bs.modal', function () {
                clearForm(form);
            });

			/*
			 * Mengirim data pada form penambahan resource saat tombol submisi ditekan.
			 */
			submit_button.click(function () {
				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function (response) {
                        generateSuccess(
                            modal,
                            response.data.name + ' berhasil ditambahkan sebagai supplier.'
                        );
					},

					error: function (response) {
						if (response.status == 422) {
                            generateError(
                                form_name,
                                response.responseJSON
                            );
						}
						else {
							systemError();
						}
					}
				});
			});
        });
    </script>
@endpush
