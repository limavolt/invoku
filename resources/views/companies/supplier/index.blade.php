{{-- companies/supplier/index.blade.php --}}

@extends('layouts.dashboard', ['page_title' => 'manajemen kontak'])

@section('content')
	@component('components.panel', ['panel_title' => 'daftar supplier'])
		<!-- start: toolbar -->
        <div class="row mb-md">
            <!-- start: left toolbar -->
            <div class="col-md-6">
                <button class="btn btn-primary" data-toggle="modal" data-target="#supplier-addition-modal" data-backdrop="static" data-keyboard="false">
                    <span class="fa fa-plus"></span>
                    {{ ucwords(__('tambah supplier')) }}
                </button>
            </div>
            <!-- end: left toolbar -->

            <!-- start: right toolbar -->
            <div class="col-md-6 text-right">
                <div class="btn-group">
                    <a href="{{ route('sales.create') }}" class="btn btn-default">
                        <span class="fa fa-print"></span>
                        {{ ucwords(__('gawe faktur')) }}
                    </a>

                    <a href="{{ route('incoming-goods.create') }}" class="btn btn-default">
                        <span class="fa fa-truck"></span>
                        {{ ucwords(__('barang datang')) }}
                    </a>
                </div>
            </div>
            <!-- end: right toolbar -->
        </div>
        <!-- end: toolbar -->

		@component('components.datatable-ajax', [
			'table_id'      => 'suppliers',
			'table_headers' => ['nama supplier', 'alamat', 'kontak'],
			'condition'     => true,
			'data'          => [
				['name' => 'name', 'data' => 'name'],
				['name' => 'address', 'data' => 'address'],
				['name' => 'pic', 'data' => 'pic'],
			],
			'renders' => [
				['text_align' => 'text-right', 'column_target' => '4']
			],
			'default_order' => 1
		])
			@slot('data_send_ajax')
			@endslot
		@endcomponent

		@include('companies.supplier.create')
		@include('companies.supplier.edit')
		@include('companies.supplier.destroy')
	@endcomponent
@endsection

@push('vendorstyles')
	<link href="{{ asset('assets/vendor/pnotify/pnotify.custom.css') }}" rel="stylesheet" style="text/css">
	<link href="{{ asset('assets/vendor/select2/select2.css') }}" rel="stylesheet" style="text/css">
@endpush

@push('vendorscripts')
	<script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendor/select2/select2.js') }}" type="text/javascript"></script>
@endpush
