{{-- companies/destroy.blade.php --}}

@component('components.modal', [
	'modal_id'    => 'modal-destroy-company',
	'modal_title' => 'penghapusan kontak',
])
	@slot('modal_body')
		<form method="post" action="{{ route('companies.destroy', 0) }}" id="form-destroy-company">
			@csrf
			@method('DELETE')

			<p>Apakah kamu yakin ingin menghapus kontak <span id="company-name"></span>?</p>
	@endslot

	@slot('modal_button')
		<button type="button" class="btn btn-primary" data-dismiss="modal"><span class="fa fa-arrow-circle-left"></span> {{ ucwords(__('batal')) }}</button>
		<button type="button" class="btn btn-default" id="btn-destroy-company"><span class="fa fa-trash-o"></span> {{ ucwords(__('hapus data')) }}</button>
	</form>
	@endslot
@endcomponent

@push('appscripts')
	<script type="text/javascript">
		$('#modal-destroy-company').ready(function () {
			/*
			 * Menampilkan jendela modal ketika tombol hapus resource ditekan.
			 */
			 $('#companies-table tbody').on('click', 'button[name="btn-destroy-company"]', function() {
                 // Ambil data company dari baris tombol yang ditekan.
				var data = table.row($(this).closest('tr')).data();

                $('#form-destroy-company #company-name').text(data.name);

				// Menetapkan URL untuk pemrosesan form.
				$('#form-destroy-company').attr('action', APP_URL + '/companies/' + $(this).data('id'));

				// Menampilkan jendela modal.
				$('#modal-destroy-company').modal('show');
			});

			/*
			 * Mengirim data pada form penghapusan resource saat tombol submisi ditekan.
			 */
			$('#btn-destroy-company').click(function () {
				var form = $('#form-destroy-company');

				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function(response) {
						$('#modal-destroy-company').modal('hide');

						if (response.status == 'success') {
							new PNotify({
								type:  'success',
								title: 'Berhasil!',
								text:  response.message,
							});

							table.ajax.reload();
						}
						else {
							new PNotify({
								type:  'warning',
								title: 'Peringatan!',
								text:  response.message,
							});
						}
					},

					error: function(response) {
                        console.log(response);
						systemError();
					}
				});
			});
		});
	</script>
@endpush
