{{-- companies/edit.blade.php --}}

@component('components.modal',
['modal_id'   => 'modal-edit-company',
'modal_title' => 'Ubah Data Perusahaan',
])
	@slot('modal_body')
		<form method="POST" action="{{ route('companies.update', 0) }}" id="form-edit-company">
			@csrf
			@method('PUT')
			@include('companies._form')
	@endslot

	@slot('modal_button')
			<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
			<button type="button" class="btn btn-primary" id="edit-company-btn">Simpan</button>
		</form>
	@endslot
@endcomponent

@push('appscripts')
    <script type="text/javascript">
        $('#modal-edit-company').ready(function () {
            /*
             * Menampilkan jendela modal ketika tombol edit resource ditekan.
             */
            $('#companies-table tbody').on('click', 'button[name="btn-edit-company"]', function () {
                cleanModal('#form-edit-company', false);

                // Ambil data supplier dari baris tombol yang ditekan.
                var data = table.row($(this).closest('tr')).data();

                // Mengisi nilai masing-masing bidang masukan formulir sesusai data yang diambil.
                $.each($('input, select, textarea', '#form-edit-company'), function () {
                    if ($(this).attr('id')) {
                        var id_element = $(this).attr('id');

                        if (data[id_element]) {
                            $('#' + id_element, '#form-edit-company').val(data[id_element]).trigger('change');
                        }
                        else {
                            $('#' + id_element, '#form-edit-company').val('').trigger('change');
						}
                    }
                });

                switch (data['type']) {
                    case 'Supplier':
                        $('#type-supplier', '#form-edit-company').prop('checked', true).val('supplier');
                        break;

                    case 'Pelanggan':
                        $('#type-customer', '#form-edit-company').prop('checked', true).val('customer');
                        break;

                    default:
                        $('#type-supplier', '#form-edit-company').prop('checked', true).val('supplier');
                        $('#type-customer', '#form-edit-company').prop('checked', true).val('customer');
                        break;
                }

				// Menetapkan URL untuk pemrosesan form.
				$('#form-edit-company').attr('action', APP_URL + '/customers/'+ $(this).data('id'));

				// Menampilkan jendela modal kepada pengguna.
				$('#modal-edit-company').modal('show');
            });

			/*
			 * Mengirim data pada form pengubahan resource saat tombol submisi ditekan.
			 */
			$('#edit-company-btn').click(function () {
				var form = $('#form-edit-company');

				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function(response) {
						$('#modal-edit-company').modal('hide');

						table.ajax.reload();

						new PNotify({
							type:  'success',
							title: 'Berhasil!',
							text:  'Informasi perusahaan ' + response.data.name + ' berhasil diperbarui.',
						});
					},

					error: function(response) {
						if (response.status == 422) {
							var errors = response.responseJSON;

							new PNotify({
								type:  'warning',
								title: 'Peringatan!',
								text:  'Terdapat kesalahan pada data yang dimasukkan',
							});

							cleanModal('#form-edit-company', false);

							$.each(errors.errors, function(col_val, msg) {
								$('#div-' + col_val, '#form-edit-company').addClass('has-error');
								$('#error- ' + col_val, '#form-edit-company').html(msg[0]);
							});
						}
						else {
                            systemError();
						}
					}
				});
			});
        });
    </script>
@endpush
