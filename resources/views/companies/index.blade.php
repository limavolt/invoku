{{-- companies/index.blade.php --}}

@extends('layouts.dashboard', ['page_title' => 'manajemen kontak'])

@section('content')
    @component('components.panel', ['panel_title' => 'daftar kontak'])
        <!-- start: toolbar -->
        <div class="row mb-md">
            <!-- start: left toolbar -->
            <div class="col-md-6">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modal-add-company" data-backdrop="static" data-keyboard="false">
                    <span class="fa fa-plus"></span>
                    {{ ucwords(__('tambah kontak')) }}
                </button>

                <div class="btn-group">
                    <a href="{{ route('suppliers.index') }}" class="btn btn-default">
                        <span class="fa fa-book"></span>
                        {{ ucwords(__('lihat supplier')) }}
                    </a>

                    <a href="{{ route('customers.index') }}" class="btn btn-default">
                        <span class="fa fa-book"></span>
                        {{ ucwords(__('lihat customer')) }}
                    </a>
                </div>
            </div>
            <!-- end: left toolbar -->

            <!-- start: right toolbar -->
            <div class="col-md-6 text-right">
                <div class="btn-group">
                    <a href="{{ route('sales.create') }}" class="btn btn-default">
                        <span class="fa fa-print"></span>
                        {{ ucwords(__('gawe faktur')) }}
                    </a>

                    <a href="{{ route('incoming-goods.create') }}" class="btn btn-default">
                        <span class="fa fa-truck"></span>
                        {{ ucwords(__('barang datang')) }}
                    </a>
                </div>

                <div class="btn-group">
                    <button id="company-template" class="btn btn-default" type="button">
                        <span class="fa fa-download"></span>
                        {{ ucwords(__('unduh templat')) }}
                    </button>

                    <button id="company-import" class="btn btn-default" type="button" data-toggle="modal" data-target="#companies-import-modal" data-backdrop="static" data-keyboard="false">
                        <span class="fa fa-upload"></span>
                        {{ ucwords(__('import data')) }}
                    </button>
                </div>
            </div>
            <!-- end: right toolbar -->
        </div>
        <!-- end: toolbar -->

        @component('components.datatable-ajax', [
			'table_id'      => 'companies',
			'table_headers' => ['nama kontak', 'alamat', 'kontak', 'jenis'],
			'condition'     => true,
			'data'          => [
				['name' => 'name', 'data' => 'name'],
				['name' => 'address', 'data' => 'address'],
				['name' => 'pic', 'data' => 'pic'],
				['name' => 'type', 'data' => 'type'],
			],
			'renders'       => [
                ['text_align' => 'text-right', 'column_target' => '5'],
            ],
            'default_order' => 1
        ])
            @slot('data_send_ajax')
            @endslot
        @endcomponent
	@endcomponent

	@include('companies.create')
	@include('companies.edit')
	@include('companies.destroy')
	@include('companies.import')
@endsection

@push('vendorstyles')
	<link href="{{ asset('assets/vendor/pnotify/pnotify.custom.css') }}" rel="stylesheet" style="text/css">
	<link href="{{ asset('assets/vendor/select2-4.0.8/css/select2.min.css') }}" rel="stylesheet" style="text/css">
	<link href="{{ asset('assets/vendor/select2-bootstrap/select2-bootstrap.min.css') }}" rel="stylesheet" style="text/css">
@endpush

@push('vendorscripts')
	<script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendor/select2-4.0.8/js/select2.min.js') }}" type="text/javascript"></script>
@endpush

@push('appscripts')
	<script type="text/javascript">
		$(document).ready(function() {
            $('#company-template').on('click', function () {
                window.location.href = "{{ route('companies.template') }}";
            });

            /*
			$('#companies-table tbody').on('click', 'button[name="btn-show-company"]', function() {
				var url = APP_URL + '/companies/'+ $(this).data('id');

				$.ajax({
					url: url,
					method: 'GET',
					success: function(response) {
						$('.modal-body', '#modal-show-company').html(response);
					}
				});

				$('#modal-show-company').modal('show');
            });
            */

            /* Menampilkan modal ubah data. */
			// $('#companies-table tbody').on('click', 'button[name="btn-edit-company"]', function() {
			// 	var data = table.row($(this).closest('tr')).data();

			// 	$.each($('input, select, textarea', '#form-edit-company'), function() {
			// 		if ($(this).attr('id')) {
            //             var id_element = $(this).attr('id');

			// 			if (data[id_element]) {
			// 				$('#' + id_element, '#form-edit-company').val(data[id_element]).trigger('change');
			// 			}
			// 			else {
			// 				if ($(this).attr('type') != 'checkbox') {
			// 					$('#' + id_element, '#form-edit-company').val('').trigger('change');
			// 				}
			// 			}
			// 		}
			// 	});

			// 	$('select[name="company_id"]', '#form-edit-company').val(data['company_id']).trigger('change');

			// 	$('#form-edit-company').attr('action', APP_URL + '/companies/' + $(this).data('id'));

			// 	cleanModal('#form-edit-company', false);

			// 	$('#modal-edit-company').modal('show');
			// });

            /* Menyimpan data modal pengubahan data. */
			// $('#btn-edit-company').click(function () {
			// 	var form = $('#form-edit-company');

			// 	$.ajax({
			// 		url: form.attr('action'),
			// 		method: form.attr('method'),
			// 		data: form.serialize(),
			// 		success: function(response) {
            //             $('#modal-edit-company').modal('hide');

			// 			table.ajax.reload();

			// 			new PNotify({
			// 				title: 'Sukses!',
			// 				text: 'Data profil berhasil diubah.',
			// 				type: 'success',
			// 			});
			// 		},
			// 		error: function(response) {
			// 			if (response.status == 422) {
            //                 var errors = response.responseJSON;

			// 				new PNotify({
			// 					title: 'Peringatan!',
			// 					text: 'Terdapat kesalahan pada data yang dimasukkan',
			// 					type: 'warning'
            //                 });

            //                 cleanModal('#form-edit-company', false);

			// 				$.each(errors.errors, function(col_val, msg) {
			// 					$('#div_' + col_val, '#form-edit-company').addClass('has-error');
			// 					$('#label_' + col_val, '#form-edit-company').html(msg[0]);
			// 				});
			// 			}
			// 			else {
			// 				systemError();
			// 			}
			// 		}
			// 	});
			// });

            /* Menampilkan modal penghapusan data. */
			$('#companies-table tbody').on('click', 'button[name="btn-destroy-company"]', function() {
				$('#form-destroy-company').attr('action', APP_URL + '/companies/' + $(this).data('id'));
				$('#modal-destroy-company').modal('show');
			});

            /* Memproses data modal penghapusan. */
			$('#btn-destroy-company').click(function () {
				var form = $('#form-destroy-company');

				$.ajax({
					url: form.attr('action'),
					method: form.attr('method'),
					data: form.serialize(),
					success: function(response) {
						$('#modal-destroy-company').modal('hide');

						if (response.status == 'destroyed') {
							new PNotify({
								title: 'Sukses!',
								text: response.message,
								type: 'success',
							});

							table.ajax.reload();
						}
						else {
							new PNotify({
								title: 'Peringatan!',
								text: response.message,
								type: 'warning',
							});
						}
					},
					error: function(response) {
						systemError();
					}
				});
			});
        });
	</script>
@endpush
