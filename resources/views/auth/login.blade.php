{{-- auth/login.blade.php --}}

@extends('layouts.page')

@section('content')
    <!-- start: page -->
    <section class="body-sign">
        <div class="center-sign">
            <a class="logo pull-left">
                <img src="{{ asset('assets/images/logo-invoku.png') }}" height="54" alt="Invoku: sistem manajemen inventori">
            </a>

            <!-- start: sign in panel -->
            <div class="panel panel-sign">
                <!-- start: sign in panel title -->
                <div class="panel-title-sign mt-xl text-right">
                    <h2 class="title text-uppercase text-bold m-none">
                        <i class="fa fa-user mr-xs"></i> Masuk
                    </h2>
                </div>
                <!-- end: sign in panel title -->

                <!-- start: sign in panel body -->
                <div class="panel-body">
                    <!-- start: sign in form -->
                    <form action="{{ route('login') }}" method="POST">
                        @csrf

                        <!-- start: user name input -->
                        <div class="form-group mb-lg {{ $errors->has('email') || $errors->has('username') ? 'has-error' : '' }}">
                            <label for="login">Username</label>

                            <div class="input-group input-group-icon">
                                <span class="input-group-addon">
                                    <span class="icon icon-lg">
                                        <i class="fa fa-user"></i>
                                    </span>
                                </span>

                                <input name="login" type="text" class="form-control input-lg" value="{{ old('email') ?: old('username') }}" required autofocus>
                            </div>

                            @if ($errors->has('email') || $errors->has('username'))
                                {{-- <span class="error help-block">{{ $errors->first('email') ? '' : $errors->first('username') }}</span> --}}
                                <span class="error help-block">{{ $errors->first() }}</span>
                            @endif
                        </div>
                        <!-- end: user name input -->

                        <!-- start: password input -->
                        <div class="form-group mb-lg {{ $errors->has('password') ? 'has-error' : '' }}">
                            <div class="clearfix">
                                <label for="password" class="pull-left">Kata Sandi</label>

                                <a href="{{ route('password.request') }}" class="pull-right">Lupa kata sandi?</a>
                            </div>

                            <div class="input-group input-group-icon">
                                <span class="input-group-addon">
                                    <span class="icon icon-lg">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </span>

                                <input class="form-control input-lg" type="password" name="password" required>
                            </div>
                        </div>
                        <!-- end: password input -->

                        <!-- start: submit button -->
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="checkbox-custom checkbox-default">
                                    <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label for="remember">Ingat saya</label>
                                </div>

                                <div class="pull-right"></div>
                            </div>

                            <div class="col-sm-4 text-right">
                                <button class="btn btn-primary hidden-xs" type="submit">Masuk</button>
                                <button class="btn btn-primary btn-block btn-lg visible-xs mt-lg" type="submit">Masuk</button>
                            </div>
                        </div>
                        <!-- end: submit button -->
                    </form>
                    <!-- end: sign in form -->
                </div>
                <!-- end: sign in panel body -->
            </div>
            <!-- end: sign in panel -->

            <p class="text-center text-muted mt-md mb-md">{{ config('app.name') }} built by <a href="https://limavolt.web.id">Limavolt</a> with <i class="fa fa-heart"></i> and <i class="fa fa-coffee"></i>.</p>
        </div>
    </section>
    <!-- end: page -->
@endsection
