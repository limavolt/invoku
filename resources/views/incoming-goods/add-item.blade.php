{{-- incoming-items/add-item.blade.php --}}

@component('components.modal', [
    'modal_id'    => 'choose-item-modal',
    'modal_title' => 'pilih barang datang'
])
    @slot('modal_body')
        <!-- start: incoming goods choose item form -->
        <form id="choose-item-form" method="post">
            @csrf

            <!-- start: product input field -->
            <div id="div-product" class="form-group">
                <label class="control-label col-sm-3 text-right" for="product">
                    {{ ucfirst(e(__('nama barang'))) }}
                </label>

                <div class="col-sm-9">
                    <select id="product" class="form-control" name="product">
                        <option></option>
                    </select>

                    <span id="error-product" class="help-block text-error"></span>
                </div>
            </div>
            <!-- end: product input field -->

            <!-- start: unit sale input field -->
            <div id="div-sale" class="form-group">
                <label class="control-label col-sm-3 text-right" for="sale">
                    {{ ucfirst(e(__('penjualan'))) }}
                </label>

                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-8">
                            <p class="help-block text-muted">Harga penjualan akan dihitung per</p>
                        </div>

                        <div class="col-sm-4">
                            <select id="sale" class="form-control" name="sale" required>
                                <option></option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end: unit sale input field -->

            <!-- start: capital price input field -->
            <div id="div-capital_price" class="form-group">
                <label class="control-label col-sm-3 text-right" for="capital_price">
                    {{ ucfirst(e(__('harga beli'))) }}
                </label>

                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon">Rp</span>
                                <input id="capital-price-label" class="form-control" name="capital_price_label" type="text" required>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            per <span id="sale-label"></span>.
                        </div>
                    </div>

                    <span id="error-capital_price" class="help-block text-error"></span>
                    <input id="capital-price" class="form-control" name="capital_price" type="hidden" required>
                </div>
            </div>
            <!-- end: capital price input field -->

            <!-- start: item count input field -->
            <div id="div-count" class="form-group">
                <label class="control-label col-sm-3 text-right" for="count">
                    {{ ucfirst(e(__('jumlah barang yang datang'))) }}
                </label>

                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-8">
                            <input id="count-label" class="form-control" name="count-label" type="text" required>
                            <input id="count" name="count" type="hidden" required>
                        </div>

                        <div class="col-sm-4">
                            <p id="unit-label" class="form-control-static"></p>
                            <input id="unit" name="unit" type="hidden">
                            <input id="pieces" name="pieces" type="hidden">
                        </div>

                        <div class="col-sm-12">
                            <span id="error-count" class="help-block text-error"></span>
                            <span id="error-unit" class="help-block text-error"></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end: item count input field -->
        </form>
        <!-- end: incoming goods choose item form -->
    @endslot

    @slot('modal_button')
        <button class="btn btn-default" type="button" data-dismiss="modal">
            <span class="fa fa-arrow-circle-left"></span>
            {{ ucwords(__('batal')) }}
        </button>

        <button id="choose-item-submit" class="btn btn-primary" type="submit">
            <span class="fa fa-check-square"></span>
            {{ ucwords(__('pilih barang')) }}
        </button>
    @endslot
@endcomponent

@push('vendorscripts')
    <script src="{{ asset('assets/vendor/autonumeric/autoNumeric-1.9.41.js') }}"></script>
@endpush

@push('appscripts')
    <script type="text/javascript">
        $('#choose-item-modal').ready(function () {
            $('#capital-price-label').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 0});
            $('#capital-price-label').on('change', function () {
                $('#capital-price').val($('#capital-price-label').autoNumeric('get'));
            });

            $('#count-label').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 0});
            $('#count-label').on('change', function () {
                $('#count').val($('#count-label').autoNumeric('get'));
            });

            $('#div-capital_price, #div-count, #div-sale').hide();

            $('#sale').on('change', function () {
                $('#sale-label').empty();
                $('#sale-label').append($('#sale option:selected').text());
            })

            /*
            * Memproses formulir penambahan produk ketika tombol
            * submit ditekan.
            */
            $('#choose-item-submit').click(function(e) {
                storeItem();
            });

            /*
            * Fungsi untuk menyimpan data pilihan produk sehingga
            * dapat digunakan formulir pencatatan penerimaan produk.
            */
            function storeItem() {
                var total = $('#total').val();

                $.ajax({
                    method: "post",
                    url:    "{{ route('incoming-goods.choose-item') }}",
                    data:   $('#choose-item-form').serialize(),

                    success: function(response) {
                        $('#selected-item-list').append(response);
                        $('#choose-item-modal').modal('hide');

                        var total = 0;

                        $('.item-list').each(function () {
                            if ($('.item-sale', this).val() == '1') {
                                var price = $('.item-value', this).val();
                            }
                            else {
                                var price = $('.item-value', this).val() * $('.item-pieces', this).val();
                            }

                            var count = $('.item-count', this).val();
                            var subtotal = price * count;

                            total += subtotal;
                        });

                        $('input#total').val('');
                        $('input#total').val(total);

                        total = Intl.NumberFormat('id-ID').format(total);

                        $('span#total-displayed').empty();
                        $('span#total-displayed').append('Rp' + total);
                    },

                    error: function(response) {
                        if (response.status == 422) {
                            var errors = response.responseJSON;

                            new PNotify({
                                type:  'warning',
                                title: 'Peringatan!',
                                text:  'Terdapat kesalahan pada data yang dimasukan.'
                            });

                            $.each(errors.errors, function (col_val, msg) {
                                $('#div-' + col_val).addClass('has-error');
                                $('#error-' + col_val).html(msg[0]);
                            });
                        }
                        else {
                            systemError();
                        }
                    }
                });
            }
        });

        // Fungsi yang dijalankan ketika modal choose-item-modal muncul.
        $('#choose-item-modal').on('shown.bs.modal', function () {
            clearForm($('#choose-item-form'));
        });

        // Fungsi yang dijalankan ketika nilai bidang product berubah.
        $('#product').on('change', function () {
            var item_id = $('#product').val();

            // Mendapatkan harga modal dari produk.
            var url_capital_price = "{{ route('ajax.get-capital-prices', ':id') }}";
            url_capital_price     = url_capital_price.replace(':id', item_id);
            $.ajax({
                method: 'get',
                type:   'json',
                url:     url_capital_price,

                success: function (data) {
                    $('#capital-price-label').empty();
                    // $('#capital-price-label').val(data.value);
                    $('#capital-price-label').autoNumeric('set', data.value);
                    $('#capital-price').val($('#capital-price-label').autoNumeric('get'));
                }
            });

            // Mendapatkan satuan dari produk.
            var url_unit = "{{ route('api.items.show', ':id') }}";
            url_unit     = url_unit.replace(':id', item_id);

            $.ajax({
                method: 'get',
                url:    url_unit,

                success: function (response) {
                    $('#unit-label').empty();
                    $('#unit-label').append(response.data.unit.label + ' berisi ' + response.data.unit.pieces + ' ' + response.data.unit.pieces_label);
                    $('#unit').val(response.data.unit.id);
                    $('#pieces').val(response.data.unit.pieces);

                    $('#sale').empty();
                    $('#sale').css('width', '100%');
                    $('#sale').select2({
                        dropdownParent: $('#choose-item-modal'),
                        placeholder:    "{{ ucfirst(__('pilih dari daftar')) }}",
                        allowClear:     true,

                        data: [
                            {
                                id: 1,
                                text: response.data.unit.label
                            },
                            {
                                id: 2,
                                text: response.data.unit.pieces_label
                            }
                        ],
                    });

                    if (response.data.stocks.length > 0) {
                        if (response.data.stocks[0].sale == 'unit') {
                            $('#sale').val('1').change();
                        }
                        else {
                            $('#sale').val('2').change();
                        }
                    }
                    else {
                        $('#sale').val('').change();
                    }

                    $('#sale-label').empty();
                    $('#sale-label').append($('#sale option:selected').text());
                },
            });

            if (item_id != null) {
                $('#div-capital_price, #div-count, #div-sale').show();
            }
            else {
                $('#div-capital_price, #div-count, #div-sale').hide();
            }
        });
    </script>
@endpush
