{{-- incoming-goods/index.blade.php --}}

@extends('layouts.dashboard', ['page_title' => 'kedatangan barang'])

@section('content')
    @component('components.panel', ['panel_title' => 'daftar kedatangan barang'])
        <!-- start: toolbar -->
        <div class="row mb-md">
            <!-- start: left toolbar -->
            <div class="col-md-6">
                <div class="btn-group">
                    <a href="{{ route('sales.create') }}" class="btn btn-default">
                        <span class="fa fa-print"></span>
                        {{ ucwords(__('gawe faktur')) }}
                    </a>

                    <a href="{{ route('incoming-goods.create') }}" class="btn btn-default">
                        <span class="fa fa-truck"></span>
                        {{ ucwords(__('barang datang')) }}
                    </a>
                </div>
            </div>
            <!-- end: left toolbar -->

            <!-- start: right toolbar -->
            <div class="col-md-6 text-right">

            </div>
            <!-- end: right toolbar -->
        </div>
        <!-- end: toolbar -->

        @component('components.datatable-ajax', [
            'table_id' => 'incoming-goods',
            'table_headers' => ['tanggal', 'supplier', 'total'],
            'data'          => [
                ['name' => 'arrival_date', 'data' => 'arrival_date'],
                ['name' => 'supplier.name', 'data' => 'supplier.name'],
                ['name' => 'total', 'data' => 'total', 'render' => "$.fn.dataTable.render.number( '.', ',', 2, 'Rp' )"],
            ],
            'renders'       => [
                ['text_align' => 'text-right', 'column_target' => '3']
            ]
        ])
            @slot('data_send_ajax')
            @endslot
        @endcomponent
    @endcomponent
@endsection

@push('vendorstyles')
	<link href="{{ asset('assets/vendor/select2/select2.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/vendor/pnotify/pnotify.custom.css') }}" rel="stylesheet" type="text/css">
@endpush

@push('vendorscripts')
	<script src="{{ asset('assets/vendor/select2/select2.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}" type="text/javascript"></script>
@endpush
