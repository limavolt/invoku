{{-- incoming-goods/selected-item.blade.php --}}

 <tr class="item-list">
    <td>{{ $item->name }}</td>
    <td>{{ $item_count }} {{ $item_unit_label }} berisi {{ $item_unit_pieces }} {{ $item_pieces_label }}</td>
    <td class="text-right">Rp{{ $price_text }} per {{ strtolower($sale == 1 ? $item_unit_label : $item_pieces_label) }}</td>
    <td class="text-right">Rp{{ number_format(($sale == 1 ? $item_price * $item_count : $item_price * $item_unit_pieces * $item_count ), 0, ',', '.') }}</td>
    <td class="text-right"><button type="button" class="btn btn-link" name="delete-item-button"><span class="fa fa-trash-o"></span> {{ ucwords(__('hapus')) }}</button></td>

    <input name="item[{{ $item->id }}][id]" type="hidden" value="{{ $item->id }}">
    <input name="item[{{ $item->id }}][name]" type="hidden" value="{{ $item->name }}">
    <input class="item-value" name="item[{{ $item->id }}][price]" type="hidden" value="{{ $item_price }}">
    <input class="item-count" name="item[{{ $item->id }}][count]" type="hidden" value="{{ $item_count }}">
    <input class="item-pieces" name="item[{{ $item->id }}][pieces]" type="hidden" value="{{ $item_unit_pieces }}">
    <input name="item[{{ $item->id }}][unit]" type="hidden" value="{{ $item_unit }}">
    <input class="item-sale" name="item[{{ $item->id }}][sale]" type="hidden" value="{{ $sale }}">
 </tr>
