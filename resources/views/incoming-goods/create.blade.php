{{-- incoming-goods/create.blade.php --}}

@extends('layouts.dashboard', ['page_title' => 'pencatatan kedatangan barang'])

@section('content')
    <!-- start: content lead -->
    <div class="col-sm-9 col-sm-offset-3 mb-lg">
        <p>
            Sebelum memilih barang, pastikan bahwa <strong>suplier, barang, dan satuan</strong>
            sudah terdaftar pada sistem. Gunakan tombol yang tersedia untuk menambahkannya terlebih dulu
            sebelum mencatat barang masuk.
        </p>
    </div>
    <!-- end: content lead -->

    <!-- start: incoming goods form -->
    <form id="incoming-good-form" action="{{ route('incoming-goods.store') }}" method="post">
        @csrf

        <!-- start: arrival date input field -->
        <div id="div-arrival_date" class="form-group">
            <label class="control-label col-sm-3 text-right" for="arrival_date">
                {{ ucfirst(__('tanggal masuk')) }}
            </label>

            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>

                            <input id="arrival_date" class="form-control" name="arrival_date" type="text">
                        </div>

                        <span id="error-arrival_date" class="help-block text-error"></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: arrival date input field -->

        <!-- start: supplier input field -->
        <div id="div-supplier_id" class="form-group">
            <label class="control-label col-sm-3 text-right" for="supplier_id">
                {{ ucfirst(__('nama supplier')) }}
            </label>

            <div class="col-sm-9">
                <div class="row">
                    <div class="col-md-8">
                        <select id="supplier_id" class="form-control" name="supplier_id">
                            <option></option>
                        </select>

                        <span class="help-block text-error" id="error-supplier_id"></span>
                    </div>

                    <div class="col-md-4">
                        <button class="btn btn-block btn-default" type="button" data-toggle="modal" data-target="#supplier-addition-modal" data-backdrop="static" data-keyboard="false">
                            <span class="fa fa-plus"></span>
                            {{ ucwords(__('tambah supplier baru')) }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: supplier input field -->

        <!-- start: product input field -->
        <div id="div-items" class="form-group">
            <label class="control-label col-sm-3 text-right" for="items">
                {{ ucfirst(__('daftar barang masuk')) }}
            </label>

            <div id="item-list" class="col-sm-9">
                <div class="row">
                    <div class="col-md-8">
                        <button id="add-item" class="btn btn-primary" type="button">
                            <span class="fa fa-check-square"></span>
                            {{ ucwords(e(__('pilih barang'))) }}
                        </button>
                    </div>

                    <div class="col-md-4">
                        <button class="btn btn-default col-md-12" type="button" data-toggle="modal" data-target="#modal-add-item" data-backdrop="static" data-keyboard="false">
                            <span class="fa fa-plus"></span>
                            {{ ucwords(__('tambah item barang baru')) }}
                        </button>
                    </div>
                </div>

                <!-- start: item list table -->
                <div class="panel mt-md">
                    <div class="panel-body table-responsive">
                        <table id="item-list-table" class="table mb-md">
                            <thead>
                                <tr>
                                    <th>{{ strtoupper(e(__('nama barang'))) }}</th>
                                    <th>{{ strtoupper(e(__('jumlah'))) }}</th>
                                    <th class="text-right">{{ strtoupper(e(__('harga beli satuan'))) }}</th>
                                    <th class="text-right">{{ strtoupper(__('subtotal')) }}</th>
                                    <th class="text-right">{{ strtoupper(__('aksi')) }}</th>
                                </tr>
                            </thead>

                            <tbody id="selected-item-list"></tbody>
                        </table>
                    </div>

                    <div class="panel-footer text-right">
                        <p class="lead">Total belanja: <span id="total-displayed"></span></p>
                        <input id="total" name="total" type="hidden">
                    </div>
                </div>
                <!-- end: item list table -->
            </div>
        </div>
        <!-- end: product input field -->

        <hr>

        <!-- start: incoming good form buttons. -->
        <div class="form-group mt-lg">
            <div class="col-md-12">
                <a href="{{ route('stocks.index') }}" class="btn btn-default">
                    <span class="fa fa-arrow-left"></span>
                    {{ ucwords(__('batal')) }}
                </a>

                <button id="incoming-goods-submit-button" class="btn btn-primary pull-right" type="submit">
                    <span class="fa fa-save"></span>
                    {{ ucwords(__('simpan catatan')) }}
                </button>
            </div>
        </div>
        <!-- end: incoming good form buttons. -->
    </form>
    <!-- end: incoming goods form -->

    @include('incoming-goods.add-item')
    @include('companies.supplier.create')
    @include('items.create')
@endsection

@push('vendorstyles')
    <!-- start: incoming good recording page vendor styles -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/pnotify/pnotify.custom.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/vendor/select2-4.0.8/css/select2.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/vendor/select2-bootstrap/select2-bootstrap.min.css') }}" type="text/css">
    <!-- end: incoming good recording page vendor styles -->
@endpush

@push('vendorscripts')
    <!-- start: incoming good recording page vendor scripts -->
    <script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendor/select2-4.0.8/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendor/bootstrap-datepicker/js/locales/bootstrap-datepicker.id.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendor/fuelux/js/spinner.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/moment.js') }}" type="text/javascript"></script>
    <!-- end: incoming good recording page vendor scripts -->
@endpush

@push('appscripts')
    <!-- start: incoming good recording page app scripts -->
    <script type="text/javascript">
        /* Function to be running when document is ready. */
        $(document).ready(function () {
            var incoming_good_form_name = '#incoming-good-form';
            var incoming_good_form      = $('#incoming-good-form');

            /* Menyiapkan form kosong kepada pengguna. */
            clearForm(incoming_good_form);

            /* Mengaturan opsi penggunaan datepicker dan
             * nilai baku ketika dimuat pertama kali. */
            var date  = new Date();
            var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

            $(incoming_good_form_name + ' #arrival_date').datepicker({
                format:         'dd/mm/yyyy',
                endDate:        '0d',
                language:       'id-ID',
                todayBtn:       'linked',
                todayHighlight: true
            });

            $(incoming_good_form_name + ' #arrival_date').datepicker('setDate', today);

            /* Mempersiapkan select2 pada form. */
            $.fn.select2.defaults.set( "theme", "bootstrap" );

            /* Receive a supplier list when the page loads. */
            selectCompany('supplier', $(incoming_good_form_name + ' #supplier_id'), incoming_good_form);

            /* Set total displayed as zero. */
            $('span#total-displayed').empty();
            $('span#total-displayed').append('Rp' + 0);
        });

        /* Menjalankan aksi yang diminta ketika tombol add-item
         * ditekan pengguna. */
        $('#add-item').click(function () {
            var supplier_id = $('#supplier_id').val();

            /* Check if supplier is selected. */
            if (supplier_id) {
                $('#choose-item-modal').modal({
                    backdrop: 'static',
                    keyboard: false
                });

                clearForm($('#choose-item-form'));

                selectItem(supplier_id, $('#choose-item-form #product'), $('#choose-item-form'));
            }
            else {
                new PNotify({
                    type:  'warning',
                    title: 'Peringatan!',
                    text:  'Pilih supplier dulu bos.'
                });
            }
        });

        /* Receive supplier list when the modal window
         * for adding suppliers is closed. */
        $('#supplier-addition-modal, #modal-add-item').on('hide.bs.modal', function() {
            selectCompany('supplier', $('#incoming-good-form #supplier_id'), $('#incoming-good-form'), 'keep');
        });

        /* Receive item unit when the modal window for adding unit
         * is closed. */
        $('#modal-add-inventory-unit').on('hide.bs.modal', function () {
            selectUnit($('#form-add-item #unit_id'), $('#form-add-item'));
        });

        /* Stacked modal window solutions. */
        $('#modal-add-inventory-unit').on('hidden.bs.modal', function () {
            if ($('.modal.in').length > 0) {
                $('body').addClass('modal-open');
            }
        })

        /* Function to remove selected items when the delete item button is pressed. */
        $('#item-list-table tbody').on('click', 'button[name=delete-item-button]', function () {

            // Remove selected row.
            $(this).parents('tr').remove();

            // Recalculate the total purchase value.
            var total = 0;

            $('.item-list').each(function () {
                var price    = $('.item-value', this).val();
                var count    = $('.item-count', this).val();
                var subtotal = price * count;

                total += subtotal;
            });

            $('input#total').val('');
            $('input#total').val(total);

            total = Intl.NumberFormat('id-ID').format(total);

            $('span#total-displayed').empty();
            $('span#total-displayed').append('Rp' + total);
        });
    </script>
    <!-- end: incoming good recording page app scripts -->
@endpush
