{{-- layouts/page.blade.php  --}}

<!doctype html>
<html class="fixed" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Dokumen metadata -->
        <title>{{ config('app.name') }}</title>

        <!-- start: stylesheets -->
        <!-- Web Fonts  -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" type="text/css">
        @stack('webfonts')
        <!-- Vendor stylesheets -->
        <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/vendor/magnific-popup/magnific-popup.css') }}" type="text/css">
        @stack('vendorstyles')
        <!-- Theme stylesheets -->
        <link rel="stylesheet" href="{{ asset('assets/stylesheets/theme.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/stylesheets/skins/default.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/stylesheets/theme-custom.css') }}" type="text/css">
        @stack('themestyles')
        <!-- App stylesheets -->
        @stack('appstyles')
        <!-- end: stylesheets -->

        <!-- start: head scripts -->
        <script src="{{ asset('js/modernizr.js')  }}"></script>
        @stack('headscripts')
        <!-- end: head scripts -->
    </head>

    <body>
        @yield('content')

        <!-- start: body scripts -->
        <!-- Vendor scripts -->
        <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
        <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/nanoscroller/nanoscroller.js') }}"></script>
        <script src="{{ asset('assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
        <script src="{{ asset('assets/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>
        @stack('vendorscripts')
        <!-- Theme scripts -->
        <script src="{{ asset('assets/javascripts/theme.js') }}"></script>
        <script src="{{ asset('assets/javascripts/theme.custom.js') }}"></script>
        <script src="{{ asset('assets/javascripts/theme.init.js') }}"></script>
        @stack('themescripts')
        <!-- App scripts -->
        @stack('appscripts')
        <!-- end: body scripts -->
    </body>
</html>
