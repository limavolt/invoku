{{-- layouts/print.blade.php --}}

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!-- start: metadata dokumen -->
        <!-- Parameter kontrol -->
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <!-- Parameter metadata -->
        <title>{{ ucfirst(__('faktur penjualan')) . ' ' . strtoupper(e(settings()->get('invoice_code'))) . \Carbon\Carbon::parse($invoice->issued_date)->format('ym') . sprintf('%03d', $invoice->number) . ' - ' . strtoupper(config('app.name')) }}</title>
        <!-- end: metadata dokumen -->

        <!-- start: stylesheets -->
        @stack('printstyles')
        <!-- end: stylesheets -->
    </head>

    <body>
        @yield('content')
    </body>
</html>
