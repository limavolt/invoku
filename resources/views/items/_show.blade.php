{{-- items/_show.blade.php --}}

<h3 class="text-semibold">{{ strtoupper($item->name) }}</h3>

<p class="text-muted m-none">Merek: {{ $item->item_brand->name }}</p>

<p>Produk ini disuplai oleh <strong>{{ $item->supplier->name }}</strong>
    yan masuk sebagai kategori produk <strong>{{ $item->item_group->name }}</strong>.</p>

<hr>

<h4>Catatan</h4>
<p class="mt-sm">{{ $item->description }}</p>
