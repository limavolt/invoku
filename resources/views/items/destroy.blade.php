{{-- items/destroy.blade.php --}}

@component('components.modal', [
	'modal_id'    => 'modal-destroy-item',
	'modal_title' => 'hapus produk terpilih',
])
	@slot('modal_body')
		<form method="post" action="{{ route('items.destroy', 0) }}" id="form-destroy-item">
			@csrf
			@method('DELETE')

			<p>Apakah anda yakin akan menghapus <span id="item-name"></span>?</p>
	@endslot

	@slot('modal_button')
			<button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-arrow-circle-left"></span> {{ ucwords(__('batal')) }}</button>
			<button type="button" class="btn btn-danger" id="btn-destroy-item"><span class="fa fa-trash-o"></span> {{ ucwords(__('hapus data')) }}</button>
		</form>
	@endslot
@endcomponent

@push('appscripts')
	<script type="text/javascript">
		$('#modal-destroy-item').ready(function () {
			/*
			 * Menampilkan jendela modal ketika tombol hapus resource ditekan.
			 */
			 $('#items-table tbody').on('click', 'button[name="btn-destroy-item"]', function() {
				// Ambil data company dari baris tombol yang ditekan.
				var data = table.row($(this).closest('tr')).data();

                $('#form-destroy-item #item-name').text(data.name);

                // Menetapkan URL untuk pemrosesan form.
				$('#form-destroy-item').attr('action', APP_URL + '/items/' + $(this).data('id'));

				// Menampilkan jendela modal.
				$('#modal-destroy-item').modal('show');
			});

			/*
			 * Mengirim data pada form penghapusan resource saat tombol submisi ditekan.
			 */
			$('#btn-destroy-item').click(function () {
				var form = $('#form-destroy-item');

				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function(response) {
						$('#modal-destroy-item').modal('hide');

						if (response.status == 'success') {
							new PNotify({
								type:  'success',
								title: 'Berhasil!',
								text:  response.message,
							});

							table.ajax.reload();
						}
						else {
							new PNotify({
								type:  'warning',
								title: 'Peringatan!',
								text:  response.message,
							});
						}
					},

					error: function(response) {
						systemError();
					}
				});
			});
		});
	</script>
@endpush
