{{-- items/_form.blade.php --}}

<div class="form-group mt-lg" id="div-item_group_id">
	<label class="col-sm-3 control-label text-right">Kategori barang</label>

	<div class="col-sm-9">
		<select class="form-control" id="item_group_id" name="item_group_id">
			<option value="">Pilih kategori ...</option>

			@foreach ($item_groups as $item_group)
				<option value="{{ $item_group->id }}">{{ $item_group->name }}</option>
			@endforeach
		</select>

		<p class="help-block text-muted">Wajib diisi.</p>
		<p class="help-block text-muted text-left">Kamu dapat langsung mendaftarkan kategori barang di sini.</p>
		<p class="help-block text-error" id="error-item_group_id"></p>
	</div>
</div>

<div class="form-group mt-lg" id="div-item_brand_id">
	<label class="col-sm-3 control-label text-right">Merek barang</label>

	<div class="col-sm-9">
		<select class="form-control" id="item_brand_id" name="item_brand_id">
			<option value="">Pilih merek ...</option>

			@foreach ($item_brands as $brand)
				<option value="{{ $brand->id }}">{{ $brand->name }}</option>
			@endforeach
		</select>

		<p class="help-block text-muted">Wajib diisi.</p>
		<p class="help-block text-muted text-left">Kamu dapat langsung mendaftarkan merek barang di sini.</p>
		<p class="help-block text-error" id="error-item_brand_id"></p>
	</div>
</div>

<div class="form-group mt-lg" id="div-supplier_id">
	<label class="col-sm-3 control-label text-right">Supplier</label>

	<div class="col-sm-9">
		<select class="form-control" id="supplier_id" name="supplier_id">
			<option value="">Pilih supplier ...</option>

			@foreach ($suppliers as $supplier)
				<option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
			@endforeach
		</select>

		<span class="help-block text-muted">Wajib diisi.</span>
		<span class="help-block text-muted">
			Kamu dapat mendaftarkan supplier baru di halaman daftar supplier.
			<a href="{{ route('suppliers.index') }}">Klik di sini untuk membuka halaman daftar supplier.</a>
			Atau gunakan tombol yang tersedia di atas tabel daftar produk.
		</span>
		<span class="help-block text-error" id="error-supplier_id"></span>
	</div>
</div>

<hr>

<div class="form-group mt-lg" id="div-name">
	<label class="col-sm-3 control-label text-right">Nama barang</label>

	<div class="col-sm-9">
		<input type="text" id="name" name="name" class="form-control" required>

		<span class="help-block text-muted">Wajib diisi</span>
		<span class="help-block text-error" id="error-name"></span>
	</div>
</div>

<div class="form-group mt-lg" id="div-unit_id">
	<label class="col-sm-3 control-label text-right">Satuan</label>

	<div class="col-sm-9">
        <div class="row">
            <div class="col-md-7">
                <select class="form-control" id="unit_id" name="unit_id">
                    <option></option>
                </select>
            </div>

            <div class="col-md-5">
                <button type="button" class="btn btn-block btn-default btn-model-add" id="unit-addition-button" data-toggle="modal" data-target="#modal-add-inventory-unit" data-backdrop="static" data-keyboard="false">
                    <span class="fa fa-plus"></span>
                    {{ ucwords(__('tambah satuan')) }}
                </button>
            </div>
        </div>

		<p class="help-block text-muted">Wajib diisi.</p>
        <span class="help-block text-error" id="error-unit_id"></span>
	</div>
</div>

<hr>

<div class="form-group mt-lg" id="div-description">
	<label class="col-sm-3 control-label text-right">Catatan</label>

	<div class="col-sm-9">
		<textarea name="description" id="description" class="form-control"></textarea>

		<span class="help-block text-error" id="error-description"></span>
	</div>
</div>
