{{-- items/create.blade.php --}}

@component('components.modal', [
	'modal_id'    => 'modal-add-item',
	'modal_title' => 'tambah produk baru'
])
	@slot('modal_body')
		<form action="{{ route('items.store') }}" method="post" id="form-add-item">
			@csrf
			@include('items._form')
		</form>
	@endslot

	@slot('modal_button')
		<button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-arrow-circle-left"></span> {{ ucwords(__('batal')) }}</button>
		<button type="submit" class="btn btn-primary" id="btn-add-item"><span class="fa fa-save"></span> {{ ucwords(__('simpan')) }}</button>
	@endslot
@endcomponent

@push('appscripts')
	<script type="text/javascript">
		/*
		 * Memodifikasi tampilan formulir penambahan resource
		 * ketika modal penambahan resource dimunculkan.
		 */
		$('#modal-add-item').on('shown.bs.modal', function() {
			clearForm($('#modal-add-item #form-add-item'));
            disableEvents('#form-add-item');

            $(this).find('select').each(function () {
                switch ($(this).attr('id')) {
                    case 'item_group_id':
                        selectItemGroup($('#form-add-item #item_group_id'), $('#form-add-item'));
                        break;

					case 'unit_id':
                        selectUnit($('#form-add-item #unit_id'), $('#form-add-item'));
                        break;

					case 'supplier_id':
                        selectCompany('supplier', $(this), $('#form-add-item'));
						break;

					default:
						$(this).select2({
							dropdownParent: $('#form-add-item'),
							tags: true,
							placeholder: 'Pilih dari daftar',
							allowClear: true
						});
						break;
				}
			});
		});

		/*
 		 * Mengirim data pada form penambahan resource saat tombol submisi ditekan.
		 */
		$('#btn-add-item').click(function() {
			var modal = $('#modal-add-item');
			var form  = $('#form-add-item');

			$.ajax({
				url:    form.attr('action'),
				method: form.attr('method'),
				data:   form.serialize(),

				success: function (response) {
					generateSuccess(modal, response.data.name + ' berhasil ditambahkan dalam daftar produk.');
				},

				error: function (response) {
					if (response.status == 422) {
						generateError('#form-add-item', response.responseJSON);
					}
					else {
						systemError();
					}
				}
			});
		});
	</script>
@endpush
