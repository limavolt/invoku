{{-- items/index.blade.php --}}

@extends('layouts.dashboard', ['page_title' => 'manajemen produk'])

@section('content')
	<p class="lead">
		Di sini kamu dapat mengatur informasi produk yang kamu miliki.
		Kamu dapat melihat daftar produk, menambahkan produk,
		memperbarui informasi produk, dan menghapus produk yang ada.
	</p>

    @component('components.panel', ['panel_title' => 'daftar barang'])
        <!-- start: toolbar -->
        <div class="row mb-md">
            <!-- start: left toolbar -->
            <div class="col-md-6">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modal-add-item" data-backdrop="static" data-keyboard="false">
                    <span class="fa fa-plus"></span>
                    {{ ucwords(__('tambah barang')) }}
                </button>

                <div class="btn-group">
                    <a href="{{ route('sales.create') }}" class="btn btn-default">
                        <span class="fa fa-print"></span>
                        {{ ucwords(__('gawe faktur')) }}
                    </a>

                    <a href="{{ route('incoming-goods.create') }}" class="btn btn-default">
                        <span class="fa fa-truck"></span>
                        {{ ucwords(__('barang datang')) }}
                    </a>
                </div>
            </div>
            <!-- end: left toolbar -->

            <!-- start: right toolbar -->
            <div class="col-md-6 text-right">
                <div class="btn-group">
                    <button class="btn btn-default btn-model-add" data-toggle="modal" data-target="#supplier-addition-modal" data-backdrop="static" data-keyboard="false">
                        <span class="fa fa-plus"></span>
                        {{ ucwords(__('tambah supplier')) }}
                    </button>

                    <button class="btn btn-default btn-model-add" id="unit-addition-button" data-toggle="modal" data-target="#modal-add-inventory-unit" data-backdrop="static" data-keyboard="false">
                        <span class="fa fa-plus"></span>
                        {{ ucwords(__('tambah satuan')) }}
                    </button>
                </div>
            </div>
            <!-- end: right toolbar -->
        </div>
        <!-- end: toolbar -->

		@component('components.datatable-ajax', [
			'table_id'      => 'items',
			'table_headers' => ['kategori produk', 'merek produk', 'supplier', 'nama produk'],
			'condition'     => true,
			'data'          => [
				['name' => 'itemGroup.name', 'data' => 'item_group.name'],
				['name' => 'itemBrand.name', 'data' => 'item_brand.name'],
				['name' => 'supplier.name', 'data' => 'supplier.name'],
				['name' => 'name', 'data' => 'name'],
			],
			'default_order' => 4
		])
				@slot('data_send_ajax')
				@endslot
		@endcomponent
	@endcomponent

	@include('items.create')
	@include('items.show')
	@include('items.edit')
	@include('items.destroy')
	@include('items.import')
	@include('companies.supplier.create')
	@include('inventory-units.create')
@endsection

@push('vendorstyles')
	<link href="{{ asset('assets/vendor/pnotify/pnotify.custom.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/vendor/select2-4.0.8/css/select2.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/vendor/select2-bootstrap/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
@endpush

@push('vendorscripts')
	<script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendor/select2-4.0.8/js/select2.min.js') }}" type="text/javascript"></script>
@endpush

@prepend('appscripts')
	<script type="text/javascript">
		$(document).ready(function () {
			$.fn.select2.defaults.set( "theme", "bootstrap" );

			$('#supplier-addition-modal, #modal-add-inventory-unit').on('hide.bs.modal', function() {
				location.reload();
			});
		});
	</script>
@endprepend
