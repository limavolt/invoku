{{-- items/import.blade.php --}}

@component('components.modal', [
    'modal_id'    => 'item-import-modal',
    'modal_title' => 'impor produk'
])
    @slot('modal_body')
        <form action="{{ route('import.items') }}" method="post" id="item-import-form" enctype="multipart/form-data">
            @csrf

            <div class="form-group" id="div-file">
                <label for="file" class="col-sm-3 control-label text-right">
                    {{ ucfirst(__('file excel')) }}
                </label>

                <div class="col-sm-9">
                    <input type="file" name="file" id="file">
                </div>
            </div>
    @endslot

    @slot('modal_button')
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-arrow-circle-left"></span> {{ ucwords(__('batal')) }}</button>
            <button type="submit" class="btn btn-primary" id="btn-import-item"><span class="fa fa-database"></span> {{ ucwords(__('import')) }}</button>
        </form>
    @endslot
@endcomponent
