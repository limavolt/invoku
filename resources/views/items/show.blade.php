{{-- items/show.blade.php --}}

@component('components.modal', [
    'modal_id'    => 'modal-show-item',
    'modal_title' => 'detil informasi produk',
    'modal_body'  => ''
])

    @slot('modal_button')
        <button type="button" class="btn btn-primary" data-dismiss="modal">{{ ucwords(__('tutup')) }}</button>
    @endslot
@endcomponent

@push('appscripts')
    <script type="text/javascript">
        $('#modal-show-item').ready(function () {
			/*
			 * Menampilkan jendela modal ketika tombol show resource ditekan.
			 */
			$('#items-table tbody').on('click', 'button[name="btn-show-item"]', function() {
				var url = APP_URL + '/items/' + $(this).data('id');

				$.ajax({
                    method: 'get',
					url:    url,

					success: function (response) {
						$('.modal-body', '#modal-show-item').html(response);
					}
				});

				$('#modal-show-item').modal('show');
			});
        });
    </script>
@endpush
