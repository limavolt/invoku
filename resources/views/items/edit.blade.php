{{-- items/edit.blade.php --}}

@component('components.modal', [
	'modal_id'    => 'modal-edit-item',
	'modal_title' => 'perbarui informasi produk'
])
	@slot('modal_body')
		<form action="{{ route('suppliers.update', 0) }}" method="put" id="form-edit-item">
			@csrf
			@include('items._form')
	@endslot

	@slot('modal_button')
			<button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-arrow-circle-left"></span> {{ ucwords(__('batal')) }}</button>
			<button type="button" class="btn btn-primary" id="btn-edit-item"><span class="fa fa-edit"></span> {{ ucwords(__('perbarui')) }}</button>
		</form>
	@endslot
@endcomponent

@push('appscripts')
	<script type="text/javascript">
		$('#modal-edit-item').ready(function () {
			/*
			 * Menampilkan jendela modal ketika tombol edit resource ditekan.
			 */
			 $('#items-table tbody').on('click', 'button[name="btn-edit-item"]', function() {
				// Menjaga agar modal tetap tampil ketika pengguna meng-klik di luar modal.
				$('#modal-edit-item').modal({backdrop: "static", keyboard: false});

				// Ambil data produk dari baris tombol yang ditekan.
				var data = table.row($(this).closest('tr')).data();

				// Mengisi nilai masing-masing bidang masukan formulir sesusai data yang diambil.
				$.each($('input, select, textarea', '#form-edit-item'), function() {
					if ($(this).attr('id')) {
						var id_element = $(this).attr('id');

						if (data[id_element]) {
							$('#' + id_element, '#form-edit-item').val(data[id_element]).trigger('change');
						}
						else {
							$('#' + id_element, '#form-edit-item').val('').trigger('change');
						}
					}
				});

				// Menetapkan URL untuk pemrosesan form.
				$('#form-edit-item').attr('action', APP_URL + '/items/' + $(this).data('id'));

				// Menampilkan jendela modal kepada pengguna.
				$('#modal-edit-item').modal('show');
			});

			/*
			 * Memodifikasi tampilan formulir penambahan resource
			 * ketika modal penambahan resource dimunculkan.
			 */
			$('#modal-edit-item').on('shown.bs.modal', function () {
				disableEvents('#form-edit-item');

				$(this).find('select').each(function () {
					switch ($(this).attr('id')) {
						default:
							$(this).select2({
								dropdownParent: $('#modal-edit-item'),
								placeholder: 'Pilih dari daftar'
							});
							break;
					}
				});
			});

			/*
			 * Mengirim data pada form pengubahan resource saat tombol submisi ditekan.
			 */
			 $('#btn-edit-item').click(function () {
				var form = $('#form-edit-item');

				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function(response) {
						$('#modal-edit-item').modal('hide');

						table.ajax.reload();

						new PNotify({
							type:  'success',
							title: 'Berhasil!',
							text:  'Informasi produk ' + response.data.name + ' berhasil diperbarui.',
						});
					},

					error: function(response) {
						console.log(response);

						if (response.status == 422) {
							var errors = response.responseJSON;

							new PNotify({
								type:  'warning',
								title: 'Peringatan!',
								text:  'Terdapat kesalahan pada data yang dimasukkan',
							});

							cleanModal('#form-edit-item', false);

							$.each(errors.errors, function(col_val, msg) {
								$('#div-' + col_val, '#form-edit-item').addClass('has-error');
								$('#error-' + col_val, '#form-edit-item').html(msg[0]);
							});
						}
						else {
							systemError();
						}
					}
				});
			});
		});
	</script>
@endpush
