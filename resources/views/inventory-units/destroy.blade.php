@component('components.modal', [
	'modal_id'    => 'modal-destroy-inventory-unit',
	'modal_title' => 'hapus satuan terpilih',
])
	@slot('modal_body')
		<form method="POST" action="{{ route('inventory-units.destroy', 0) }}" id="form-destroy-inventory-unit">
			@csrf
			@method('DELETE')

			<p>Apakah anda yakin akan menghapus data ini?</p>
	@endslot

	@slot('modal_button')
			<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
			<button type="button" class="btn btn-danger" id="btn-destroy-inventory-unit">Hapus</button>
		</form>
	@endslot
@endcomponent

@push('appscripts')
	<script type="text/javascript">
		$('#modal-destroy-inventory-unit').ready(function () {
			/*
			 * Menampilkan jendela modal ketika tombol hapus resource ditekan.
			 */
			$('#units-table tbody').on('click', 'button[name="btn-destroy-inventory-unit"]', function() {
				// Menetapkan URL untuk pemrosesan form.
				$('#form-destroy-inventory-unit').attr('action', APP_URL + '/inventory-units/'+ $(this).data('id'));

				// Menampilkan jendela modal.
				$('#modal-destroy-inventory-unit').modal('show');
			});

			/*
			 * Mengirim data pada form penghapusan resource saat tombol submisi ditekan.
			 */
			$('#btn-destroy-inventory-unit').click(function (){
				var form = $('#form-destroy-inventory-unit');

				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function(response) {
						$('#modal-destroy-inventory-unit').modal('hide');

						if (response.status == 'destroyed') {
							new PNotify({
								type:  'success',
								title: 'Berhasil!',
								text:  response.message,
							});

							table.ajax.reload();
						}
						else {
							new PNotify({
								type:  'warning',
								title: 'Peringatan!',
								text:  response.message,
							});
						}
					},

					error: function(response){
						systemError();
					}
				});
			});
		});
	</script>
@endpush
