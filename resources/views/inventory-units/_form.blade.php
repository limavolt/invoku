{{-- inventory-units/_form.blade.php --}}

<div class="form-group" id="div-label">
	<label class="control-label col-sm-3 text-right">{{ ucfirst(e(__('nama satuan'))) }}</label>

	<div class="col-sm-9">
		<input type="text" class="form-control" id="label" name="label" required>
		<p class="help-block text-error" id="error-label"></p>
	</div>
</div>

<div class="form-group">
	<label class="control-label col-sm-3 text-right">{{ ucfirst(e(__('jumlah isi'))) }}</label>

	<div class="col-sm-9">
		<div class="row">
			<div class="col-sm-4" id="div-pieces">
				<input type="number" class="form-control" id="pieces" name="pieces" min="0" required>
				<p class="help-block text-error" id="error-pieces"></p>
			</div>

			<div class="col-sm-8" id="div-pieces_label">
				<select class="form-control" id="pieces_label" name="pieces_label">
					<option></option>
				</select>

				<p class="help-block text-error" id="error-pieces"></p>
			</div>
		</div>
	</div>
</div>
