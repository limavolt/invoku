@component('components.modal', [
	'modal_id'    => 'modal-edit-inventory-unit',
	'modal_title' => 'perbarui informasi satuan',
])
	@slot('modal_body')
		<form method="PUT" action="{{ route('inventory-units.update', 0) }}" id="form-edit-inventory-unit">
			@csrf
			@include('inventory-units._form')
	@endslot

	@slot('modal_button')
			<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
			<button type="button" class="btn btn-primary" id="btn-edit-inventory-unit">Simpan</button>
		</form>
	@endslot
@endcomponent

@push('appscripts')
	<script type="text/javascript">
		$('#modal-edit-inventory-unit').ready(function () {
			var modal        = $('#modal-edit-inventory-unit');
			var form         = $('#form-edit-inventory-unit');
			var pieces_label = $('#form-edit-inventory-unit #pieces_label');
			/*
			 * Menampilkan jendela modal ketika tombol show resource ditekan.
			 */
			$('#units-table tbody').on('click', 'button[name="btn-edit-inventory-unit"]', function () {
				// Ambil data supplier dari baris tombol yang ditekan.
				var data = table.row($(this).closest('tr')).data();

				// Mengisi nilai masing-masing bidang masukan formulir sesusai data yang diambil.
				$.each($('input, select, textarea', '#form-edit-inventory-unit'), function () {
					if ($(this).attr('id')) {
						var id_element = $(this).attr('id');

						if (data[id_element]){
							$('#' + id_element, '#form-edit-inventory-unit').val(data[id_element]).trigger('change');
						}
						else {
							$('#' + id_element, '#form-edit-inventory-unit').val('').trigger('change');
						}
					}
				});

				// Menetapkan URL untuk pemrosesan form.
				form.attr('action', APP_URL + '/inventory-units/'+ $(this).data('id'));

				// Menampilkan jendela modal kepada pengguna.
				modal.modal('show');
			})

			/*
			 * Memodifikasi tampilan formulir pengubahan resource
			 * ketika modal pengubahan resource dimunculkan.
			 */
			modal.on('shown.bs.modal', function () {
				// Menghapus isian formulir.
				cleanModal('#form-edit-inventory-unit', false);

				$.ajax({
					method: 'get',
					type:   'json',
					url:    "{{ route('ajax.get-units') }}",

					success: function (data) {
						pieces_label.empty();
						pieces_label.css('width', '100%');
						pieces_label.select2({
							dropdownParent: $('#modal-edit-inventory-unit'),
							placeholder:    "{{ ucfirst(__('pilih dari daftar')) }}",
							tags:           true,
							allowClear:     true,
							data:			data,
						});

						$.each(data, function (key, value) {
							pieces_label.val(value.id).trigger('change');
						});
					}
				});
			});

			/*
			 * Menampilkan jendela modal ketika tombol edit resource ditekan.
			 */
			$('#btn-edit-inventory-unit').click(function () {
				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function(response) {
						generateSuccess(
							modal,
							'Informasi satuan ' + response.data.label + ' ' + response.data.pieces + ' ' + response.data.pieces_label +
								'berhasil diperbarui.'
						);
					},

					error: function (response) {
						console.log(response);

						generateError(
								'#form-edit-inventory-unit',
								response.responseJSON
						);
					}
				});
			});
		});
	</script>
@endpush
