{{-- inventory-units/create.blade.php --}}

@component('components.modal', [
	'modal_id'    => 'modal-add-inventory-unit',
	'modal_title' => 'menambah satuan',
])
	@slot('modal_body')
		<form method="post" action="{{ route('inventory-units.store') }}" id="form-add-inventory-unit">
			@csrf
			@include('inventory-units._form')
		</form>
	@endslot

	@slot('modal_button')
		<button type="button" class="btn btn-default" data-dismiss="modal">
			<span class="fa fa-arrow-circle-left"></span>
			{{ ucwords(__('batal')) }}
		</button>

		<button type="submit" class="btn btn-primary" id="btn-add-inventory-unit">
			<span class="fa fa-save"></span>
			{{ ucwords(__('simpan')) }}
		</button>
    @endslot
@endcomponent

@push('vendorscripts')
    <script src="{{ asset('assets/vendor/jquery-scrollto/jquery.scrollTo.min.js') }}"></script>
@endpush

@push('appscripts')
	<script type="text/javascript">
		$('#modal-add-inventory-unit').ready(function () {
			var modal        = $('#modal-add-inventory-unit');
			var form         = $('#form-add-inventory-unit');
			var pieces_label = $('#form-add-inventory-unit #pieces_label');

			/*
			 * Mengirim data pada form penambahan resource saat tombol submisi ditekan.
			 */
			$('#btn-add-inventory-unit').click(function () {
				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function (response) {
						generateSuccess(
							$('#modal-add-inventory-unit'),
							response.data.label + ' ' + response.data.pieces + ' ' + response.data.pieces_label +
                                ' berhasil ditambahkan ke dalam daftar satuan.',
                            'keep'
						);
					},

					error: function (response) {
						if (response.status == 422) {
							generateError(
								'#form-add-inventory-unit',
								response
							);
						}
						else {
							systemError();
						}
					}
				});
			});
        });

        /*
         * Memodifikasi tampilan formulir penambahan resource
         * ketika modal penambahan resource dimunculkan.
         */
        $('#modal-add-inventory-unit').on('shown.bs.modal', function () {
            clearForm($('#form-add-inventory-unit'));

            selectSubunit($('#form-add-inventory-unit #pieces_label'), $('#form-add-inventory-unit'));

            // $(window).scrollTo($('#modal-add-inventory-unit'));
        });
	</script>
@endpush
