{{-- inventory-units/index.blade.php --}}

@extends('layouts.dashboard', ['page_title' => 'manajemen produk'])

@section('content')
	<p class="lead">
		Di sini kamu dapat mengatur informasi tentang satuan produk
		yang kamu miliki. Kamu dapat melihat daftar satuan produk,
		menambahkan satuan baru, memperbarui informasi satuan yang ada,
		dan menghapus satuan yang sudah ada.
	</p>

	@component('components.panel', ['panel_title' => 'daftar satuan produk'])
		<div class="mb-sm hidden-xs">
			<button class="btn btn-primary btn-model-add" data-toggle="modal" data-target="#modal-add-inventory-unit" data-backdrop="static" data-keyboard="false">
				<span class="fa fa-plus"></span>
				{{ ucwords(__('tambah satuan')) }}
			</button>

			<div class="btn-group">
				<a href="{{ route('items.index') }}" class="btn btn-default">
					<span class="fa fa-cubes"></span>
					{{ ucwords(__('lihat daftar produk')) }}
				</a>

				<a href="{{ route('stocks.index') }}" class="btn btn-default">
					<span class="fa fa-cubes"></span>
					{{ ucwords(__('lihat daftar stok')) }}
				</a>
			</div>
		</div>

		<div class="mb-sm visible-xs-block">
			<button class="btn btn-primary btn-model-add" data-toggle="modal" data-target="#modal-add-inventory-unit" data-backdrop="static" data-keyboard="false">
				<span class="fa fa-plus"></span>
				{{ ucwords(__('tambah satuan')) }}
			</button>
		</div>

		@component('components.datatable-ajax', [
			'table_id'      => 'units',
			'table_headers' => ['nama satuan', 'jumlah isi', ''],
			'condition'     => true,
			'data'          => [
				['name' => 'label', 'data' => 'label'],
				['name' => 'pieces', 'data' => 'pieces'],
				['name' => 'pieces_label', 'data' => 'pieces_label', 'orderable' => 'false'],
			],
			'renders'       => [
				['text_align' => 'text-right', 'column_target' => '2, 4']
			]
		])
			@slot('data_send_ajax')
			@endslot
		@endcomponent

		@include('inventory-units.create')
		@include('inventory-units.edit')
		@include('inventory-units.destroy')
	@endcomponent
@endsection

@push('vendorstyles')
	<link href="{{ asset('assets/vendor/pnotify/pnotify.custom.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/vendor/select2-4.0.8/css/select2.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/vendor/select2-bootstrap/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
@endpush

@push('vendorscripts')
	<script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendor/select2-4.0.8/js/select2.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendor/fuelux/js/spinner.js') }}" type="text/javascript"></script>
@endpush
