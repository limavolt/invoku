{{-- suppliers/destroy.blade.php --}}

@component('components.modal', [
	'modal_id'    => 'modal-destroy-supplier',
	'modal_title' => 'penghapusan supplier',
])
	@slot('modal_body')
		<form method="post" action="{{ route('suppliers.destroy', 0) }}" id="form-destroy-supplier">
			@csrf
			@method('DELETE')

			<p>Apakah kamu yakin ingin menghapus data ini?</p>
	@endslot

	@slot('modal_button')
		<button type="button" class="btn btn-primary" data-dismiss="modal"><span class="fa fa-arrow-circle-left"></span> {{ ucwords(__('batal')) }}</button>
		<button type="button" class="btn btn-default" id="btn-destroy-supplier"><span class="fa fa-trash-o"></span> {{ ucwords(__('hapus data')) }}</button>
	</form>
	@endslot
@endcomponent

@push('appscripts')
	<script type="text/javascript">
		$('#modal-destroy-supplier').ready(function () {
			/*
			 * Menampilkan jendela modal ketika tombol hapus resource ditekan.
			 */
			 $('#suppliers-table tbody').on('click', 'button[name="btn-destroy-supplier"]', function() {
				// Menetapkan URL untuk pemrosesan form.
				$('#form-destroy-supplier').attr('action', APP_URL + '/suppliers/' + $(this).data('id'));

				// Menampilkan jendela modal.
				$('#modal-destroy-supplier').modal('show');
			});

			/*
			 * Mengirim data pada form penghapusan resource saat tombol submisi ditekan.
			 */
			$('#btn-destroy-supplier').click(function () {
				var form = $('#form-destroy-supplier');

				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function(response) {
						$('#modal-destroy-supplier').modal('hide');

						if (response.status == 'success') {
							new PNotify({
								type:  'success',
								title: 'Berhasil!',
								text:  response.message,
							});

							table.ajax.reload();
						}
						else {
							new PNotify({
								type:  'warning',
								title: 'Peringatan!',
								text:  response.message,
							});

							console.log(response);
						}
					},

					error: function(response) {
						console.log(response);

						systemError();
					}
				});
			});
		});
	</script>
@endpush
