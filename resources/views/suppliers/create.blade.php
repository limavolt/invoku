{{-- suppliers/create.blade.php --}}

@component('components.modal', [
	'modal_id'    => 'modal-add-supplier',
	'modal_title' => 'tambah pemasok produk'
])
	@slot('modal_body')
		<form method="post" action="{{ route('suppliers.store') }}" id="form-add-supplier">
			@csrf
			@include('companies.supplier.form')
		</form>
	@endslot

	@slot('modal_button')
		<button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-arrow-circle-left"></span> {{ ucwords(__('batal')) }}</button>
		<button type="submit" class="btn btn-primary" id="btn-add-supplier"><span class="fa fa-save"></span> {{ ucwords(__('simpan')) }}</button>
	@endslot
@endcomponent

@push('appscripts')
	<script type="text/javascript">
		$('#modal-add-supplier').ready(function () {
			/*
			 * Memodifikasi tampilan formulir penambahan resource
			 * ketika modal penambahan resource dimunculkan.
			 */
			 $('#modal-add-supplier').on('shown.bs.modal', function() {
				// Menghapus isian formulir.
				cleanModal('#form-add-supplier', true);
			});

			/*
			 * Mengirim data pada form penambahan resource saat tombol submisi ditekan.
			 */
			$('#btn-add-supplier').click(function() {
				var form = $('#form-add-supplier');

				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function(response) {
						$('#modal-add-supplier').modal('hide');

						table.ajax.reload();

						new PNotify({
							type:  'success',
							title: 'Berhasil!',
							text:  response.data.name + ' berhasil ditambahkan sebagai supplier.',
						});
					},

					error: function(response) {
						if (response.status == 422) {
							var errors = response.responseJSON;

							new PNotify({
								type:  'warning',
								title: 'Peringatan!',
								text:  'Terdapat kesalahan pada data yang dimasukkan',
							});

							cleanModal('#form-add-supplier', false);

							$.each(errors.errors, function(col_val, msg) {
								$('#div-' + col_val).addClass('has-error');
								$('#error-' + col_val).html(msg[0]);
							});

							console.log(errors.errors);
						}
						else {
							systemError();
						}
					}
				});
			});
		})
	</script>
@endpush
