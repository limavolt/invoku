{{-- suppliers/edit.blade.php --}}

@component('components.modal', [
	'modal_id'    => 'modal-edit-supplier',
	'modal_title' => 'perbarui informasi pemasok'
])
	@slot('modal_body')
		<form method="put" action="{{ route('suppliers.update', 0) }}" id="form-edit-supplier">
			@csrf
			@include('suppliers._form')
	@endslot

	@slot('modal_button')
			<button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-arrow-circle-left"></span> {{ ucwords(__('batal')) }}</button>
			<button type="button" class="btn btn-primary" id="btn-edit-supplier"><span class="fa fa-edit"></span> {{ ucwords(__('perbarui')) }}</button>
		</form>
	@endslot
@endcomponent

@push('appscripts')
	<script type="text/javascript">
		$('#modal-edit-supplier').ready(function () {
			/*
			 * Menampilkan jendela modal ketika tombol edit resource ditekan.
			 */
			 $('#suppliers-table tbody').on('click', 'button[name="btn-edit-supplier"]', function() {
				// Ambil data supplier dari baris tombol yang ditekan.
				var data = table.row($(this).closest('tr')).data();

				// Mengisi nilai masing-masing bidang masukan formulir sesusai data yang diambil.
				$.each($('input, select, textarea', '#form-edit-supplier'), function() {
					if ($(this).attr('id')) {
						var id_element = $(this).attr('id');

						if (data[id_element]) {
							$('#' + id_element, '#form-edit-supplier').val(data[id_element]).trigger('change');
						}
						else {
							$('#' + id_element, '#form-edit-supplier').val('').trigger('change');
						}
					}
				});

				// Membatasi data yang boleh diubah pengguna dengan menghilangkan bidang yang dilindungi.
				$('#modal-edit-supplier #div-type').remove();

				// Menetapkan URL untuk pemrosesan form.
				$('#form-edit-supplier').attr('action', APP_URL + '/suppliers/'+ $(this).data('id'));

				// Menampilkan jendela modal kepada pengguna.
				$('#modal-edit-supplier').modal('show');
			});

			/*
			 * Memodifikasi tampilan formulir pengubahan resource
			 * ketika modal pengubahan resource dimunculkan.
			 */
			$('#modal-edit-supplier').on('shown.bs.modal', function() {
				// Menghapus isian formulir.
				cleanModal('#form-edit-supplier', false);
			});

			/*
			 * Mengirim data pada form pengubahan resource saat tombol submisi ditekan.
			 */
			$('#btn-edit-supplier').click(function () {
				var form = $('#form-edit-supplier');

				$.ajax({
					url:    form.attr('action'),
					method: form.attr('method'),
					data:   form.serialize(),

					success: function(response) {
						$('#modal-edit-supplier').modal('hide');

						table.ajax.reload();

						new PNotify({
							type:  'success',
							title: 'Berhasil!',
							text:  'Informasi supplier ' + response.data.name + ' berhasil diperbarui.',
						});
					},

					error: function(response) {
						if (response.status == 422) {
							var errors = response.responseJSON;

							new PNotify({
								type:  'warning',
								title: 'Peringatan!',
								text:  'Terdapat kesalahan pada data yang dimasukkan',
							});

							cleanModal('#form-edit-supplier', false);

							$.each(errors.errors, function(col_val, msg) {
								$('#div-' + col_val, '#form-edit-supplier').addClass('has-error');
								$('#error- ' + col_val, '#form-edit-supplier').html(msg[0]);
							});
						}
						else {
							systemError();
						}
					}
				});
			});
		});
	</script>
@endpush
