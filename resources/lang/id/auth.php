<?php

return [
    /* Terjemahan Proses Otentifikasi
     *
     * Berikut ini adalah daftar terjemahan yang digunakan dalam proses
     * otentifikasi.
     */

    'failed' => 'Nama pengguna/email atau kata sandi yang kamu masukan salah.',
    'throttle' => 'Terlalu banyak upaya masuk. Silakan coba lagi dalam :seconds detik.',

];
